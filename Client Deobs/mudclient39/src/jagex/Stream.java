package jagex;

import java.io.*;
import java.net.Socket;

public class Stream {

	public Stream(InputStream inputstream) {
		wd = false;
		td = inputstream;
	}

	public Stream(Socket socket) throws IOException {
		wd = false;
		vd = socket;
		td = socket.getInputStream();
		ud = socket.getOutputStream();
	}

	public Stream(String s) throws IOException {
		wd = false;
		td = o.km(s);
	}

	public Stream(byte abyte0[]) {
		wd = false;
		xd = abyte0;
		yd = 0;
		wd = true;
	}

	public Stream(byte abyte0[], int i) {
		wd = false;
		xd = abyte0;
		yd = i;
		wd = true;
	}

	public void xb() {
		if (wd)
			return;
		if (ud != null)
			try {
				ud.flush();
			} catch (IOException _ex) {
			}
		try {
			if (vd != null)
				vd.close();
			if (td != null)
				td.close();
			if (ud != null) {
				ud.close();
				return;
			}
		} catch (IOException _ex) {
			System.out.println("Error closing stream");
		}
	}

	public int yb() throws IOException {
		if (xd != null)
			return xd[yd++];
		if (wd)
			return 0;
		else
			return td.read();
	}

	public int ub() throws IOException {
		return yb();
	}

	public int tb() throws IOException {
		int i = ub();
		int j = ub();
		return i * 256 + j;
	}

	public int pb() throws IOException {
		if (wd)
			return 0;
		else
			return td.available();
	}

	public void sb(int i, byte abyte0[]) throws IOException {
		if (wd)
			return;
		int j = 0;
		int k;
		for (; j < i; j += k)
			if ((k = td.read(abyte0, j, i - j)) <= 0)
				throw new IOException("EOF");

	}

	public void qb() throws IOException {
		for (int i = yb(); i != 61 && i != -1; i = yb())
			;
	}

	public int vb() throws IOException {
		int i = 0;
		boolean flag = false;
		int j;
		for (j = yb(); j < 48 || j > 57;) {
			if (j == 45)
				flag = true;
			j = yb();
			if (j == -1)
				throw new IOException("Eof!");
		}

		for (; j >= 48 && j <= 57; j = yb())
			i = (i * 10 + j) - 48;

		if (flag)
			i = -i;
		return i;
	}

	public String wb() throws IOException {
		String s = "";
		boolean flag = false;
		int i;
		for (i = yb(); i < 32 || i == 44 || i == 59 || i == 61;) {
			i = yb();
			if (i == -1)
				throw new IOException("Eof!");
		}

		if (i == 34) {
			flag = true;
			i = yb();
		}
		for (; i != -1; i = yb()) {
			if (!flag && (i == 44 || i == 61 || i == 59) || flag && i == 34)
				break;
			s = s + le[i];
		}

		return s;
	}

	public int rb() throws IOException {
		int i = 0;
		int j;
		for (j = yb(); (j < 48 || j > 57) && (j < 97 || j > 102) && (j < 65 || j > 70);) {
			j = yb();
			if (j == -1)
				throw new IOException("Eof!");
		}

		do {
			if (j >= 48 && j <= 57)
				i = (i * 16 + j) - 48;
			else if (j >= 97 && j <= 102) {
				i = (i * 16 + j + 10) - 97;
			} else {
				if (j < 65 || j > 70)
					break;
				i = (i * 16 + j + 10) - 65;
			}
			j = yb();
		} while (true);
		return i;
	}

	protected InputStream td;
	protected OutputStream ud;
	protected Socket vd;
	protected boolean wd;
	protected byte xd[];
	int yd;
	final int zd = 61;
	final int ae = 59;
	final int be = 42;
	final int ce = 43;
	final int de = 44;
	final int ee = 45;
	final int fe = 46;
	final int ge = 47;
	final int he = 92;
	final int ie = 32;
	final int je = 124;
	final int ke = 34;
	static char le[];

	static {
		le = new char[256];
		for (int i = 0; i < 256; i++)
			le[i] = (char) i;

		le[61] = '=';
		le[59] = ';';
		le[42] = '*';
		le[43] = '+';
		le[44] = ',';
		le[45] = '-';
		le[46] = '.';
		le[47] = '/';
		le[92] = '\\';
		le[124] = '|';
		le[33] = '!';
		le[34] = '"';
	}
}
