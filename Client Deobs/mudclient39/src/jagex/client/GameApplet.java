package jagex.client;

import jagex.o;
import jagex.n;

import java.applet.Applet;
import java.awt.*;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.io.*;

@SuppressWarnings("serial")
public class GameApplet extends Applet implements Runnable {

	public final void ui(int j, int l, String s, boolean flag) {
		op = false;
		System.out.println("Started application");
		hp = j;
		ip = l;
		np = new GameFrame(this, j, l, s, flag, false);
		tp = 1;
		jp = new Thread(this);
		jp.start();
		jp.setPriority(1);
	}

	public final boolean qj() {
		return op;
	}

	public final void fj(int j, int l, String s, boolean flag) {
		if (np != null) {
		} else {
			hp = j;
			ip = l;
			np = new GameFrame(this, j, l, s, flag, op);
		}
	}

	public final void vi(int j, int l) {
		if (np == null) {
		} else {
			np.resize(j, l);
			hp = j;
			ip = l;
		}
	}

	public final void xi(Image image) {
		if (np == null) {
		} else {
			np.setIconImage(image);
		}
	}

	public void mj(int j, int l) {
		vi(j, l);
	}

	public final Graphics getGraphics() {
		if (np == null)
			return super.getGraphics();
		else
			return np.getGraphics();
	}

	public final int ej() {
		return hp;
	}

	public final int oj() {
		return ip;
	}

	public final Image createImage(int j, int l) {
		if (np == null)
			return super.createImage(j, l);
		else
			return np.createImage(j, l);
	}

	public Frame ij() {
		return np;
	}

	public final void lj(int j) {
		kp = 1000 / j;
	}

	public final void nj(int j) {
		lp = j;
	}

	public final void pj() {
		for (int j = 0; j < 10; j++)
			mp[j] = 0L;

	}

	public synchronized boolean keyDown(Event event, int j) {
		qq = j;
		rq = j;
		sp = 0;
		if (j == 1006)
			fq = true;
		if (j == 1007)
			gq = true;
		if (j == 1004)
			hq = true;
		if (j == 1005)
			iq = true;
		if ((char) j == ' ')
			jq = true;
		if ((char) j == 'n' || (char) j == 'm')
			kq = true;
		if ((char) j == 'N' || (char) j == 'M')
			kq = true;
		if ((char) j == '{')
			dq = true;
		if ((char) j == '}')
			eq = true;
		if ((char) j == '\u03F0')
			sq = !sq;
		if ((j >= 97 && j <= 122 || j >= 65 && j <= 90 || j >= 48 && j <= 57 || j == 32) && tq.length() < 16)
			tq += (char) j;
		if (j >= 32 && j <= 122 && vq.length() < 80)
			vq += (char) j;
		if (j == 8 && tq.length() > 0)
			tq = tq.substring(0, tq.length() - 1);
		if (j == 8 && vq.length() > 0)
			vq = vq.substring(0, vq.length() - 1);
		if (j == 10 || j == 13) {
			uq = tq;
			wq = vq;
		}
		return true;
	}

	public synchronized boolean keyUp(Event event, int j) {
		qq = 0;
		if (j == 1006)
			fq = false;
		if (j == 1007)
			gq = false;
		if (j == 1004)
			hq = false;
		if (j == 1005)
			iq = false;
		if ((char) j == ' ')
			jq = false;
		if ((char) j == 'n' || (char) j == 'm')
			kq = false;
		if ((char) j == 'N' || (char) j == 'M')
			kq = false;
		if ((char) j == '{')
			dq = false;
		if ((char) j == '}')
			eq = false;
		return true;
	}

	public synchronized boolean mouseMove(Event event, int j, int l) {
		mq = j;
		nq = l + rp;
		oq = 0;
		sp = 0;
		return true;
	}

	public synchronized boolean mouseUp(Event event, int j, int l) {
		mq = j;
		nq = l + rp;
		oq = 0;
		return true;
	}

	public synchronized boolean mouseDown(Event event, int j, int l) {
		mq = j;
		nq = l + rp;
		if (event.metaDown())
			oq = 2;
		else
			oq = 1;
		pq = oq;
		sp = 0;
		return true;
	}

	public synchronized boolean mouseDrag(Event event, int j, int l) {
		mq = j;
		nq = l + rp;
		if (event.metaDown())
			oq = 2;
		else
			oq = 1;
		return true;
	}

	@SuppressWarnings("deprecation")
	public final void init() {
		op = true;
		System.out.println("Started applet");
		hp = size().width;
		ip = size().height;
		tp = 1;
		o.kdb = getCodeBase();
		jp = new Thread(this);
		jp.start();
	}

	public final void start() {
		if (pp >= 0)
			pp = 0;
	}

	public final void stop() {
		if (pp >= 0)
			pp = 4000 / kp;
	}

	@SuppressWarnings("deprecation")
	public final void destroy() {
		pp = -1;
		try {
			Thread.sleep(5000L);
		} catch (Exception ignored) {
		}
		if (pp == -1) {
			System.out.println("5 seconds expired, forcing kill");
			aj();
			if (jp != null) {
				jp.stop();
				jp = null;
			}
		}
	}

	public final void aj() {
		pp = -2;
		System.out.println("Closing program");
		try {
			Thread.sleep(1000L);
		} catch (Exception ignored) {
		}
		if (np != null)
			np.dispose();
		if (!op)
			System.exit(0);
	}

	public final void run() {
		if (tp == 1) {
			tp = 2;
			cq = getGraphics();
			cj();
			hj(0, "Loading...");
			tp = 0;
		}
		int j = 0;
		int i1 = 256;
		int j1 = 1;
		int k1 = 0;
		for (int i2 = 0; i2 < 10; i2++)
			mp[i2] = System.currentTimeMillis();

		while (pp >= 0) {
			if (pp > 0) {
				pp--;
				if (pp == 0) {
					aj();
					jp = null;
					return;
				}
			}
			int j2 = i1;
			int k2 = j1;
			i1 = 300;
			j1 = 1;
			long l1 = System.currentTimeMillis();
			if (mp[j] == 0L) {
				i1 = j2;
				j1 = k2;
			} else if (l1 > mp[j])
				i1 = (int) ((long) (2560 * kp) / (l1 - mp[j]));
			if (i1 < 25)
				i1 = 25;
			if (i1 > 256) {
				i1 = 256;
				j1 = (int) ((long) kp - (l1 - mp[j]) / 10L);
				if (j1 < lq)
					j1 = lq;
			}
			try {
				Thread.sleep(j1);
			} catch (InterruptedException ignored) {
			}
			mp[j] = l1;
			j = (j + 1) % 10;
			if (j1 > 1) {
				for (int l2 = 0; l2 < 10; l2++)
					if (mp[l2] != 0L)
						mp[l2] += j1;

			}
			int i3 = 0;
			while (k1 < 256) {
				k1 += i1;
				if (++i3 > lp) {
					k1 = 0;
					qp += 6;
					if (qp > 25) {
						qp = 0;
						sq = true;
					}
					break;
				}
			}
			qp--;
			k1 &= 0xff;
			xq = (1000 * i1) / (kp * 256);
			if (op && j == 0)
				showStatus("Fps:" + xq + "Del:" + j1);
			if (np != null && (np.o() != hp || np.p() != ip))
				mj(np.o(), np.p());
		}
		if (pp == -1)
			aj();
		jp = null;
	}

	public final void update(Graphics g) {
		paint(g);
	}

	public final void paint(Graphics g) {
		if (tp == 2 && bq != null) {
			hj(wp, xp);
		}
	}

	public void cj() {
		try {
			byte[] abyte0 = o.pm("jagex.jag");
			byte[] abyte1 = o.lm("logo.tga", 0, abyte0);
			assert abyte1 != null;
			bq = bj(abyte1);
			GameImageProducer.ye(o.lm("h11p.jf", 0, abyte0));
			GameImageProducer.ye(o.lm("h12b.jf", 0, abyte0));
			GameImageProducer.ye(o.lm("h12p.jf", 0, abyte0));
			GameImageProducer.ye(o.lm("h13b.jf", 0, abyte0));
			GameImageProducer.ye(o.lm("h14b.jf", 0, abyte0));
			GameImageProducer.ye(o.lm("h16b.jf", 0, abyte0));
			GameImageProducer.ye(o.lm("h20b.jf", 0, abyte0));
			GameImageProducer.ye(o.lm("h24b.jf", 0, abyte0));
		} catch (IOException _ex) {
			System.out.println("Error loading jagex.dat");
		}
	}

	public void hj(int j, String s) {
		int l = (hp - 281) / 2;
		int i1 = (ip - 148) / 2;
		cq.setColor(Color.black);
		cq.fillRect(0, 0, hp, ip);
		if (!vp)
			cq.drawImage(bq, l, i1, this);
		l += 2;
		i1 += 90;
		wp = j;
		xp = s;
		cq.setColor(new Color(132, 132, 132));
		if (vp)
			cq.setColor(new Color(220, 0, 0));
		cq.drawRect(l - 2, i1 - 2, 280, 23);
		cq.fillRect(l, i1, (277 * j) / 100, 20);
		cq.setColor(new Color(198, 198, 198));
		if (vp)
			cq.setColor(new Color(255, 255, 255));
		zi(cq, s, yp, l + 138, i1 + 10);
		if (!vp) {
			zi(cq, "Created by JAGeX - visit www.jagex.com", zp, l + 138, i1 + 30);
			zi(cq, "Copyright \2512000 Andrew Gower", zp, l + 138, i1 + 44);
		} else {
			cq.setColor(new Color(132, 132, 152));
			zi(cq, "Copyright \2512000 Andrew Gower", aq, l + 138, ip - 20);
		}
		if (up != null) {
			cq.setColor(Color.white);
			zi(cq, up, zp, l + 138, i1 - 120);
		}
	}

	public void gj(int j, String s) {
		int l = (hp - 281) / 2;
		int i1 = (ip - 148) / 2;
		l += 2;
		i1 += 90;
		wp = j;
		xp = s;
		int j1 = (277 * j) / 100;
		cq.setColor(new Color(132, 132, 132));
		if (vp)
			cq.setColor(new Color(220, 0, 0));
		cq.fillRect(l, i1, j1, 20);
		cq.setColor(Color.black);
		cq.fillRect(l + j1, i1, 277 - j1, 20);
		cq.setColor(new Color(198, 198, 198));
		if (vp)
			cq.setColor(new Color(255, 255, 255));
		zi(cq, s, yp, l + 138, i1 + 10);
	}

	public void zi(Graphics g, String s, Font font, int j, int l) {
		Object obj;
		if (np == null)
			obj = this;
		else
			obj = np;
		FontMetrics fontmetrics = ((Component) (obj)).getFontMetrics(font);
		fontmetrics.stringWidth(s);
		g.setFont(font);
		g.drawString(s, j - fontmetrics.stringWidth(s) / 2, l + fontmetrics.getHeight() / 4);
	}

	public Image bj(byte[] abyte0) {
		int j = abyte0[13] * 256 + abyte0[12];
		int l = abyte0[15] * 256 + abyte0[14];
		byte[] abyte1 = new byte[256];
		byte[] abyte2 = new byte[256];
		byte[] abyte3 = new byte[256];
		for (int i1 = 0; i1 < 256; i1++) {
			abyte1[i1] = abyte0[20 + i1 * 3];
			abyte2[i1] = abyte0[19 + i1 * 3];
			abyte3[i1] = abyte0[18 + i1 * 3];
		}

		IndexColorModel indexcolormodel = new IndexColorModel(8, 256, abyte1, abyte2, abyte3);
		byte[] abyte4 = new byte[j * l];
		int j1 = 0;
		for (int k1 = l - 1; k1 >= 0; k1--) {
			for (int l1 = 0; l1 < j; l1++)
				abyte4[j1++] = abyte0[786 + l1 + k1 * j];

		}

		MemoryImageSource memoryimagesource = new MemoryImageSource(j, l, indexcolormodel, abyte4, 0, j);
		Image image = createImage(memoryimagesource);
		return image;
	}

	public byte[] wi(String s, String s1, int j) throws IOException {
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		byte[] abyte0 = null;
		while (l < 2)
			try {
				gj(j, "Loading " + s1 + " - 0%");
				if (l == 1)
					s = s.toUpperCase();
				java.io.InputStream inputstream = o.km(s);
				DataInputStream datainputstream = new DataInputStream(inputstream);
				byte[] abyte2 = new byte[6];
				datainputstream.readFully(abyte2, 0, 6);
				i1 = ((abyte2[0] & 0xff) << 16) + ((abyte2[1] & 0xff) << 8) + (abyte2[2] & 0xff);
				j1 = ((abyte2[3] & 0xff) << 16) + ((abyte2[4] & 0xff) << 8) + (abyte2[5] & 0xff);
				gj(j, "Loading " + s1 + " - 5%");
				int k1 = 0;
				abyte0 = new byte[j1];
				while (k1 < j1) {
					int l1 = j1 - k1;
					if (l1 > 1000)
						l1 = 1000;
					datainputstream.readFully(abyte0, k1, l1);
					k1 += l1;
					gj(j, "Loading " + s1 + " - " + (5 + (k1 * 95) / j1) + "%");
				}
				l = 2;
				datainputstream.close();
			} catch (IOException _ex) {
				l++;
			}
		gj(j, "Unpacking " + s1);
		if (j1 != i1) {
			byte[] abyte1 = new byte[i1];
			n.uj(abyte1, i1, abyte0, j1, 0);
			return abyte1;
		} else {
			return abyte0;
		}
	}

	public GameApplet() {
		hp = 512;
		ip = 384;
		kp = 20;
		lp = 1000;
		mp = new long[10];
		tp = 1;
		vp = false;
		xp = "Loading";
		yp = new Font("TimesRoman", 0, 15);
		zp = new Font("Helvetica", 1, 13);
		aq = new Font("Helvetica", 0, 12);
		dq = false;
		eq = false;
		fq = false;
		gq = false;
		hq = false;
		iq = false;
		jq = false;
		kq = false;
		lq = 1;
		sq = false;
		tq = "";
		uq = "";
		vq = "";
		wq = "";
	}

	private int hp;
	private int ip;
	private Thread jp;
	private int kp;
	private int lp;
	private long[] mp;
	static GameFrame np = null;
	private boolean op;
	private int pp;
	private int qp;
	public int rp;
	public int sp;
	public int tp;
	public String up;
	private boolean vp;
	private int wp;
	private String xp;
	private Font yp;
	private Font zp;
	private Font aq;
	private Image bq;
	private Graphics cq;
	public boolean dq;
	public boolean eq;
	public boolean fq;
	public boolean gq;
	public boolean hq;
	public boolean iq;
	public boolean jq;
	public boolean kq;
	public int lq;
	public int mq;
	public int nq;
	public int oq;
	public int pq;
	public int qq;
	public int rq;
	public boolean sq;
	public String tq;
	public String uq;
	public String vq;
	public String wq;
	public int xq;

}
