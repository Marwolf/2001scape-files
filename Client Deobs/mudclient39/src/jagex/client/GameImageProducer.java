package jagex.client;

import java.awt.*;
import java.awt.image.*;

public class GameImageProducer implements ImageProducer, ImageObserver {

	public GameImageProducer(int j, int k, int l, Component component) {
		mk = true;
		rk = false;
		vk = false;
		ok = k;
		qk = j;
		vj = sj = j;
		wj = tj = k;
		uj = j * k;
		yj = new int[j * k];
		ck = new int[l][];
		lk = new boolean[l];
		dk = new byte[l][];
		ek = new int[l][];
		fk = new int[l];
		gk = new int[l];
		jk = new int[l];
		kk = new int[l];
		hk = new int[l];
		ik = new int[l];
		if (j > 1 && k > 1 && component != null) {
			xj = new DirectColorModel(32, 0xff0000, 65280, 255);
			int i1 = sj * tj;
			for (int j1 = 0; j1 < i1; j1++)
				yj[j1] = 0;

			bk = component.createImage(this);
			of();
			component.prepareImage(bk, component);
			of();
			component.prepareImage(bk, component);
			of();
			component.prepareImage(bk, component);
		}
	}

	public synchronized void qg(int j, int k) {
		if (sj > vj)
			sj = vj;
		if (tj > wj)
			tj = wj;
		sj = j;
		tj = k;
		uj = j * k;
	}

	public synchronized void addConsumer(ImageConsumer imageconsumer) {
		zj = imageconsumer;
		imageconsumer.setDimensions(sj, tj);
		imageconsumer.setProperties(null);
		imageconsumer.setColorModel(xj);
		imageconsumer.setHints(14);
	}

	public synchronized boolean isConsumer(ImageConsumer imageconsumer) {
		return zj == imageconsumer;
	}

	public synchronized void removeConsumer(ImageConsumer imageconsumer) {
		if (zj == imageconsumer)
			zj = null;
	}

	public void startProduction(ImageConsumer imageconsumer) {
		addConsumer(imageconsumer);
	}

	public void requestTopDownLeftRightResend(ImageConsumer imageconsumer) {
		System.out.println("TDLR");
	}

	public synchronized void of() {
		if (zj == null) {
			return;
		} else {
			zj.setPixels(0, 0, sj, tj, xj, yj, 0, sj);
			zj.imageComplete(2);
			return;
		}
	}

	public void kf(int j, int k, int l, int i1) {
		if (j < 0)
			j = 0;
		if (k < 0)
			k = 0;
		if (l > sj)
			l = sj;
		if (i1 > tj)
			i1 = tj;
		pk = j;
		nk = k;
		qk = l;
		ok = i1;
	}

	public void mf() {
		pk = 0;
		nk = 0;
		qk = sj;
		ok = tj;
	}

	public void cf(Graphics g, int j, int k) {
		of();
		g.drawImage(bk, j, k, this);
	}

	public void df() {
		int j = sj * tj;
		if (!rk) {
			for (int k = 0; k < j; k++)
				yj[k] = 0;

			return;
		}
		int l = 0;
		for (int i1 = -tj; i1 < 0; i1 += 2) {
			for (int j1 = -sj; j1 < 0; j1++)
				yj[l++] = 0;

			l += sj;
		}

	}

	public void rf(int j, int k, int l, int i1, int j1) {
		int k1 = 256 - j1;
		int l1 = (i1 >> 16 & 0xff) * j1;
		int i2 = (i1 >> 8 & 0xff) * j1;
		int j2 = (i1 & 0xff) * j1;
		int j3 = k - l;
		if (j3 < 0)
			j3 = 0;
		int k3 = k + l;
		if (k3 >= tj)
			k3 = tj - 1;
		byte byte0 = 1;
		if (rk) {
			byte0 = 2;
			if ((j3 & 1) != 0)
				j3++;
		}
		for (int l3 = j3; l3 <= k3; l3 += byte0) {
			int i4 = l3 - k;
			int j4 = (int) Math.sqrt(l * l - i4 * i4);
			int k4 = j - j4;
			if (k4 < 0)
				k4 = 0;
			int l4 = j + j4;
			if (l4 >= sj)
				l4 = sj - 1;
			int i5 = k4 + l3 * sj;
			for (int j5 = k4; j5 <= l4; j5++) {
				int k2 = (yj[i5] >> 16 & 0xff) * k1;
				int l2 = (yj[i5] >> 8 & 0xff) * k1;
				int i3 = (yj[i5] & 0xff) * k1;
				int k5 = ((l1 + k2 >> 8) << 16) + ((i2 + l2 >> 8) << 8) + (j2 + i3 >> 8);
				yj[i5++] = k5;
			}

		}

	}

	public void nf(int j, int k, int l, int i1, int j1, int k1) {
		if (j < pk) {
			l -= pk - j;
			j = pk;
		}
		if (k < nk) {
			i1 -= nk - k;
			k = nk;
		}
		if (j + l > qk)
			l = qk - j;
		if (k + i1 > ok)
			i1 = ok - k;
		int l1 = 256 - k1;
		int i2 = (j1 >> 16 & 0xff) * k1;
		int j2 = (j1 >> 8 & 0xff) * k1;
		int k2 = (j1 & 0xff) * k1;
		int k3 = sj - l;
		byte byte0 = 1;
		if (rk) {
			byte0 = 2;
			k3 += sj;
			if ((k & 1) != 0) {
				k++;
				i1--;
			}
		}
		int l3 = j + k * sj;
		for (int i4 = 0; i4 < i1; i4 += byte0) {
			for (int j4 = -l; j4 < 0; j4++) {
				int l2 = (yj[l3] >> 16 & 0xff) * l1;
				int i3 = (yj[l3] >> 8 & 0xff) * l1;
				int j3 = (yj[l3] & 0xff) * l1;
				int k4 = ((i2 + l2 >> 8) << 16) + ((j2 + i3 >> 8) << 8) + (k2 + j3 >> 8);
				yj[l3++] = k4;
			}

			l3 += k3;
		}

	}

	public void ag(int j, int k, int l, int i1, int j1, int k1) {
		if (j < pk) {
			l -= pk - j;
			j = pk;
		}
		if (j + l > qk)
			l = qk - j;
		int l1 = k1 >> 16 & 0xff;
		int i2 = k1 >> 8 & 0xff;
		int j2 = k1 & 0xff;
		int k2 = j1 >> 16 & 0xff;
		int l2 = j1 >> 8 & 0xff;
		int i3 = j1 & 0xff;
		int j3 = sj - l;
		byte byte0 = 1;
		if (rk) {
			byte0 = 2;
			j3 += sj;
			if ((k & 1) != 0) {
				k++;
				i1--;
			}
		}
		int k3 = j + k * sj;
		for (int l3 = 0; l3 < i1; l3 += byte0)
			if (l3 + k >= nk && l3 + k < ok) {
				int i4 = ((l1 * l3 + k2 * (i1 - l3)) / i1 << 16) + ((i2 * l3 + l2 * (i1 - l3)) / i1 << 8)
						+ (j2 * l3 + i3 * (i1 - l3)) / i1;
				for (int j4 = -l; j4 < 0; j4++)
					yj[k3++] = i4;

				k3 += j3;
			} else {
				k3 += sj;
			}

	}

	public void qf(int j, int k, int l, int i1, int j1) {
		if (j < pk) {
			l -= pk - j;
			j = pk;
		}
		if (k < nk) {
			i1 -= nk - k;
			k = nk;
		}
		if (j + l > qk)
			l = qk - j;
		if (k + i1 > ok)
			i1 = ok - k;
		int k1 = sj - l;
		byte byte0 = 1;
		if (rk) {
			byte0 = 2;
			k1 += sj;
			if ((k & 1) != 0) {
				k++;
				i1--;
			}
		}
		int l1 = j + k * sj;
		for (int i2 = -i1; i2 < 0; i2 += byte0) {
			for (int j2 = -l; j2 < 0; j2++)
				yj[l1++] = j1;

			l1 += k1;
		}

	}

	public void hf(int j, int k, int l, int i1, int j1) {
		kg(j, k, l, j1);
		kg(j, (k + i1) - 1, l, j1);
		og(j, k, i1, j1);
		og((j + l) - 1, k, i1, j1);
	}

	public void kg(int j, int k, int l, int i1) {
		if (k < nk || k >= ok)
			return;
		if (j < pk) {
			l -= pk - j;
			j = pk;
		}
		if (j + l > qk)
			l = qk - j;
		int j1 = j + k * sj;
		for (int k1 = 0; k1 < l; k1++)
			yj[j1 + k1] = i1;

	}

	public void og(int j, int k, int l, int i1) {
		if (j < pk || j >= qk)
			return;
		if (k < nk) {
			l -= nk - k;
			k = nk;
		}
		if (k + l > qk)
			l = ok - k;
		int j1 = j + k * sj;
		for (int k1 = 0; k1 < l; k1++)
			yj[j1 + k1 * sj] = i1;

	}

	public void gg(int j, int k, int l) {
		if (j < pk || k < nk || j >= qk || k >= ok) {
			return;
		} else {
			yj[j + k * sj] = l;
			return;
		}
	}

	public void ze() {
		int l = sj * tj;
		for (int k = 0; k < l; k++) {
			int j = yj[k] & 0xffffff;
			yj[k] = (j >>> 1 & 0x7f7f7f) + (j >>> 2 & 0x3f3f3f) + (j >>> 3 & 0x1f1f1f) + (j >>> 4 & 0xf0f0f);
		}

	}

	public static int fg(int j, int k, int l) {
		return (j << 16) + (k << 8) + l;
	}

	public void dg() {
		for (int j = 0; j < ck.length; j++) {
			ck[j] = null;
			fk[j] = 0;
			gk[j] = 0;
			dk[j] = null;
			ek[j] = null;
		}

	}

	public void eg(byte abyte0[], int j, int k, boolean flag, boolean flag1) {
		cg(abyte0, j, k, flag, 1, 1, flag1);
	}

	public void jg(byte abyte0[], int j, int k, boolean flag, int l, boolean flag1) {
		cg(abyte0, j, k, flag, l, 1, flag1);
	}

	public void cg(byte abyte0[], int j, int k, boolean flag, int l, int i1, boolean flag1) {
		int j1 = (abyte0[13 + j] & 0xff) * 256 + (abyte0[12 + j] & 0xff);
		int k1 = (abyte0[15 + j] & 0xff) * 256 + (abyte0[14 + j] & 0xff);
		int l1 = -1;
		int ai[] = new int[256];
		for (int i2 = 0; i2 < 256; i2++) {
			ai[i2] = 0xff000000 + ((abyte0[j + 20 + i2 * 3] & 0xff) << 16) + ((abyte0[j + 19 + i2 * 3] & 0xff) << 8)
					+ (abyte0[j + 18 + i2 * 3] & 0xff);
			if (ai[i2] == -65281)
				l1 = i2;
		}

		if (l1 == -1)
			flag = false;
		if (flag1 && flag)
			ai[l1] = ai[0];
		int j2 = j1 / l;
		int k2 = k1 / i1;
		int ai1[] = new int[j2 * k2];
		for (int l2 = 0; l2 < i1; l2++) {
			for (int i3 = 0; i3 < l; i3++) {
				int j3 = 0;
				for (int k3 = k2 * l2; k3 < k2 * (l2 + 1); k3++) {
					for (int l3 = j2 * i3; l3 < j2 * (i3 + 1); l3++)
						if (flag1)
							ai1[j3++] = abyte0[j + 786 + l3 + (k1 - k3 - 1) * j1] & 0xff;
						else
							ai1[j3++] = ai[abyte0[j + 786 + l3 + (k1 - k3 - 1) * j1] & 0xff];

				}

				if (flag1)
					af(ai1, j2, k2, k++, flag, ai, l1);
				else
					af(ai1, j2, k2, k++, flag, null, -65281);
			}

		}

	}

	private void af(int ai[], int j, int k, int l, boolean flag, int ai1[], int i1) {
		int j1 = 0;
		int k1 = 0;
		int l1 = j;
		int i2 = k;
		if (flag && mk) {
			label0: for (int j2 = 0; j2 < k; j2++) {
				for (int i3 = 0; i3 < j; i3++) {
					int i4 = ai[i3 + j2 * j];
					if (i4 == i1)
						continue;
					k1 = j2;
					break label0;
				}

			}

			label1: for (int j3 = 0; j3 < j; j3++) {
				for (int j4 = 0; j4 < k; j4++) {
					int j5 = ai[j3 + j4 * j];
					if (j5 == i1)
						continue;
					j1 = j3;
					break label1;
				}

			}

			label2: for (int k4 = k - 1; k4 >= 0; k4--) {
				for (int k5 = 0; k5 < j; k5++) {
					int k6 = ai[k5 + k4 * j];
					if (k6 == i1)
						continue;
					i2 = k4 + 1;
					break label2;
				}

			}

			label3: for (int l5 = j - 1; l5 >= 0; l5--) {
				for (int l6 = 0; l6 < k; l6++) {
					int i7 = ai[l5 + l6 * j];
					if (i7 == i1)
						continue;
					l1 = l5 + 1;
					break label3;
				}

			}

		}
		fk[l] = l1 - j1;
		gk[l] = i2 - k1;
		lk[l] = flag;
		hk[l] = j1;
		ik[l] = k1;
		jk[l] = j;
		kk[l] = k;
		if (ai1 == null) {
			ck[l] = new int[(l1 - j1) * (i2 - k1)];
			int k2 = 0;
			for (int k3 = k1; k3 < i2; k3++) {
				for (int l4 = j1; l4 < l1; l4++) {
					int i6 = ai[l4 + k3 * j];
					if (flag) {
						if (i6 == i1)
							i6 = 0;
						if (i6 == 0xff000000)
							i6 = 0xff010101;
					}
					ck[l][k2++] = i6 & 0xffffff;
				}

			}

			return;
		}
		dk[l] = new byte[(l1 - j1) * (i2 - k1)];
		ek[l] = ai1;
		int l2 = 0;
		for (int l3 = k1; l3 < i2; l3++) {
			for (int i5 = j1; i5 < l1; i5++) {
				int j6 = ai[i5 + l3 * j];
				if (flag)
					if (j6 == i1)
						j6 = 0;
					else if (j6 == 0)
						j6 = i1;
				dk[l][l2++] = (byte) j6;
			}

		}

	}

	public void jf(int j, int k, int l, int i1, int j1) {
		fk[j] = i1;
		gk[j] = j1;
		lk[j] = false;
		hk[j] = 0;
		ik[j] = 0;
		jk[j] = i1;
		kk[j] = j1;
		int k1 = i1 * j1;
		int l1 = 0;
		ck[j] = new int[k1];
		for (int i2 = k; i2 < k + i1; i2++) {
			for (int j2 = l; j2 < l + j1; j2++)
				ck[j][l1++] = yj[i2 + j2 * sj];

		}

	}

	public void _mthif(int j, int k, int l) {
		rg(j - jk[l] / 2, k - kk[l] / 2, l);
	}

	public void rg(int j, int k, int l) {
		if (lk[l]) {
			j += hk[l];
			k += ik[l];
		}
		int i1 = j + k * sj;
		int j1 = 0;
		int k1 = gk[l];
		int l1 = fk[l];
		int i2 = sj - l1;
		int j2 = 0;
		if (k < nk) {
			int k2 = nk - k;
			k1 -= k2;
			k = nk;
			j1 += k2 * l1;
			i1 += k2 * sj;
		}
		if (k + k1 >= ok)
			k1 -= ((k + k1) - ok) + 1;
		if (j < pk) {
			int l2 = pk - j;
			l1 -= l2;
			j = pk;
			j1 += l2;
			i1 += l2;
			j2 += l2;
			i2 += l2;
		}
		if (j + l1 >= qk) {
			int i3 = ((j + l1) - qk) + 1;
			l1 -= i3;
			j2 += i3;
			i2 += i3;
		}
		if (l1 <= 0 || k1 <= 0)
			return;
		byte byte0 = 1;
		if (rk) {
			byte0 = 2;
			i2 += sj;
			j2 += fk[l];
			if ((k & 1) != 0) {
				i1 += sj;
				k1--;
			}
		}
		if (lk[l]) {
			ff(yj, ck[l], 0, j1, i1, l1, k1, i2, j2, byte0);
			return;
		} else {
			tg(yj, ck[l], j1, i1, l1, k1, i2, j2, byte0);
			return;
		}
	}

	private void tg(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1) {
		int i2 = -(l >> 2);
		l = -(l & 3);
		for (int j2 = -i1; j2 < 0; j2 += l1) {
			for (int k2 = i2; k2 < 0; k2++) {
				ai[k++] = ai1[j++];
				ai[k++] = ai1[j++];
				ai[k++] = ai1[j++];
				ai[k++] = ai1[j++];
			}

			for (int l2 = l; l2 < 0; l2++)
				ai[k++] = ai1[j++];

			k += j1;
			j += k1;
		}

	}

	private void ff(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2) {
		int j2 = -(i1 >> 2);
		i1 = -(i1 & 3);
		for (int k2 = -j1; k2 < 0; k2 += i2) {
			for (int l2 = j2; l2 < 0; l2++) {
				j = ai1[k++];
				if (j != 0)
					ai[l++] = j;
				else
					l++;
				j = ai1[k++];
				if (j != 0)
					ai[l++] = j;
				else
					l++;
				j = ai1[k++];
				if (j != 0)
					ai[l++] = j;
				else
					l++;
				j = ai1[k++];
				if (j != 0)
					ai[l++] = j;
				else
					l++;
			}

			for (int i3 = i1; i3 < 0; i3++) {
				j = ai1[k++];
				if (j != 0)
					ai[l++] = j;
				else
					l++;
			}

			l += k1;
			k += l1;
		}

	}

	public void vf(int j, int k, int l) {
		if (lk[l]) {
			j += hk[l];
			k += ik[l];
		}
		int i1 = j + k * sj;
		int j1 = 0;
		int k1 = gk[l];
		int l1 = fk[l];
		int i2 = sj - l1;
		int j2 = 0;
		if (k < nk) {
			int k2 = nk - k;
			k1 -= k2;
			k = nk;
			j1 += k2 * l1;
			i1 += k2 * sj;
		}
		if (k + k1 >= ok)
			k1 -= ((k + k1) - ok) + 1;
		if (j < pk) {
			int l2 = pk - j;
			l1 -= l2;
			j = pk;
			j1 += l2;
			i1 += l2;
			j2 += l2;
			i2 += l2;
		}
		if (j + l1 >= qk) {
			int i3 = ((j + l1) - qk) + 1;
			l1 -= i3;
			j2 += i3;
			i2 += i3;
		}
		if (l1 <= 0 || k1 <= 0)
			return;
		byte byte0 = 1;
		if (rk) {
			byte0 = 2;
			i2 += sj;
			j2 += fk[l];
			if ((k & 1) != 0) {
				i1 += sj;
				k1--;
			}
		}
		pg(yj, ck[l], 0, j1, i1, l1, k1, i2, j2, byte0);
	}

	private void pg(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2) {
		int j2 = -(i1 >> 2);
		i1 = -(i1 & 3);
		for (int k2 = -j1; k2 < 0; k2 += i2) {
			for (int l2 = j2; l2 < 0; l2++) {
				j = ai1[k++];
				if (j != 0)
					ai[l++] = (j >> 1 & 0x7f7f7f) + (ai[l] >> 1 & 0x7f7f7f);
				else
					l++;
				j = ai1[k++];
				if (j != 0)
					ai[l++] = (j >> 1 & 0x7f7f7f) + (ai[l] >> 1 & 0x7f7f7f);
				else
					l++;
				j = ai1[k++];
				if (j != 0)
					ai[l++] = (j >> 1 & 0x7f7f7f) + (ai[l] >> 1 & 0x7f7f7f);
				else
					l++;
				j = ai1[k++];
				if (j != 0)
					ai[l++] = (j >> 1 & 0x7f7f7f) + (ai[l] >> 1 & 0x7f7f7f);
				else
					l++;
			}

			for (int i3 = i1; i3 < 0; i3++) {
				j = ai1[k++];
				if (j != 0)
					ai[l++] = (j >> 1 & 0x7f7f7f) + (ai[l] >> 1 & 0x7f7f7f);
				else
					l++;
			}

			l += k1;
			k += l1;
		}

	}

	public void gf(int j, int k, int l, int i1, int j1) {
		int k1 = sj;
		int l1 = tj;
		if (wk == null) {
			wk = new int[512];
			for (int i2 = 0; i2 < 256; i2++) {
				wk[i2] = (int) (Math.sin((double) i2 * 0.02454369D) * 32768D);
				wk[i2 + 256] = (int) (Math.cos((double) i2 * 0.02454369D) * 32768D);
			}

		}
		int j2 = -jk[l] / 2;
		int k2 = -kk[l] / 2;
		if (lk[l]) {
			j2 += hk[l];
			k2 += ik[l];
		}
		int l2 = j2 + fk[l];
		int i3 = k2 + gk[l];
		int j3 = l2;
		int k3 = k2;
		int l3 = j2;
		int i4 = i3;
		i1 &= 0xff;
		int j4 = wk[i1] * j1;
		int k4 = wk[i1 + 256] * j1;
		int l4 = j + (k2 * j4 + j2 * k4 >> 22);
		int i5 = k + (k2 * k4 - j2 * j4 >> 22);
		int j5 = j + (k3 * j4 + j3 * k4 >> 22);
		int k5 = k + (k3 * k4 - j3 * j4 >> 22);
		int l5 = j + (i3 * j4 + l2 * k4 >> 22);
		int i6 = k + (i3 * k4 - l2 * j4 >> 22);
		int j6 = j + (i4 * j4 + l3 * k4 >> 22);
		int k6 = k + (i4 * k4 - l3 * j4 >> 22);
		int l6 = i5;
		int i7 = i5;
		if (k5 < l6)
			l6 = k5;
		else if (k5 > i7)
			i7 = k5;
		if (i6 < l6)
			l6 = i6;
		else if (i6 > i7)
			i7 = i6;
		if (k6 < l6)
			l6 = k6;
		else if (k6 > i7)
			i7 = k6;
		if (l6 < nk)
			l6 = nk;
		if (i7 > ok)
			i7 = ok;
		if (xk == null || xk.length != l1 + 1) {
			xk = new int[l1 + 1];
			yk = new int[l1 + 1];
			zk = new int[l1 + 1];
			al = new int[l1 + 1];
			bl = new int[l1 + 1];
			cl = new int[l1 + 1];
		}
		for (int j7 = l6; j7 <= i7; j7++) {
			xk[j7] = 0x5f5e0ff;
			yk[j7] = 0xfa0a1f01;
		}

		int j8 = 0;
		int l8 = 0;
		int j9 = 0;
		int k9 = fk[l];
		int l9 = gk[l];
		j2 = 0;
		k2 = 0;
		j3 = k9 - 1;
		k3 = 0;
		l2 = k9 - 1;
		i3 = l9 - 1;
		l3 = 0;
		i4 = l9 - 1;
		if (k6 != i5) {
			j8 = (j6 - l4 << 8) / (k6 - i5);
			j9 = (i4 - k2 << 8) / (k6 - i5);
		}
		int k7;
		int l7;
		int i8;
		int i9;
		if (i5 > k6) {
			i8 = j6 << 8;
			i9 = i4 << 8;
			k7 = k6;
			l7 = i5;
		} else {
			i8 = l4 << 8;
			i9 = k2 << 8;
			k7 = i5;
			l7 = k6;
		}
		if (k7 < 0) {
			i8 -= j8 * k7;
			i9 -= j9 * k7;
			k7 = 0;
		}
		if (l7 > l1 - 1)
			l7 = l1 - 1;
		for (int i10 = k7; i10 <= l7; i10++) {
			xk[i10] = yk[i10] = i8;
			i8 += j8;
			zk[i10] = al[i10] = 0;
			bl[i10] = cl[i10] = i9;
			i9 += j9;
		}

		if (k5 != i5) {
			j8 = (j5 - l4 << 8) / (k5 - i5);
			l8 = (j3 - j2 << 8) / (k5 - i5);
		}
		int k8;
		if (i5 > k5) {
			i8 = j5 << 8;
			k8 = j3 << 8;
			k7 = k5;
			l7 = i5;
		} else {
			i8 = l4 << 8;
			k8 = j2 << 8;
			k7 = i5;
			l7 = k5;
		}
		if (k7 < 0) {
			i8 -= j8 * k7;
			k8 -= l8 * k7;
			k7 = 0;
		}
		if (l7 > l1 - 1)
			l7 = l1 - 1;
		for (int j10 = k7; j10 <= l7; j10++) {
			if (i8 < xk[j10]) {
				xk[j10] = i8;
				zk[j10] = k8;
				bl[j10] = 0;
			}
			if (i8 > yk[j10]) {
				yk[j10] = i8;
				al[j10] = k8;
				cl[j10] = 0;
			}
			i8 += j8;
			k8 += l8;
		}

		if (i6 != k5) {
			j8 = (l5 - j5 << 8) / (i6 - k5);
			j9 = (i3 - k3 << 8) / (i6 - k5);
		}
		if (k5 > i6) {
			i8 = l5 << 8;
			k8 = l2 << 8;
			i9 = i3 << 8;
			k7 = i6;
			l7 = k5;
		} else {
			i8 = j5 << 8;
			k8 = j3 << 8;
			i9 = k3 << 8;
			k7 = k5;
			l7 = i6;
		}
		if (k7 < 0) {
			i8 -= j8 * k7;
			i9 -= j9 * k7;
			k7 = 0;
		}
		if (l7 > l1 - 1)
			l7 = l1 - 1;
		for (int k10 = k7; k10 <= l7; k10++) {
			if (i8 < xk[k10]) {
				xk[k10] = i8;
				zk[k10] = k8;
				bl[k10] = i9;
			}
			if (i8 > yk[k10]) {
				yk[k10] = i8;
				al[k10] = k8;
				cl[k10] = i9;
			}
			i8 += j8;
			i9 += j9;
		}

		if (k6 != i6) {
			j8 = (j6 - l5 << 8) / (k6 - i6);
			l8 = (l3 - l2 << 8) / (k6 - i6);
		}
		if (i6 > k6) {
			i8 = j6 << 8;
			k8 = l3 << 8;
			i9 = i4 << 8;
			k7 = k6;
			l7 = i6;
		} else {
			i8 = l5 << 8;
			k8 = l2 << 8;
			i9 = i3 << 8;
			k7 = i6;
			l7 = k6;
		}
		if (k7 < 0) {
			i8 -= j8 * k7;
			k8 -= l8 * k7;
			k7 = 0;
		}
		if (l7 > l1 - 1)
			l7 = l1 - 1;
		for (int l10 = k7; l10 <= l7; l10++) {
			if (i8 < xk[l10]) {
				xk[l10] = i8;
				zk[l10] = k8;
				bl[l10] = i9;
			}
			if (i8 > yk[l10]) {
				yk[l10] = i8;
				al[l10] = k8;
				cl[l10] = i9;
			}
			i8 += j8;
			k8 += l8;
		}

		int i11 = l6 * k1;
		int ai[] = ck[l];
		for (int j11 = l6; j11 < i7; j11++) {
			int k11 = xk[j11] >> 8;
			int l11 = yk[j11] >> 8;
			if (l11 - k11 <= 0) {
				i11 += k1;
			} else {
				int i12 = zk[j11] << 9;
				int j12 = ((al[j11] << 9) - i12) / (l11 - k11);
				int k12 = bl[j11] << 9;
				int l12 = ((cl[j11] << 9) - k12) / (l11 - k11);
				if (k11 < pk) {
					i12 += (pk - k11) * j12;
					k12 += (pk - k11) * l12;
					k11 = pk;
				}
				if (l11 > qk)
					l11 = qk;
				if (!rk || (j11 & 1) == 0)
					if (!lk[l])
						sf(yj, ai, 0, i11 + k11, i12, k12, j12, l12, k11 - l11, k9);
					else
						zf(yj, ai, 0, i11 + k11, i12, k12, j12, l12, k11 - l11, k9);
				i11 += k1;
			}
		}

	}

	private void sf(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2) {
		for (j = l1; j < 0; j++) {
			yj[k++] = ai1[(l >> 17) + (i1 >> 17) * i2];
			l += j1;
			i1 += k1;
		}

	}

	private void zf(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2) {
		for (int j2 = l1; j2 < 0; j2++) {
			j = ai1[(l >> 17) + (i1 >> 17) * i2];
			if (j != 0)
				yj[k++] = j;
			else
				k++;
			l += j1;
			i1 += k1;
		}

	}

	public void xf(int j, int k, int l, int i1, int j1, int k1) {
		try {
			int l1 = fk[j1];
			int i2 = gk[j1];
			int j2 = 0;
			int k2 = 0;
			int l2 = (l1 << 16) / l;
			int i3 = (i2 << 16) / i1;
			if (lk[j1]) {
				int j3 = jk[j1];
				int l3 = kk[j1];
				l2 = (j3 << 16) / l;
				i3 = (l3 << 16) / i1;
				j += ((hk[j1] * l + j3) - 1) / j3;
				k += ((ik[j1] * i1 + l3) - 1) / l3;
				if ((hk[j1] * l) % j3 != 0)
					j2 = (j3 - (hk[j1] * l) % j3 << 16) / l;
				if ((ik[j1] * i1) % l3 != 0)
					k2 = (l3 - (ik[j1] * i1) % l3 << 16) / i1;
				l = (l * (fk[j1] - (j2 >> 16))) / j3;
				i1 = (i1 * (gk[j1] - (k2 >> 16))) / l3;
			}
			int k3 = j + k * sj;
			int i4 = sj - l;
			if (k < nk) {
				int j4 = nk - k;
				i1 -= j4;
				k = 0;
				k3 += j4 * sj;
				k2 += i3 * j4;
			}
			if (k + i1 >= ok)
				i1 -= ((k + i1) - ok) + 1;
			if (j < pk) {
				int k4 = pk - j;
				l -= k4;
				j = 0;
				k3 += k4;
				j2 += l2 * k4;
				i4 += k4;
			}
			if (j + l >= qk) {
				int l4 = ((j + l) - qk) + 1;
				l -= l4;
				i4 += l4;
			}
			byte byte0 = 1;
			if (rk) {
				byte0 = 2;
				i4 += sj;
				i3 += i3;
				if ((k & 1) != 0) {
					k3 += sj;
					i1--;
				}
			}
			tf(yj, ck[j1], 0, j2, k2, k3, i4, l, i1, l2, i3, l1, byte0, k1);
			return;
		} catch (Exception _ex) {
			System.out.println("error in sprite clipping routine");
		}
	}

	private void tf(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2,
			int l2, int i3) {
		int i4 = i3 >> 16 & 0xff;
		int j4 = i3 >> 8 & 0xff;
		int k4 = i3 & 0xff;
		try {
			int l4 = k;
			for (int i5 = -l1; i5 < 0; i5 += l2) {
				int j5 = (l >> 16) * k2;
				for (int k5 = -k1; k5 < 0; k5++) {
					j = ai1[(k >> 16) + j5];
					if (j != 0) {
						int j3 = j >> 16 & 0xff;
						int k3 = j >> 8 & 0xff;
						int l3 = j & 0xff;
						if (j3 == k3 && k3 == l3)
							ai[i1++] = ((j3 * i4 >> 8) << 16) + ((k3 * j4 >> 8) << 8) + (l3 * k4 >> 8);
						else
							ai[i1++] = j;
					} else {
						i1++;
					}
					k += i2;
				}

				l += j2;
				k = l4;
				i1 += j1;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	public void lf(int j, int k, int l, int i1, int j1) {
		try {
			int k1 = fk[j1];
			int l1 = gk[j1];
			int i2 = 0;
			int j2 = 0;
			int k2 = (k1 << 16) / l;
			int l2 = (l1 << 16) / i1;
			if (lk[j1]) {
				int i3 = jk[j1];
				int k3 = kk[j1];
				k2 = (i3 << 16) / l;
				l2 = (k3 << 16) / i1;
				j += ((hk[j1] * l + i3) - 1) / i3;
				k += ((ik[j1] * i1 + k3) - 1) / k3;
				if ((hk[j1] * l) % i3 != 0)
					i2 = (i3 - (hk[j1] * l) % i3 << 16) / l;
				if ((ik[j1] * i1) % k3 != 0)
					j2 = (k3 - (ik[j1] * i1) % k3 << 16) / i1;
				l = (l * (fk[j1] - (i2 >> 16))) / i3;
				i1 = (i1 * (gk[j1] - (j2 >> 16))) / k3;
			}
			int j3 = j + k * sj;
			int l3 = sj - l;
			if (k < nk) {
				int i4 = nk - k;
				i1 -= i4;
				k = 0;
				j3 += i4 * sj;
				j2 += l2 * i4;
			}
			if (k + i1 >= ok)
				i1 -= ((k + i1) - ok) + 1;
			if (j < pk) {
				int j4 = pk - j;
				l -= j4;
				j = 0;
				j3 += j4;
				i2 += k2 * j4;
				l3 += j4;
			}
			if (j + l >= qk) {
				int k4 = ((j + l) - qk) + 1;
				l -= k4;
				l3 += k4;
			}
			byte byte0 = 1;
			if (rk) {
				byte0 = 2;
				l3 += sj;
				l2 += l2;
				if ((k & 1) != 0) {
					j3 += sj;
					i1--;
				}
			}
			if (lk[j1]) {
				bg(yj, ck[j1], 0, i2, j2, j3, l3, l, i1, k2, l2, k1, byte0);
				return;
			} else {
				hg(yj, ck[j1], j3, i2, j2, l3, l, i1, k2, l2, k1, byte0);
				return;
			}
		} catch (Exception _ex) {
			System.out.println("error in sprite clipping routine");
		}
	}

	private void hg(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2) {
		try {
			int l2 = k;
			for (int i3 = -k1; i3 < 0; i3 += k2) {
				int j3 = (l >> 16) * j2;
				for (int k3 = -j1; k3 < 0; k3++) {
					ai[j++] = ai1[(k >> 16) + j3];
					k += l1;
				}

				l += i2;
				k = l2;
				j += i1;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in sprite plot routine");
		}
	}

	private void bg(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2,
			int l2) {
		try {
			int i3 = k;
			for (int j3 = -l1; j3 < 0; j3 += l2) {
				int k3 = (l >> 16) * k2;
				for (int l3 = -k1; l3 < 0; l3++) {
					j = ai1[(k >> 16) + k3];
					if (j != 0)
						ai[i1++] = j;
					else
						i1++;
					k += i2;
				}

				l += j2;
				k = i3;
				i1 += j1;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	public void wg(int j, int k, int l, int i1, int j1, int k1) {
		int l1 = fk[j1];
		int i2 = gk[j1];
		int j2 = 0;
		int k2 = 0;
		int l2 = (l1 << 16) / l;
		int i3 = (i2 << 16) / i1;
		if (lk[j1]) {
			int j3 = jk[j1];
			int l3 = kk[j1];
			l2 = (j3 << 16) / l;
			i3 = (l3 << 16) / i1;
			j += ((hk[j1] * l + j3) - 1) / j3;
			k += ((ik[j1] * i1 + l3) - 1) / l3;
			if ((hk[j1] * l) % j3 != 0)
				j2 = (j3 - (hk[j1] * l) % j3 << 16) / l;
			if ((ik[j1] * i1) % l3 != 0)
				k2 = (l3 - (ik[j1] * i1) % l3 << 16) / i1;
			l = (l * (fk[j1] - (j2 >> 16))) / j3;
			i1 = (i1 * (gk[j1] - (k2 >> 16))) / l3;
		}
		int k3 = j + k * sj;
		int i4 = sj - l;
		if (k < nk) {
			int j4 = nk - k;
			i1 -= j4;
			k = 0;
			k3 += j4 * sj;
			k2 += i3 * j4;
		}
		if (k + i1 >= ok)
			i1 -= ((k + i1) - ok) + 1;
		if (j < pk) {
			int k4 = pk - j;
			l -= k4;
			j = 0;
			k3 += k4;
			j2 += l2 * k4;
			i4 += k4;
		}
		if (j + l >= qk) {
			int l4 = ((j + l) - qk) + 1;
			l -= l4;
			i4 += l4;
		}
		if (k1 == 128) {
			ng(yj, ck[j1], 0, j2, k2, k3, i4, l, i1, l2, i3, l1);
			return;
		} else {
			wf(yj, ck[j1], 0, j2, k2, k3, i4, l, i1, l2, i3, l1, k1);
			return;
		}
	}

	private void ng(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2) {
		int l2 = k;
		for (int i3 = -l1; i3 < 0; i3++) {
			int j3 = (l >> 16) * k2;
			for (int k3 = -k1; k3 < 0; k3++) {
				j = ai1[(k >> 16) + j3];
				if (j != 0)
					ai[i1++] = (j >>> 1 & 0x7f7f7f) + (ai[i1] >>> 1 & 0x7f7f7f);
				else
					i1++;
				k += i2;
			}

			l += j2;
			k = l2;
			i1 += j1;
		}

	}

	private void wf(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2,
			int l2) {
		int i3 = k;
		int j3 = 256 - l2;
		for (int i5 = -l1; i5 < 0; i5++) {
			int j5 = (l >> 16) * k2;
			for (int k5 = -k1; k5 < 0; k5++) {
				j = ai1[(k >> 16) + j5];
				if (j != 0) {
					int k3 = (j >> 16 & 0xff) * l2;
					int l3 = (j >> 8 & 0xff) * l2;
					int i4 = (j & 0xff) * l2;
					int j4 = (ai[i1] >> 16 & 0xff) * j3;
					int k4 = (ai[i1] >> 8 & 0xff) * j3;
					int l4 = (ai[i1] & 0xff) * j3;
					int l5 = ((k3 + j4 >> 8) << 16) + ((l3 + k4 >> 8) << 8) + (i4 + l4 >> 8);
					ai[i1++] = l5;
				} else {
					i1++;
				}
				k += i2;
			}

			l += j2;
			k = i3;
			i1 += j1;
		}

	}

	public void yf(int j, int k, int l, int i1, int j1, int k1, int l1) {
		lf(j, k, l, i1, j1);
	}

	public void pf(int j, int k, int l, int i1, int j1, int k1, int l1, int i2, boolean flag) {
		try {
			if (k1 == 0)
				k1 = 0xffffff;
			if (l1 == 0)
				l1 = 0xffffff;
			int j2 = fk[j1];
			int k2 = gk[j1];
			int l2 = 0;
			int i3 = 0;
			int j3 = i2 << 16;
			int k3 = (j2 << 16) / l;
			int l3 = (k2 << 16) / i1;
			int i4 = -(i2 << 16) / i1;
			if (lk[j1]) {
				int j4 = jk[j1];
				int l4 = kk[j1];
				k3 = (j4 << 16) / l;
				l3 = (l4 << 16) / i1;
				int k5 = hk[j1];
				int l5 = ik[j1];
				if (flag)
					k5 = j4 - fk[j1] - k5;
				j += ((k5 * l + j4) - 1) / j4;
				int i6 = ((l5 * i1 + l4) - 1) / l4;
				k += i6;
				j3 += i6 * i4;
				if ((k5 * l) % j4 != 0)
					l2 = (j4 - (k5 * l) % j4 << 16) / l;
				if ((l5 * i1) % l4 != 0)
					i3 = (l4 - (l5 * i1) % l4 << 16) / i1;
				l = ((((fk[j1] << 16) - l2) + k3) - 1) / k3;
				i1 = ((((gk[j1] << 16) - i3) + l3) - 1) / l3;
			}
			int k4 = k * sj;
			j3 += j << 16;
			if (k < nk) {
				int i5 = nk - k;
				i1 -= i5;
				k = nk;
				k4 += i5 * sj;
				i3 += l3 * i5;
				j3 += i4 * i5;
			}
			if (k + i1 >= ok)
				i1 -= ((k + i1) - ok) + 1;
			int j5 = k4 / sj & 1;
			if (!rk)
				j5 = 2;
			if (l1 == 0xffffff) {
				if (ck[j1] != null)
					if (!flag) {
						xg(yj, ck[j1], 0, l2, i3, k4, l, i1, k3, l3, j2, k1, j3, i4, j5);
						return;
					} else {
						xg(yj, ck[j1], 0, (fk[j1] << 16) - l2 - 1, i3, k4, l, i1, -k3, l3, j2, k1, j3, i4, j5);
						return;
					}
				if (!flag) {
					bf(yj, dk[j1], ek[j1], 0, l2, i3, k4, l, i1, k3, l3, j2, k1, j3, i4, j5);
					return;
				} else {
					bf(yj, dk[j1], ek[j1], 0, (fk[j1] << 16) - l2 - 1, i3, k4, l, i1, -k3, l3, j2, k1, j3, i4, j5);
					return;
				}
			}
			if (ck[j1] != null)
				if (!flag) {
					uf(yj, ck[j1], 0, l2, i3, k4, l, i1, k3, l3, j2, k1, l1, j3, i4, j5);
					return;
				} else {
					uf(yj, ck[j1], 0, (fk[j1] << 16) - l2 - 1, i3, k4, l, i1, -k3, l3, j2, k1, l1, j3, i4, j5);
					return;
				}
			if (!flag) {
				vg(yj, dk[j1], ek[j1], 0, l2, i3, k4, l, i1, k3, l3, j2, k1, l1, j3, i4, j5);
				return;
			} else {
				vg(yj, dk[j1], ek[j1], 0, (fk[j1] << 16) - l2 - 1, i3, k4, l, i1, -k3, l3, j2, k1, l1, j3, i4, j5);
				return;
			}
		} catch (Exception _ex) {
			System.out.println("error in sprite clipping routine");
		}
	}

	private void xg(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2,
			int l2, int i3, int j3) {
		int j4 = k2 >> 16 & 0xff;
		int k4 = k2 >> 8 & 0xff;
		int l4 = k2 & 0xff;
		try {
			int i5 = k;
			for (int j5 = -k1; j5 < 0; j5++) {
				int k5 = (l >> 16) * j2;
				int l5 = l2 >> 16;
				int i6 = j1;
				if (l5 < pk) {
					int j6 = pk - l5;
					i6 -= j6;
					l5 = pk;
					k += l1 * j6;
				}
				if (l5 + i6 >= qk) {
					int k6 = (l5 + i6) - qk;
					i6 -= k6;
				}
				j3 = 1 - j3;
				if (j3 != 0) {
					for (int l6 = l5; l6 < l5 + i6; l6++) {
						j = ai1[(k >> 16) + k5];
						if (j != 0) {
							int k3 = j >> 16 & 0xff;
							int l3 = j >> 8 & 0xff;
							int i4 = j & 0xff;
							if (k3 == l3 && l3 == i4)
								ai[l6 + i1] = ((k3 * j4 >> 8) << 16) + ((l3 * k4 >> 8) << 8) + (i4 * l4 >> 8);
							else
								ai[l6 + i1] = j;
						}
						k += l1;
					}

				}
				l += i2;
				k = i5;
				i1 += sj;
				l2 += i3;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	private void uf(int ai[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2,
			int l2, int i3, int j3, int k3) {
		int k4 = k2 >> 16 & 0xff;
		int l4 = k2 >> 8 & 0xff;
		int i5 = k2 & 0xff;
		int j5 = l2 >> 16 & 0xff;
		int k5 = l2 >> 8 & 0xff;
		int l5 = l2 & 0xff;
		try {
			int i6 = k;
			for (int j6 = -k1; j6 < 0; j6++) {
				int k6 = (l >> 16) * j2;
				int l6 = i3 >> 16;
				int i7 = j1;
				if (l6 < pk) {
					int j7 = pk - l6;
					i7 -= j7;
					l6 = pk;
					k += l1 * j7;
				}
				if (l6 + i7 >= qk) {
					int k7 = (l6 + i7) - qk;
					i7 -= k7;
				}
				k3 = 1 - k3;
				if (k3 != 0) {
					for (int l7 = l6; l7 < l6 + i7; l7++) {
						j = ai1[(k >> 16) + k6];
						if (j != 0) {
							int l3 = j >> 16 & 0xff;
							int i4 = j >> 8 & 0xff;
							int j4 = j & 0xff;
							if (l3 == i4 && i4 == j4)
								ai[l7 + i1] = ((l3 * k4 >> 8) << 16) + ((i4 * l4 >> 8) << 8) + (j4 * i5 >> 8);
							else if (l3 == 255 && i4 == j4)
								ai[l7 + i1] = ((l3 * j5 >> 8) << 16) + ((i4 * k5 >> 8) << 8) + (j4 * l5 >> 8);
							else
								ai[l7 + i1] = j;
						}
						k += l1;
					}

				}
				l += i2;
				k = i6;
				i1 += sj;
				i3 += j3;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	private void bf(int ai[], byte abyte0[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2,
			int j2, int k2, int l2, int i3, int j3) {
		int j4 = k2 >> 16 & 0xff;
		int k4 = k2 >> 8 & 0xff;
		int l4 = k2 & 0xff;
		try {
			int i5 = k;
			for (int j5 = -k1; j5 < 0; j5++) {
				int k5 = (l >> 16) * j2;
				int l5 = l2 >> 16;
				int i6 = j1;
				if (l5 < pk) {
					int j6 = pk - l5;
					i6 -= j6;
					l5 = pk;
					k += l1 * j6;
				}
				if (l5 + i6 >= qk) {
					int k6 = (l5 + i6) - qk;
					i6 -= k6;
				}
				j3 = 1 - j3;
				if (j3 != 0) {
					for (int l6 = l5; l6 < l5 + i6; l6++) {
						j = abyte0[(k >> 16) + k5] & 0xff;
						if (j != 0) {
							j = ai1[j];
							int k3 = j >> 16 & 0xff;
							int l3 = j >> 8 & 0xff;
							int i4 = j & 0xff;
							if (k3 == l3 && l3 == i4)
								ai[l6 + i1] = ((k3 * j4 >> 8) << 16) + ((l3 * k4 >> 8) << 8) + (i4 * l4 >> 8);
							else
								ai[l6 + i1] = j;
						}
						k += l1;
					}

				}
				l += i2;
				k = i5;
				i1 += sj;
				l2 += i3;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	private void vg(int ai[], byte abyte0[], int ai1[], int j, int k, int l, int i1, int j1, int k1, int l1, int i2,
			int j2, int k2, int l2, int i3, int j3, int k3) {
		int k4 = k2 >> 16 & 0xff;
		int l4 = k2 >> 8 & 0xff;
		int i5 = k2 & 0xff;
		int j5 = l2 >> 16 & 0xff;
		int k5 = l2 >> 8 & 0xff;
		int l5 = l2 & 0xff;
		try {
			int i6 = k;
			for (int j6 = -k1; j6 < 0; j6++) {
				int k6 = (l >> 16) * j2;
				int l6 = i3 >> 16;
				int i7 = j1;
				if (l6 < pk) {
					int j7 = pk - l6;
					i7 -= j7;
					l6 = pk;
					k += l1 * j7;
				}
				if (l6 + i7 >= qk) {
					int k7 = (l6 + i7) - qk;
					i7 -= k7;
				}
				k3 = 1 - k3;
				if (k3 != 0) {
					for (int l7 = l6; l7 < l6 + i7; l7++) {
						j = abyte0[(k >> 16) + k6] & 0xff;
						if (j != 0) {
							j = ai1[j];
							int l3 = j >> 16 & 0xff;
							int i4 = j >> 8 & 0xff;
							int j4 = j & 0xff;
							if (l3 == i4 && i4 == j4)
								ai[l7 + i1] = ((l3 * k4 >> 8) << 16) + ((i4 * l4 >> 8) << 8) + (j4 * i5 >> 8);
							else if (l3 == 255 && i4 == j4)
								ai[l7 + i1] = ((l3 * j5 >> 8) << 16) + ((i4 * k5 >> 8) << 8) + (j4 * l5 >> 8);
							else
								ai[l7 + i1] = j;
						}
						k += l1;
					}

				}
				l += i2;
				k = i6;
				i1 += sj;
				i3 += j3;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	public static int ye(byte abyte0[]) {
		sk[uk] = abyte0;
		return uk++;
	}

	public void sg(String s, int j, int k, int l, int i1) {
		ef(s, j - xe(s, l), k, l, i1);
	}

	public void mg(String s, int j, int k, int l, int i1) {
		ef(s, j - xe(s, l) / 2, k, l, i1);
	}

	public void ug(String s, int j, int k, int l, int i1, int j1) {
		int k1 = 0;
		byte abyte0[] = sk[l];
		int l1 = 0;
		int i2 = 0;
		for (int j2 = 0; j2 < s.length(); j2++) {
			if (s.charAt(j2) == '@' && j2 + 4 < s.length() && s.charAt(j2 + 4) == '@')
				j2 += 4;
			else if (s.charAt(j2) == '~' && j2 + 4 < s.length() && s.charAt(j2 + 4) == '~')
				j2 += 4;
			else
				k1 += abyte0[tk[s.charAt(j2)] + 7];
			if (s.charAt(j2) == ' ')
				i2 = j2;
			if (k1 > j1) {
				if (i2 <= l1)
					i2 = j2;
				mg(s.substring(l1, i2), j, k, l, i1);
				k1 = 0;
				l1 = j2 = i2 + 1;
				k += ig(l);
			}
		}

		if (k1 > 0)
			mg(s.substring(l1), j, k, l, i1);
	}

	public void ef(String s, int j, int k, int l, int i1) {
		byte abyte0[] = sk[l];
		for (int j1 = 0; j1 < s.length(); j1++)
			if (s.charAt(j1) == '@' && j1 + 4 < s.length() && s.charAt(j1 + 4) == '@') {
				if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("red"))
					i1 = 0xff0000;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("lre"))
					i1 = 0xff9040;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("yel"))
					i1 = 0xffff00;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("gre"))
					i1 = 65280;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("blu"))
					i1 = 255;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("cya"))
					i1 = 65535;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("mag"))
					i1 = 0xff00ff;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("whi"))
					i1 = 0xffffff;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("bla"))
					i1 = 0;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("dre"))
					i1 = 0xc00000;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("ora"))
					i1 = 0xff9040;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("ran"))
					i1 = (int) (Math.random() * 16777215D);
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("or1"))
					i1 = 0xffb000;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("or2"))
					i1 = 0xff7000;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("or3"))
					i1 = 0xff3000;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("gr1"))
					i1 = 0xc0ff00;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("gr2"))
					i1 = 0x80ff00;
				else if (s.substring(j1 + 1, j1 + 4).equalsIgnoreCase("gr3"))
					i1 = 0x40ff00;
				j1 += 4;
			} else if (s.charAt(j1) == '~' && j1 + 4 < s.length() && s.charAt(j1 + 4) == '~') {
				char c = s.charAt(j1 + 1);
				char c1 = s.charAt(j1 + 2);
				char c2 = s.charAt(j1 + 3);
				if (c >= '0' && c <= '9' && c1 >= '0' && c1 <= '9' && c2 >= '0' && c2 <= '9')
					j = Integer.parseInt(s.substring(j1 + 1, j1 + 4));
				j1 += 4;
			} else {
				int k1 = tk[s.charAt(j1)];
				if (vk && i1 != 0)
					lg(k1, j + 1, k, 0, abyte0);
				if (vk && i1 != 0)
					lg(k1, j, k + 1, 0, abyte0);
				lg(k1, j, k, i1, abyte0);
				j += abyte0[k1 + 7];
			}

	}

	private void lg(int j, int k, int l, int i1, byte abyte0[]) {
		int j1 = k + abyte0[j + 5];
		int k1 = l - abyte0[j + 6];
		int l1 = abyte0[j + 3];
		int i2 = abyte0[j + 4];
		int j2 = abyte0[j] * 16384 + abyte0[j + 1] * 128 + abyte0[j + 2];
		int k2 = j1 + k1 * sj;
		int l2 = sj - l1;
		int i3 = 0;
		if (k1 < nk) {
			int j3 = nk - k1;
			i2 -= j3;
			k1 = nk;
			j2 += j3 * l1;
			k2 += j3 * sj;
		}
		if (k1 + i2 >= ok)
			i2 -= ((k1 + i2) - ok) + 1;
		if (j1 < pk) {
			int k3 = pk - j1;
			l1 -= k3;
			j1 = pk;
			j2 += k3;
			k2 += k3;
			i3 += k3;
			l2 += k3;
		}
		if (j1 + l1 >= qk) {
			int l3 = ((j1 + l1) - qk) + 1;
			l1 -= l3;
			i3 += l3;
			l2 += l3;
		}
		if (l1 > 0 && i2 > 0)
			we(yj, abyte0, i1, j2, k2, l1, i2, l2, i3);
	}

	private void we(int ai[], byte abyte0[], int j, int k, int l, int i1, int j1, int k1, int l1) {
		int i2 = -(i1 >> 2);
		i1 = -(i1 & 3);
		for (int j2 = -j1; j2 < 0; j2++) {
			for (int k2 = i2; k2 < 0; k2++) {
				if (abyte0[k++] != 0)
					ai[l++] = j;
				else
					l++;
				if (abyte0[k++] != 0)
					ai[l++] = j;
				else
					l++;
				if (abyte0[k++] != 0)
					ai[l++] = j;
				else
					l++;
				if (abyte0[k++] != 0)
					ai[l++] = j;
				else
					l++;
			}

			for (int l2 = i1; l2 < 0; l2++)
				if (abyte0[k++] != 0)
					ai[l++] = j;
				else
					l++;

			l += k1;
			k += l1;
		}

	}

	public int ig(int j) {
		if (j == 0)
			return sk[j][8] - 2;
		else
			return sk[j][8] - 1;
	}

	public int xe(String s, int j) {
		int k = 0;
		byte abyte0[] = sk[j];
		for (int l = 0; l < s.length(); l++)
			if (s.charAt(l) == '@' && l + 4 < s.length() && s.charAt(l + 4) == '@')
				l += 4;
			else if (s.charAt(l) == '~' && l + 4 < s.length() && s.charAt(l + 4) == '~')
				l += 4;
			else
				k += abyte0[tk[s.charAt(l)] + 7];

		return k;
	}

	public boolean imageUpdate(Image image, int j, int k, int l, int i1, int j1) {
		return true;
	}

	public int sj;
	public int tj;
	public int uj;
	public int vj;
	public int wj;
	ColorModel xj;
	public int yj[];
	ImageConsumer zj;
	public Image bk;
	private int ck[][];
	private byte dk[][];
	private int ek[][];
	public int fk[];
	public int gk[];
	public int hk[];
	public int ik[];
	public int jk[];
	public int kk[];
	public boolean lk[];
	private boolean mk;
	private int nk;
	private int ok;
	private int pk;
	private int qk;
	public boolean rk;
	static byte sk[][] = new byte[50][];
	static int tk[];
	static int uk;
	public boolean vk;
	int wk[];
	int xk[];
	int yk[];
	int zk[];
	int al[];
	int bl[];
	int cl[];
	public static final int dl = 0;
	public static final int el = 0xffffff;
	public static final int fl = 0xff0000;
	public static final int gl = 0xc00000;
	public static final int hl = 65280;
	public static final int il = 255;
	public static final int jl = 0xffff00;
	public static final int kl = 65535;
	public static final int ll = 0xff00ff;
	public static final int ml = 0xc0c0c0;
	public static final int nl = 0x808080;
	public static final int ol = 0x404040;
	public static final int pl = 0;
	public static final int ql = 1;
	public static final int rl = 2;
	public static final int sl = 3;
	public static final int tl = 4;
	public static final int ul = 5;
	public static final int vl = 6;
	public static final int wl = 7;

	static {
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\243$%^&*()-_=+[{]};:'@#~,<.>/?\\| ";
		tk = new int[256];
		for (int j = 0; j < 256; j++) {
			int k = characters.indexOf(j);
			if (k == -1)
				k = 74;
			tk[j] = k * 9;
		}

	}
}
