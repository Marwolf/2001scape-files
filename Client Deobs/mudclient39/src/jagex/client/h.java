package jagex.client;

import jagex.o;
import java.io.*;

public class h {

	public h(int i, int k) {
		eh = 1;
		fh = true;
		mh = true;
		nh = false;
		oh = false;
		ph = -1;
		sh = false;
		th = false;
		uh = false;
		vh = false;
		wh = false;
		ci = 0xbc614e;
		ij = 0xbc614e;
		jj = 180;
		kj = 155;
		lj = 95;
		mj = 256;
		nj = 512;
		oj = 32;
		td(i, k);
		li = new int[k][1];
		for (int l = 0; l < k; l++)
			li[l][0] = l;

	}

	public h(int i, int k, boolean flag, boolean flag1, boolean flag2, boolean flag3, boolean flag4) {
		eh = 1;
		fh = true;
		mh = true;
		nh = false;
		oh = false;
		ph = -1;
		sh = false;
		th = false;
		uh = false;
		vh = false;
		wh = false;
		ci = 0xbc614e;
		ij = 0xbc614e;
		jj = 180;
		kj = 155;
		lj = 95;
		mj = 256;
		nj = 512;
		oj = 32;
		sh = flag;
		th = flag1;
		uh = flag2;
		vh = flag3;
		wh = flag4;
		td(i, k);
	}

	private void td(int i, int k) {
		ei = new int[i];
		fi = new int[i];
		gi = new int[i];
		pg = new int[i];
		qg = new byte[i];
		sg = new int[k];
		tg = new int[k][];
		ug = new int[k];
		vg = new int[k];
		yg = new int[k];
		xg = new int[k];
		wg = new int[k];
		if (!wh) {
			kg = new int[i];
			lg = new int[i];
			mg = new int[i];
			ng = new int[i];
			og = new int[i];
		}
		if (!vh) {
			rh = new byte[k];
			qh = new int[k];
		}
		if (sh) {
			hi = ei;
			ii = fi;
			ji = gi;
		} else {
			hi = new int[i];
			ii = new int[i];
			ji = new int[i];
		}
		if (!uh || !th) {
			zg = new int[k];
			ah = new int[k];
			bh = new int[k];
		}
		if (!th) {
			mi = new int[k];
			ni = new int[k];
			oi = new int[k];
			pi = new int[k];
			qi = new int[k];
			ri = new int[k];
		}
		rg = 0;
		jg = 0;
		di = i;
		ki = k;
		si = ti = ui = 0;
		vi = wi = xi = 0;
		yi = zi = aj = 256;
		bj = cj = dj = ej = fj = gj = 256;
		hj = 0;
	}

	public void oe() {
		kg = new int[jg];
		lg = new int[jg];
		mg = new int[jg];
		ng = new int[jg];
		og = new int[jg];
	}

	public void re() {
		rg = 0;
		jg = 0;
	}

	public void ge(int i, int k) {
		rg -= i;
		if (rg < 0)
			rg = 0;
		jg -= k;
		if (jg < 0)
			jg = 0;
	}

	public h(byte abyte0[], int i) {
		eh = 1;
		fh = true;
		mh = true;
		nh = false;
		oh = false;
		ph = -1;
		sh = false;
		th = false;
		uh = false;
		vh = false;
		wh = false;
		ci = 0xbc614e;
		ij = 0xbc614e;
		jj = 180;
		kj = 155;
		lj = 95;
		mj = 256;
		nj = 512;
		oj = 32;
		pj = abyte0;
		qj = i;
		me(pj);
		int k = me(pj);
		int l = me(pj);
		td(k, l);
		li = new int[l][];
		for (int i3 = 0; i3 < k; i3++) {
			int i1 = me(pj);
			int j1 = me(pj);
			int k1 = me(pj);
			je(i1, j1, k1);
		}

		for (int j3 = 0; j3 < l; j3++) {
			int l1 = me(pj);
			int i2 = me(pj);
			int j2 = me(pj);
			int k2 = me(pj);
			nj = me(pj);
			oj = me(pj);
			int l2 = me(pj);
			int ai1[] = new int[l1];
			for (int k3 = 0; k3 < l1; k3++)
				ai1[k3] = me(pj);

			int ai2[] = new int[k2];
			for (int l3 = 0; l3 < k2; l3++)
				ai2[l3] = me(pj);

			int i4 = ie(l1, ai1, i2, j2);
			li[j3] = ai2;
			if (l2 == 0)
				yg[i4] = 0;
			else
				yg[i4] = ci;
		}

		eh = 1;
	}

	public h(String s) {
		eh = 1;
		fh = true;
		mh = true;
		nh = false;
		oh = false;
		ph = -1;
		sh = false;
		th = false;
		uh = false;
		vh = false;
		wh = false;
		ci = 0xbc614e;
		ij = 0xbc614e;
		jj = 180;
		kj = 155;
		lj = 95;
		mj = 256;
		nj = 512;
		oj = 32;
		byte abyte0[] = null;
		try {
			java.io.InputStream inputstream = o.km(s);
			DataInputStream datainputstream = new DataInputStream(inputstream);
			abyte0 = new byte[3];
			qj = 0;
			for (int i = 0; i < 3; i += datainputstream.read(abyte0, i, 3 - i))
				;
			int l = me(abyte0);
			abyte0 = new byte[l];
			qj = 0;
			for (int k = 0; k < l; k += datainputstream.read(abyte0, k, l - k))
				;
			datainputstream.close();
		} catch (IOException _ex) {
			jg = 0;
			rg = 0;
			return;
		}
		int i1 = me(abyte0);
		int j1 = me(abyte0);
		td(i1, j1);
		li = new int[j1][];
		for (int k3 = 0; k3 < i1; k3++) {
			int k1 = me(abyte0);
			int l1 = me(abyte0);
			int i2 = me(abyte0);
			je(k1, l1, i2);
		}

		for (int l3 = 0; l3 < j1; l3++) {
			int j2 = me(abyte0);
			int k2 = me(abyte0);
			int l2 = me(abyte0);
			int i3 = me(abyte0);
			nj = me(abyte0);
			oj = me(abyte0);
			int j3 = me(abyte0);
			int ai1[] = new int[j2];
			for (int i4 = 0; i4 < j2; i4++)
				ai1[i4] = me(abyte0);

			int ai2[] = new int[i3];
			for (int j4 = 0; j4 < i3; j4++)
				ai2[j4] = me(abyte0);

			int k4 = ie(j2, ai1, k2, l2);
			li[l3] = ai2;
			if (j3 == 0)
				yg[k4] = 0;
			else
				yg[k4] = ci;
		}

		eh = 1;
	}

	public h(h ah1[], int i) {
		eh = 1;
		fh = true;
		mh = true;
		nh = false;
		oh = false;
		ph = -1;
		sh = false;
		th = false;
		uh = false;
		vh = false;
		wh = false;
		ci = 0xbc614e;
		ij = 0xbc614e;
		jj = 180;
		kj = 155;
		lj = 95;
		mj = 256;
		nj = 512;
		oj = 32;
		be(ah1, i, true);
	}

	public void be(h ah1[], int i, boolean flag) {
		int k = 0;
		int l = 0;
		for (int i1 = 0; i1 < i; i1++) {
			k += ah1[i1].rg;
			l += ah1[i1].jg;
		}

		td(l, k);
		if (flag)
			li = new int[k][];
		for (int j1 = 0; j1 < i; j1++) {
			h h1 = ah1[j1];
			h1.ee();
			oj = h1.oj;
			nj = h1.nj;
			jj = h1.jj;
			kj = h1.kj;
			lj = h1.lj;
			mj = h1.mj;
			for (int k1 = 0; k1 < h1.rg; k1++) {
				int ai1[] = new int[h1.sg[k1]];
				int ai2[] = h1.tg[k1];
				for (int l1 = 0; l1 < h1.sg[k1]; l1++)
					ai1[l1] = je(h1.ei[ai2[l1]], h1.fi[ai2[l1]], h1.gi[ai2[l1]]);

				int i2 = ie(h1.sg[k1], ai1, h1.ug[k1], h1.vg[k1]);
				yg[i2] = h1.yg[k1];
				xg[i2] = h1.xg[k1];
				wg[i2] = h1.wg[k1];
				if (flag)
					if (i > 1) {
						li[i2] = new int[h1.li[k1].length + 1];
						li[i2][0] = j1;
						for (int j2 = 0; j2 < h1.li[k1].length; j2++)
							li[i2][j2 + 1] = h1.li[k1][j2];

					} else {
						li[i2] = new int[h1.li[k1].length];
						for (int k2 = 0; k2 < h1.li[k1].length; k2++)
							li[i2][k2] = h1.li[k1][k2];

					}
			}

		}

		eh = 1;
	}

	public int je(int i, int k, int l) {
		for (int i1 = 0; i1 < jg; i1++)
			if (ei[i1] == i && fi[i1] == k && gi[i1] == l)
				return i1;

		if (jg >= di) {
			return -1;
		} else {
			ei[jg] = i;
			fi[jg] = k;
			gi[jg] = l;
			return jg++;
		}
	}

	public int zd(int i, int k, int l) {
		if (jg >= di) {
			return -1;
		} else {
			ei[jg] = i;
			fi[jg] = k;
			gi[jg] = l;
			return jg++;
		}
	}

	public int ie(int i, int ai1[], int k, int l) {
		if (rg >= ki) {
			return -1;
		} else {
			sg[rg] = i;
			tg[rg] = ai1;
			ug[rg] = k;
			vg[rg] = l;
			eh = 1;
			return rg++;
		}
	}

	public h[] rd(int i, int k, int l, int i1, int j1, int k1, int l1, boolean flag) {
		ee();
		int ai1[] = new int[k1];
		int ai2[] = new int[k1];
		for (int i2 = 0; i2 < k1; i2++) {
			ai1[i2] = 0;
			ai2[i2] = 0;
		}

		for (int j2 = 0; j2 < rg; j2++) {
			int k2 = 0;
			int l2 = 0;
			int j3 = sg[j2];
			int ai3[] = tg[j2];
			for (int j4 = 0; j4 < j3; j4++) {
				k2 += ei[ai3[j4]];
				l2 += gi[ai3[j4]];
			}

			int l4 = k2 / (j3 * l) + (l2 / (j3 * i1)) * j1;
			ai1[l4] += j3;
			ai2[l4]++;
		}

		h ah1[] = new h[k1];
		for (int i3 = 0; i3 < k1; i3++) {
			if (ai1[i3] > l1)
				ai1[i3] = l1;
			ah1[i3] = new h(ai1[i3], ai2[i3], true, true, true, flag, true);
			ah1[i3].nj = nj;
			ah1[i3].oj = oj;
		}

		for (int k3 = 0; k3 < rg; k3++) {
			int l3 = 0;
			int k4 = 0;
			int i5 = sg[k3];
			int ai4[] = tg[k3];
			for (int j5 = 0; j5 < i5; j5++) {
				l3 += ei[ai4[j5]];
				k4 += gi[ai4[j5]];
			}

			int k5 = l3 / (i5 * l) + (k4 / (i5 * i1)) * j1;
			te(ah1[k5], ai4, i5, k3);
		}

		for (int i4 = 0; i4 < k1; i4++)
			ah1[i4].oe();

		return ah1;
	}

	public void te(h h1, int ai1[], int i, int k) {
		int ai2[] = new int[i];
		for (int l = 0; l < i; l++) {
			int i1 = ai2[l] = h1.je(ei[ai1[l]], fi[ai1[l]], gi[ai1[l]]);
			h1.pg[i1] = pg[ai1[l]];
			h1.qg[i1] = qg[ai1[l]];
		}

		int j1 = h1.ie(i, ai2, ug[k], vg[k]);
		if (!h1.vh && !vh)
			h1.qh[j1] = qh[k];
		h1.yg[j1] = yg[k];
		h1.xg[j1] = xg[k];
		h1.wg[j1] = wg[k];
	}

	public void ne(boolean flag, int i, int k, int l, int i1, int j1) {
		oj = 256 - i * 4;
		nj = (64 - k) * 16 + 128;
		if (uh)
			return;
		for (int k1 = 0; k1 < rg; k1++)
			if (flag)
				yg[k1] = ci;
			else
				yg[k1] = 0;

		jj = l;
		kj = i1;
		lj = j1;
		mj = (int) Math.sqrt(l * l + i1 * i1 + j1 * j1);
		he();
	}

	public void se(int i, int k, int l) {
		if (uh) {
			return;
		} else {
			jj = i;
			kj = k;
			lj = l;
			mj = (int) Math.sqrt(i * i + k * k + l * l);
			he();
			return;
		}
	}

	public void ud(int i, int k) {
		qg[i] = (byte) k;
	}

	public void pe(int i, int k, int l) {
		vi = vi + i & 0xff;
		wi = wi + k & 0xff;
		xi = xi + l & 0xff;
		qe();
		eh = 1;
	}

	public void wd(int i, int k, int l) {
		si += i;
		ti += k;
		ui += l;
		qe();
		eh = 1;
	}

	public void ce(int i, int k, int l) {
		si = i;
		ti = k;
		ui = l;
		qe();
		eh = 1;
	}

	public int ue() {
		return si;
	}

	public void sd(int i, int k, int l) {
		yi = i;
		zi = k;
		aj = l;
		qe();
		eh = 1;
	}

	public void xd(int i, int k, int l, int i1, int j1, int k1) {
		bj = i;
		cj = k;
		dj = l;
		ej = i1;
		fj = j1;
		gj = k1;
		qe();
		eh = 1;
	}

	private void qe() {
		if (bj != 256 || cj != 256 || dj != 256 || ej != 256 || fj != 256 || gj != 256) {
			hj = 4;
			return;
		}
		if (yi != 256 || zi != 256 || aj != 256) {
			hj = 3;
			return;
		}
		if (vi != 0 || wi != 0 || xi != 0) {
			hj = 2;
			return;
		}
		if (si != 0 || ti != 0 || ui != 0) {
			hj = 1;
			return;
		} else {
			hj = 0;
			return;
		}
	}

	private void ve(int i, int k, int l) {
		for (int i1 = 0; i1 < jg; i1++) {
			hi[i1] += i;
			ii[i1] += k;
			ji[i1] += l;
		}

	}

	private void ae(int i, int k, int l) {
		for (int j3 = 0; j3 < jg; j3++) {
			if (l != 0) {
				int i1 = xh[l];
				int l1 = xh[l + 256];
				int k2 = ii[j3] * i1 + hi[j3] * l1 >> 15;
				ii[j3] = ii[j3] * l1 - hi[j3] * i1 >> 15;
				hi[j3] = k2;
			}
			if (i != 0) {
				int j1 = xh[i];
				int i2 = xh[i + 256];
				int l2 = ii[j3] * i2 - ji[j3] * j1 >> 15;
				ji[j3] = ii[j3] * j1 + ji[j3] * i2 >> 15;
				ii[j3] = l2;
			}
			if (k != 0) {
				int k1 = xh[k];
				int j2 = xh[k + 256];
				int i3 = ji[j3] * k1 + hi[j3] * j2 >> 15;
				ji[j3] = ji[j3] * j2 - hi[j3] * k1 >> 15;
				hi[j3] = i3;
			}
		}

	}

	private void yd(int i, int k, int l, int i1, int j1, int k1) {
		for (int l1 = 0; l1 < jg; l1++) {
			if (i != 0)
				hi[l1] += ii[l1] * i >> 8;
			if (k != 0)
				ji[l1] += ii[l1] * k >> 8;
			if (l != 0)
				hi[l1] += ji[l1] * l >> 8;
			if (i1 != 0)
				ii[l1] += ji[l1] * i1 >> 8;
			if (j1 != 0)
				ji[l1] += hi[l1] * j1 >> 8;
			if (k1 != 0)
				ii[l1] += hi[l1] * k1 >> 8;
		}

	}

	private void fe(int i, int k, int l) {
		for (int i1 = 0; i1 < jg; i1++) {
			hi[i1] = hi[i1] * i >> 8;
			ii[i1] = ii[i1] * k >> 8;
			ji[i1] = ji[i1] * l >> 8;
		}

	}

	private void qd() {
		gh = ih = kh = 0xf423f;
		ij = hh = jh = lh = 0xfff0bdc1;
		for (int i = 0; i < rg; i++) {
			int ai1[] = tg[i];
			int l = ai1[0];
			int j1 = sg[i];
			int k1;
			int l1 = k1 = hi[l];
			int i2;
			int j2 = i2 = ii[l];
			int k2;
			int l2 = k2 = ji[l];
			for (int k = 0; k < j1; k++) {
				int i1 = ai1[k];
				if (hi[i1] < k1)
					k1 = hi[i1];
				else if (hi[i1] > l1)
					l1 = hi[i1];
				if (ii[i1] < i2)
					i2 = ii[i1];
				else if (ii[i1] > j2)
					j2 = ii[i1];
				if (ji[i1] < k2)
					k2 = ji[i1];
				else if (ji[i1] > l2)
					l2 = ji[i1];
			}

			if (!th) {
				mi[i] = k1;
				ni[i] = l1;
				oi[i] = i2;
				pi[i] = j2;
				qi[i] = k2;
				ri[i] = l2;
			}
			if (l1 - k1 > ij)
				ij = l1 - k1;
			if (j2 - i2 > ij)
				ij = j2 - i2;
			if (l2 - k2 > ij)
				ij = l2 - k2;
			if (k1 < gh)
				gh = k1;
			if (l1 > hh)
				hh = l1;
			if (i2 < ih)
				ih = i2;
			if (j2 > jh)
				jh = j2;
			if (k2 < kh)
				kh = k2;
			if (l2 > lh)
				lh = l2;
		}

	}

	public void he() {
		if (uh)
			return;
		int i = nj * mj >> 8;
		for (int k = 0; k < rg; k++)
			if (yg[k] != ci)
				yg[k] = (zg[k] * jj + ah[k] * kj + bh[k] * lj) / i;

		int ai1[] = new int[jg];
		int ai2[] = new int[jg];
		int ai3[] = new int[jg];
		int ai4[] = new int[jg];
		for (int l = 0; l < jg; l++) {
			ai1[l] = 0;
			ai2[l] = 0;
			ai3[l] = 0;
			ai4[l] = 0;
		}

		for (int i1 = 0; i1 < rg; i1++)
			if (yg[i1] == ci) {
				for (int j1 = 0; j1 < sg[i1]; j1++) {
					int l1 = tg[i1][j1];
					ai1[l1] += zg[i1];
					ai2[l1] += ah[i1];
					ai3[l1] += bh[i1];
					ai4[l1]++;
				}

			}

		for (int k1 = 0; k1 < jg; k1++)
			if (ai4[k1] > 0)
				pg[k1] = (ai1[k1] * jj + ai2[k1] * kj + ai3[k1] * lj) / (i * ai4[k1]);

	}

	public void ke() {
		if (uh && th)
			return;
		for (int i = 0; i < rg; i++) {
			int ai1[] = tg[i];
			int k = hi[ai1[0]];
			int l = ii[ai1[0]];
			int i1 = ji[ai1[0]];
			int j1 = hi[ai1[1]] - k;
			int k1 = ii[ai1[1]] - l;
			int l1 = ji[ai1[1]] - i1;
			int i2 = hi[ai1[2]] - k;
			int j2 = ii[ai1[2]] - l;
			int k2 = ji[ai1[2]] - i1;
			int l2 = k1 * k2 - j2 * l1;
			int i3 = l1 * i2 - k2 * j1;
			int j3;
			for (j3 = j1 * j2 - i2 * k1; l2 > 8192 || i3 > 8192 || j3 > 8192 || l2 < -8192 || i3 < -8192
					|| j3 < -8192; j3 >>= 1) {
				l2 >>= 1;
				i3 >>= 1;
			}

			int k3 = (int) (256D * Math.sqrt(l2 * l2 + i3 * i3 + j3 * j3));
			if (k3 <= 0)
				k3 = 1;
			zg[i] = (l2 * 0x10000) / k3;
			ah[i] = (i3 * 0x10000) / k3;
			bh[i] = (j3 * 65535) / k3;
			xg[i] = -1;
		}

		he();
	}

	public void pd() {
		if (eh == 2) {
			eh = 0;
			for (int i = 0; i < jg; i++) {
				hi[i] = ei[i];
				ii[i] = fi[i];
				ji[i] = gi[i];
			}

			gh = ih = kh = 0xff676981;
			ij = hh = jh = lh = 0x98967f;
			return;
		}
		if (eh == 1) {
			eh = 0;
			for (int k = 0; k < jg; k++) {
				hi[k] = ei[k];
				ii[k] = fi[k];
				ji[k] = gi[k];
			}

			if (hj >= 2)
				ae(vi, wi, xi);
			if (hj >= 3)
				fe(yi, zi, aj);
			if (hj >= 4)
				yd(bj, cj, dj, ej, fj, gj);
			if (hj >= 1)
				ve(si, ti, ui);
			qd();
			ke();
		}
	}

	public void de(int i, int k, int l, int i1, int j1, int k1, int l1, int i2) {
		pd();
		if (kh > j.ep || lh < j.dp || gh > j.ap || hh < j.zo || ih > j.cp || jh < j.bp) {
			fh = false;
			return;
		}
		fh = true;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		if (k1 != 0) {
			i3 = yh[k1];
			j3 = yh[k1 + 1024];
		}
		if (j1 != 0) {
			i4 = yh[j1];
			j4 = yh[j1 + 1024];
		}
		if (i1 != 0) {
			k3 = yh[i1];
			l3 = yh[i1 + 1024];
		}
		for (int k4 = 0; k4 < jg; k4++) {
			int l4 = hi[k4] - i;
			int i5 = ii[k4] - k;
			int j5 = ji[k4] - l;
			if (k1 != 0) {
				int j2 = i5 * i3 + l4 * j3 >> 15;
				i5 = i5 * j3 - l4 * i3 >> 15;
				l4 = j2;
			}
			if (j1 != 0) {
				int k2 = j5 * i4 + l4 * j4 >> 15;
				j5 = j5 * j4 - l4 * i4 >> 15;
				l4 = k2;
			}
			if (i1 != 0) {
				int l2 = i5 * l3 - j5 * k3 >> 15;
				j5 = i5 * k3 + j5 * l3 >> 15;
				i5 = l2;
			}
			if (j5 >= i2)
				ng[k4] = (l4 << l1) / j5;
			else
				ng[k4] = l4 << l1;
			if (j5 >= i2)
				og[k4] = (i5 << l1) / j5;
			else
				og[k4] = i5 << l1;
			kg[k4] = l4;
			lg[k4] = i5;
			mg[k4] = j5;
		}

	}

	public void ee() {
		pd();
		for (int i = 0; i < jg; i++) {
			ei[i] = hi[i];
			fi[i] = ii[i];
			gi[i] = ji[i];
		}

		si = ti = ui = 0;
		vi = wi = xi = 0;
		yi = zi = aj = 256;
		bj = cj = dj = ej = fj = gj = 256;
		hj = 0;
	}

	public h le() {
		h ah1[] = new h[1];
		ah1[0] = this;
		h h1 = new h(ah1, 1);
		h1.dh = dh;
		return h1;
	}

	public void vd(h h1) {
		vi = h1.vi;
		wi = h1.wi;
		xi = h1.xi;
		si = h1.si;
		ti = h1.ti;
		ui = h1.ui;
		qe();
		eh = 1;
	}

	public int me(byte abyte0[]) {
		for (; abyte0[qj] == 10 || abyte0[qj] == 13; qj++)
			;
		int i = ai[abyte0[qj++] & 0xff];
		int k = ai[abyte0[qj++] & 0xff];
		int l = ai[abyte0[qj++] & 0xff];
		int i1 = (i * 4096 + k * 64 + l) - 0x20000;
		if (i1 == 0x1e240)
			i1 = ci;
		return i1;
	}

	public int jg;
	public int kg[];
	public int lg[];
	public int mg[];
	public int ng[];
	public int og[];
	public int pg[];
	public byte qg[];
	public int rg;
	public int sg[];
	public int tg[][];
	public int ug[];
	public int vg[];
	public int wg[];
	public int xg[];
	public int yg[];
	public int zg[];
	public int ah[];
	public int bh[];
	public int ch;
	public int dh;
	public int eh;
	public boolean fh;
	public int gh;
	public int hh;
	public int ih;
	public int jh;
	public int kh;
	public int lh;
	public boolean mh;
	public boolean nh;
	public boolean oh;
	public int ph;
	public int qh[];
	public byte rh[];
	private boolean sh;
	public boolean th;
	public boolean uh;
	public boolean vh;
	public boolean wh;
	private static int xh[];
	private static int yh[];
	private static byte zh[];
	private static int ai[];
	private int ci;
	public int di;
	public int ei[];
	public int fi[];
	public int gi[];
	public int hi[];
	public int ii[];
	public int ji[];
	private int ki;
	private int li[][];
	private int mi[];
	private int ni[];
	private int oi[];
	private int pi[];
	private int qi[];
	private int ri[];
	private int si;
	private int ti;
	private int ui;
	private int vi;
	private int wi;
	private int xi;
	private int yi;
	private int zi;
	private int aj;
	private int bj;
	private int cj;
	private int dj;
	private int ej;
	private int fj;
	private int gj;
	private int hj;
	private int ij;
	private int jj;
	private int kj;
	private int lj;
	private int mj;
	protected int nj;
	protected int oj;
	private byte pj[];
	private int qj;
	static {
		xh = new int[512];
		yh = new int[2048];
		zh = new byte[64];
		ai = new int[256];
		for (int i = 0; i < 256; i++) {
			xh[i] = (int) (Math.sin((double) i * 0.02454369D) * 32768D);
			xh[i + 256] = (int) (Math.cos((double) i * 0.02454369D) * 32768D);
		}

		for (int k = 0; k < 1024; k++) {
			yh[k] = (int) (Math.sin((double) k * 0.00613592315D) * 32768D);
			yh[k + 1024] = (int) (Math.cos((double) k * 0.00613592315D) * 32768D);
		}

		for (int l = 0; l < 10; l++)
			zh[l] = (byte) (48 + l);

		for (int i1 = 0; i1 < 26; i1++)
			zh[i1 + 10] = (byte) (65 + i1);

		for (int j1 = 0; j1 < 26; j1++)
			zh[j1 + 36] = (byte) (97 + j1);

		zh[62] = -93;
		zh[63] = 36;
		for (int k1 = 0; k1 < 10; k1++)
			ai[48 + k1] = k1;

		for (int l1 = 0; l1 < 26; l1++)
			ai[65 + l1] = l1 + 10;

		for (int i2 = 0; i2 < 26; i2++)
			ai[97 + i2] = i2 + 36;

		ai[163] = 62;
		ai[36] = 63;
	}
}
