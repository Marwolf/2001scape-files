public class Player {

	public Player() {
		kr = new int[10];
		lr = new int[10];
		mr = new int[12];
		combatLevel = -1;
		fs = false;
		gs = -1;
	}

	public long yq;
	public String playerName;
	public int ar;
	public int br;
	public int cr;
	public int dr;
	public int er;
	public int fr;
	public int gr;
	public int hr;
	public int ir;
	public int jr;
	public int kr[];
	public int lr[];
	public int mr[];
	public String nr;
	public int or;
	public int pr;
	public int qr;
	public int rr;
	public int sr;
	public int tr;
	public int ur;
	public int vr;
	public int combatLevel;
	public int xr;
	public int yr;
	public int zr;
	public int as;
	public int bs;
	public int cs;
	public int ds;
	public int es;
	public boolean fs;
	public int gs;
	public int hs;
}
