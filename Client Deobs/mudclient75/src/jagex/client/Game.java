package jagex.client;

import jagex.Util;
import jagex.q;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Component;
import java.awt.Event;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.io.DataInputStream;
import java.io.IOException;

@SuppressWarnings("serial")
public class Game extends Applet
	implements Runnable {

	public void load() { }

	public synchronized void fj() { }

	public void rj() { }

	public synchronized void oj() { }

	public void yi() { }

	public final void startApplication(int width, int height, String title, boolean resizable) {
		appletMode = false;
		System.out.println("Started application");
		gameWidth = width;
		gameHeight = height;
		window = new Window(this, width, height, title, resizable, false);
		lr = 1;
		thread = new Thread(this);
		thread.start();
		thread.setPriority(1);
	}
	
	public final void init() {
		appletMode = true;
		System.out.println("Started applet");
		gameWidth = getWidth();
		gameHeight = getHeight();
		lr = 1;
		Util.codebase = getCodeBase();
		thread = new Thread(this);
		thread.start();
	}

	public final boolean applet() {
		return appletMode;
	}

	public final void mj(int width, int height, String title, boolean resizable) {
		if(window != null) {
			return;
		} else {
			gameWidth = width;
			gameHeight = height;
			window = new Window(this, width, height, title, resizable, appletMode);
			return;
		}
	}

	public final void resize(int width, int height) {
		if(window == null) {
			return;
		} else {
			window.setSize(width, height);
			gameWidth = width;
			gameHeight = height;
			return;
		}
	}

	public final void cj(Image image) {
		if(window == null) {
			return;
		} else {
			window.setIconImage(image);
			return;
		}
	}

	public final Graphics getGraphics() {
		if(window == null) {
			return super.getGraphics();
		} else {
			return window.getGraphics();
		}
	}

	public final int ui() {
		return gameWidth;
	}

	public final int bj() {
		return gameHeight;
	}

	public final Image createImage(int j, int k) {
		if(window == null) {
			return super.createImage(j, k);
		} else {
			return window.createImage(j, k);
		}
	}

	public Frame zi() {
		return window;
	}

	public final void ij(int j) {
		cr = 1000 / j;
	}

	public final void nj(int j) {
		dr = j;
	}

	public final void qj() {
		for(int j = 0; j < 10; j++) {
			er[j] = 0L;
		}

	}

	public synchronized boolean keyDown(Event event, int j) {
		hj(j);
		is = j;
		js = j;
		kr = 0;
		if(j == 1006) {
			xr = true;
		}
		if(j == 1007) {
			yr = true;
		}
		if(j == 1004) {
			zr = true;
		}
		if(j == 1005) {
			as = true;
		}
		if((char)j == ' ') {
			bs = true;
		}
		if((char)j == 'n' || (char)j == 'm') {
			cs = true;
		}
		if((char)j == 'N' || (char)j == 'M') {
			cs = true;
		}
		if((char)j == '{') {
			vr = true;
		}
		if((char)j == '}') {
			wr = true;
		}
		if((char)j == '\u03F0') {
			ks = !ks;
		}
		if((j >= 97 && j <= 122 || j >= 65 && j <= 90 || j >= 48 && j <= 57 || j == 32) && ls.length() < 20) {
			ls += (char)j;
		}
		if(j >= 32 && j <= 122 && ns.length() < 80) {
			ns += (char)j;
		}
		if(j == 8 && ls.length() > 0) {
			ls = ls.substring(0, ls.length() - 1);
		}
		if(j == 8 && ns.length() > 0) {
			ns = ns.substring(0, ns.length() - 1);
		}
		if(j == 10 || j == 13) {
			ms = ls;
			os = ns;
		}
		return true;
	}

	public void hj(int j) {
	}

	public synchronized boolean keyUp(Event event, int j) {
		is = 0;
		if(j == 1006) {
			xr = false;
		}
		if(j == 1007) {
			yr = false;
		}
		if(j == 1004) {
			zr = false;
		}
		if(j == 1005) {
			as = false;
		}
		if(j == ' ') {
			bs = false;
		}
		if(j == 'n' || j == 'm') {
			cs = false;
		}
		if(j == 'N' || j == 'M') {
			cs = false;
		}
		if(j == '{') {
			vr = false;
		}
		if(j == '}') {
			wr = false;
		}
		return true;
	}

	public synchronized boolean mouseMove(Event event, int j, int k) {
		es = j;
		fs = k + jr;
		gs = 0;
		kr = 0;
		return true;
	}

	public synchronized boolean mouseUp(Event event, int j, int k) {
		es = j;
		fs = k + jr;
		gs = 0;
		return true;
	}

	public synchronized boolean mouseDown(Event event, int j, int k) {
		es = j;
		fs = k + jr;
		if(event.metaDown()) {
			gs = 2;
		} else {
			gs = 1;
		}
		hs = gs;
		kr = 0;
		return true;
	}

	public synchronized boolean mouseDrag(Event event, int j, int k) {
		es = j;
		fs = k + jr;
		if(event.metaDown()) {
			gs = 2;
		} else {
			gs = 1;
		}
		return true;
	}

	public final void start() {
		if(hr >= 0) {
			hr = 0;
		}
	}

	public final void stop() {
		if(hr >= 0) {
			hr = 4000 / cr;
		}
	}

	@SuppressWarnings("deprecation")
	public final void destroy() {
		hr = -1;
		try {
			Thread.sleep(5000L);
		}
		catch(Exception _ex) { }
		if(hr == -1) {
			System.out.println("5 seconds expired, forcing kill");
			ej();
			if(thread != null) {
				thread.stop();
				thread = null;
			}
		}
	}

	public final void ej() {
		hr = -2;
		System.out.println("Closing program");
		rj();
		try {
			Thread.sleep(1000L);
		}
		catch(Exception _ex) { }
		if(window != null) {
			window.dispose();
		}
		if(!appletMode) {
			System.exit(0);
		}
	}

	public final void run() {
		if(lr == 1) {
			lr = 2;
			ur = getGraphics();
			aj();
			lj(0, "Loading...");
			load();
			lr = 0;
		}
		int j = 0;
		int k = 256;
		int del = 1;
		int j1 = 0;
		for(int k1 = 0; k1 < 10; k1++) {
			er[k1] = System.currentTimeMillis();
		}

		while(hr >= 0)  {
			if(hr > 0) {
				hr--;
				if(hr == 0) {
					ej();
					thread = null;
					return;
				}
			}
			int i2 = k;
			int j2 = del;
			k = 300;
			del = 1;
			long l2 = System.currentTimeMillis();
			if(er[j] == 0L) {
				k = i2;
				del = j2;
			} else
			if(l2 > er[j]) {
				k = (int)((long)(2560 * cr) / (l2 - er[j]));
			}
			if(k < 25) {
				k = 25;
			}
			if(k > 256) {
				k = 256;
				del = (int)((long)cr - (l2 - er[j]) / 10L);
				if(del < ds) {
					del = ds;
				}
			}
			try {
				Thread.sleep(del);
			}
			catch(InterruptedException _ex) { }
			er[j] = l2;
			j = (j + 1) % 10;
			if(del > 1) {
				for(int k2 = 0; k2 < 10; k2++) {
					if(er[k2] != 0L) {
						er[k2] += del;
					}
				}

			}
			int i3 = 0;
			while(j1 < 256)  {
				fj();
				j1 += k;
				if(++i3 > dr) {
					j1 = 0;
					ir += 6;
					if(ir > 25) {
						ir = 0;
						ks = true;
					}
					break;
				}
			}
			ir--;
			j1 &= 0xff;
			oj();
			fps = (1000 * k) / (cr * 256);
			if(appletMode && j == 0) {
				showStatus("Fps:" + fps + "Del:" + del);
			}
			if(window != null && (window.width() != gameWidth || window.height() != gameHeight)) {
				resize(window.width(), window.height());
			}
		}
		if(hr == -1) {
			ej();
		}
		thread = null;
	}

	public final void update(i g) {
		paint(g);
	}

	public final void paint(i g) {
		if(lr == 2 && logo != null) {
			lj(or, pr);
			return;
		}
		if(lr == 0) {
			yi();
		}
	}

	public void aj() {
		try {
			byte archive[] = Util.cd("jagex.jag");
			byte data[] = Util.archiveEntry("logo.tga", 0, archive);
			logo = decodeTga(data);
			i.loadFont(Util.archiveEntry("h11p.jf", 0, archive));
			i.loadFont(Util.archiveEntry("h12b.jf", 0, archive));
			i.loadFont(Util.archiveEntry("h12p.jf", 0, archive));
			i.loadFont(Util.archiveEntry("h13b.jf", 0, archive));
			i.loadFont(Util.archiveEntry("h14b.jf", 0, archive));
			i.loadFont(Util.archiveEntry("h16b.jf", 0, archive));
			i.loadFont(Util.archiveEntry("h20b.jf", 0, archive));
			i.loadFont(Util.archiveEntry("h24b.jf", 0, archive));
		} catch(IOException _ex) {
			System.out.println("Error loading jagex.dat");
		}
	}

	public void lj(int j, String s) {
		int k = (gameWidth - 281) / 2;
		int i1 = (gameHeight - 148) / 2;
		ur.setColor(Color.black);
		ur.fillRect(0, 0, gameWidth, gameHeight);
		if(!nr) {
			ur.drawImage(logo, k, i1, this);
		}
		k += 2;
		i1 += 90;
		or = j;
		pr = s;
		ur.setColor(new Color(132, 132, 132));
		if(nr) {
			ur.setColor(new Color(220, 0, 0));
		}
		ur.drawRect(k - 2, i1 - 2, 280, 23);
		ur.fillRect(k, i1, (277 * j) / 100, 20);
		ur.setColor(new Color(198, 198, 198));
		if(nr) {
			ur.setColor(new Color(255, 255, 255));
		}
		dj(ur, s, qr, k + 138, i1 + 10);
		if(!nr) {
			dj(ur, "Created by JAGeX - visit www.jagex.com", rr, k + 138, i1 + 30);
			dj(ur, "Copyright \2512000 Andrew Gower", rr, k + 138, i1 + 44);
		} else {
			ur.setColor(new Color(132, 132, 152));
			dj(ur, "Copyright \2512000 Andrew Gower", sr, k + 138, gameHeight - 20);
		}
		if(mr != null) {
			ur.setColor(Color.white);
			dj(ur, mr, rr, k + 138, i1 - 120);
		}
	}

	public void ti(int j, String s) {
		int k = (gameWidth - 281) / 2;
		int i1 = (gameHeight - 148) / 2;
		k += 2;
		i1 += 90;
		or = j;
		pr = s;
		int j1 = (277 * j) / 100;
		ur.setColor(new Color(132, 132, 132));
		if(nr) {
			ur.setColor(new Color(220, 0, 0));
		}
		ur.fillRect(k, i1, j1, 20);
		ur.setColor(Color.black);
		ur.fillRect(k + j1, i1, 277 - j1, 20);
		ur.setColor(new Color(198, 198, 198));
		if(nr) {
			ur.setColor(new Color(255, 255, 255));
		}
		dj(ur, s, qr, k + 138, i1 + 10);
	}

	public void dj(Graphics g, String s, Font font, int j, int k) {
		Object obj;
		if(window == null) {
			obj = this;
		} else {
			obj = window;
		}
		FontMetrics fontmetrics = ((Component) (obj)).getFontMetrics(font);
		fontmetrics.stringWidth(s);
		g.setFont(font);
		g.drawString(s, j - fontmetrics.stringWidth(s) / 2, k + fontmetrics.getHeight() / 4);
	}

	public Image decodeTga(byte abyte0[]) {
		int j = abyte0[13] * 256 + abyte0[12];
		int k = abyte0[15] * 256 + abyte0[14];
		byte abyte1[] = new byte[256];
		byte abyte2[] = new byte[256];
		byte abyte3[] = new byte[256];
		for(int i1 = 0; i1 < 256; i1++) {
			abyte1[i1] = abyte0[20 + i1 * 3];
			abyte2[i1] = abyte0[19 + i1 * 3];
			abyte3[i1] = abyte0[18 + i1 * 3];
		}

		IndexColorModel indexcolormodel = new IndexColorModel(8, 256, abyte1, abyte2, abyte3);
		byte abyte4[] = new byte[j * k];
		int j1 = 0;
		for(int k1 = k - 1; k1 >= 0; k1--) {
			for(int l1 = 0; l1 < j; l1++) {
				abyte4[j1++] = abyte0[786 + l1 + k1 * j];
			}

		}

		MemoryImageSource memoryimagesource = new MemoryImageSource(j, k, indexcolormodel, abyte4, 0, j);
		Image image = createImage(memoryimagesource);
		return image;
	}

	public byte[] xi(String s, String s1, int j) throws IOException {
		int k = 0;
		int i1 = 0;
		int j1 = 0;
		byte abyte0[] = null;
		while(k < 2)  {
			try {
				ti(j, "Loading " + s1 + " - 0%");
				if(k == 1) {
					s = s.toUpperCase();
				}
				java.io.InputStream inputstream = Util.openStream(s);
				DataInputStream datainputstream = new DataInputStream(inputstream);
				byte abyte2[] = new byte[6];
				datainputstream.readFully(abyte2, 0, 6);
				i1 = ((abyte2[0] & 0xff) << 16) + ((abyte2[1] & 0xff) << 8) + (abyte2[2] & 0xff);
				j1 = ((abyte2[3] & 0xff) << 16) + ((abyte2[4] & 0xff) << 8) + (abyte2[5] & 0xff);
				ti(j, "Loading " + s1 + " - 5%");
				int k1 = 0;
				abyte0 = new byte[j1];
				while(k1 < j1)  {
					int l1 = j1 - k1;
					if(l1 > 1000) {
						l1 = 1000;
					}
					datainputstream.readFully(abyte0, k1, l1);
					k1 += l1;
					ti(j, "Loading " + s1 + " - " + (5 + (k1 * 95) / j1) + "%");
				}
				k = 2;
				datainputstream.close();
			}
			catch(IOException _ex) {
				k++;
			}
		}
		ti(j, "Unpacking " + s1);
		if(j1 != i1) {
			byte abyte1[] = new byte[i1];
			q.fn(abyte1, i1, abyte0, j1, 0);
			return abyte1;
		} else {
			return abyte0;
		}
	}

	public Game() {
		gameWidth = 512;
		gameHeight = 384;
		cr = 20;
		dr = 1000;
		er = new long[10];
		lr = 1;
		nr = false;
		pr = "Loading";
		qr = new Font("TimesRoman", 0, 15);
		rr = new Font("Helvetica", 1, 13);
		sr = new Font("Helvetica", 0, 12);
		vr = false;
		wr = false;
		xr = false;
		yr = false;
		zr = false;
		as = false;
		bs = false;
		cs = false;
		ds = 1;
		ks = false;
		ls = "";
		ms = "";
		ns = "";
		os = "";
	}

	private int gameWidth;
	private int gameHeight;
	private Thread thread;
	private int cr;
	private int dr;
	private long er[];
	static Window window = null;
	private boolean appletMode;
	private int hr;
	private int ir;
	public int jr;
	public int kr;
	public int lr;
	public String mr;
	private boolean nr;
	private int or;
	private String pr;
	private Font qr;
	private Font rr;
	private Font sr;
	private Image logo;
	private Graphics ur;
	public boolean vr;
	public boolean wr;
	public boolean xr;
	public boolean yr;
	public boolean zr;
	public boolean as;
	public boolean bs;
	public boolean cs;
	public int ds;
	public int es;
	public int fs;
	public int gs;
	public int hs;
	public int is;
	public int js;
	public boolean ks;
	public String ls;
	public String ms;
	public String ns;
	public String os;
	public int fps;

}
