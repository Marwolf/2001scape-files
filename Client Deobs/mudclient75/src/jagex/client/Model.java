// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) braces nonlb 

package jagex.client;

import jagex.Util;

import java.io.DataInputStream;
import java.io.IOException;

// Referenced classes of package jagex.client:
//			a

public class Model {

	public Model(int i, int j) {
		fv = 1;
		gv = true;
		nv = true;
		ov = false;
		pv = false;
		qv = -1;
		tv = false;
		uv = false;
		vv = false;
		wv = false;
		xv = false;
		dw = 0xbc614e;
		jx = 0xbc614e;
		kx = 180;
		lx = 155;
		mx = 95;
		nx = 256;
		ox = 512;
		px = 32;
		xl(i, j);
		mw = new int[j][1];
		for(int k = 0; k < j; k++) {
			mw[k][0] = k;
		}

	}

	public Model(int i, int j, boolean flag, boolean flag1, boolean flag2, boolean flag3, boolean flag4) {
		fv = 1;
		gv = true;
		nv = true;
		ov = false;
		pv = false;
		qv = -1;
		tv = false;
		uv = false;
		vv = false;
		wv = false;
		xv = false;
		dw = 0xbc614e;
		jx = 0xbc614e;
		kx = 180;
		lx = 155;
		mx = 95;
		nx = 256;
		ox = 512;
		px = 32;
		tv = flag;
		uv = flag1;
		vv = flag2;
		wv = flag3;
		xv = flag4;
		xl(i, j);
	}

	private void xl(int i, int j) {
		fw = new int[i];
		gw = new int[i];
		hw = new int[i];
		qu = new int[i];
		ru = new byte[i];
		tu = new int[j];
		uu = new int[j][];
		vu = new int[j];
		wu = new int[j];
		zu = new int[j];
		yu = new int[j];
		xu = new int[j];
		if(!xv) {
			lu = new int[i];
			mu = new int[i];
			nu = new int[i];
			ou = new int[i];
			pu = new int[i];
		}
		if(!wv) {
			sv = new byte[j];
			rv = new int[j];
		}
		if(tv) {
			iw = fw;
			jw = gw;
			kw = hw;
		} else {
			iw = new int[i];
			jw = new int[i];
			kw = new int[i];
		}
		if(!vv || !uv) {
			av = new int[j];
			bv = new int[j];
			cv = new int[j];
		}
		if(!uv) {
			nw = new int[j];
			ow = new int[j];
			pw = new int[j];
			qw = new int[j];
			rw = new int[j];
			sw = new int[j];
		}
		su = 0;
		ku = 0;
		ew = i;
		lw = j;
		tw = uw = vw = 0;
		ww = xw = yw = 0;
		zw = ax = bx = 256;
		cx = dx = ex = fx = gx = hx = 256;
		ix = 0;
	}

	public void zl() {
		lu = new int[ku];
		mu = new int[ku];
		nu = new int[ku];
		ou = new int[ku];
		pu = new int[ku];
	}

	public void hm() {
		su = 0;
		ku = 0;
	}

	public void sl(int i, int j) {
		su -= i;
		if(su < 0) {
			su = 0;
		}
		ku -= j;
		if(ku < 0) {
			ku = 0;
		}
	}

	public Model(byte abyte0[], int i, boolean flag) {
		fv = 1;
		gv = true;
		nv = true;
		ov = false;
		pv = false;
		qv = -1;
		tv = false;
		uv = false;
		vv = false;
		wv = false;
		xv = false;
		dw = 0xbc614e;
		jx = 0xbc614e;
		kx = 180;
		lx = 155;
		mx = 95;
		nx = 256;
		ox = 512;
		px = 32;
		int j = Util.vc(abyte0, i);
		i += 2;
		int k = Util.vc(abyte0, i);
		i += 2;
		xl(j, k);
		mw = new int[k][1];
		for(int l = 0; l < j; l++) {
			fw[l] = Util.bd(abyte0, i);
			i += 2;
		}

		for(int i1 = 0; i1 < j; i1++) {
			gw[i1] = Util.bd(abyte0, i);
			i += 2;
		}

		for(int j1 = 0; j1 < j; j1++) {
			hw[j1] = Util.bd(abyte0, i);
			i += 2;
		}

		ku = j;
		for(int k1 = 0; k1 < k; k1++) {
			tu[k1] = abyte0[i++] & 0xff;
		}

		for(int l1 = 0; l1 < k; l1++) {
			vu[l1] = Util.bd(abyte0, i);
			i += 2;
			if(vu[l1] == 32767) {
				vu[l1] = dw;
			}
		}

		for(int i2 = 0; i2 < k; i2++) {
			wu[i2] = Util.bd(abyte0, i);
			i += 2;
			if(wu[i2] == 32767) {
				wu[i2] = dw;
			}
		}

		for(int j2 = 0; j2 < k; j2++) {
			int k2 = abyte0[i++] & 0xff;
			if(k2 == 0) {
				zu[j2] = 0;
			} else {
				zu[j2] = dw;
			}
		}

		for(int l2 = 0; l2 < k; l2++) {
			uu[l2] = new int[tu[l2]];
			for(int i3 = 0; i3 < tu[l2]; i3++) {
				if(j < 256) {
					uu[l2][i3] = abyte0[i++] & 0xff;
				} else {
					uu[l2][i3] = Util.vc(abyte0, i);
					i += 2;
				}
			}

		}

		su = k;
		fv = 1;
	}

	public Model(byte abyte0[], int i) {
		fv = 1;
		gv = true;
		nv = true;
		ov = false;
		pv = false;
		qv = -1;
		tv = false;
		uv = false;
		vv = false;
		wv = false;
		xv = false;
		dw = 0xbc614e;
		jx = 0xbc614e;
		kx = 180;
		lx = 155;
		mx = 95;
		nx = 256;
		ox = 512;
		px = 32;
		qx = abyte0;
		rx = i;
		em(qx);
		int j = em(qx);
		int k = em(qx);
		xl(j, k);
		mw = new int[k][];
		for(int l2 = 0; l2 < j; l2++) {
			int l = em(qx);
			int i1 = em(qx);
			int j1 = em(qx);
			gm(l, i1, j1);
		}

		for(int i3 = 0; i3 < k; i3++) {
			int k1 = em(qx);
			int l1 = em(qx);
			int i2 = em(qx);
			int j2 = em(qx);
			ox = em(qx);
			px = em(qx);
			int k2 = em(qx);
			int ai[] = new int[k1];
			for(int j3 = 0; j3 < k1; j3++) {
				ai[j3] = em(qx);
			}

			int ai1[] = new int[j2];
			for(int k3 = 0; k3 < j2; k3++) {
				ai1[k3] = em(qx);
			}

			int l3 = im(k1, ai, l1, i2);
			mw[i3] = ai1;
			if(k2 == 0) {
				zu[l3] = 0;
			} else {
				zu[l3] = dw;
			}
		}

		fv = 1;
	}

	public Model(String file) {
		fv = 1;
		gv = true;
		nv = true;
		ov = false;
		pv = false;
		qv = -1;
		tv = false;
		uv = false;
		vv = false;
		wv = false;
		xv = false;
		dw = 0xbc614e;
		jx = 0xbc614e;
		kx = 180;
		lx = 155;
		mx = 95;
		nx = 256;
		ox = 512;
		px = 32;
		byte data[] = null;
		try {
			java.io.InputStream inputstream = Util.openStream(file);
			DataInputStream datainputstream = new DataInputStream(inputstream);
			data = new byte[3];
			rx = 0;
			for(int i = 0; i < 3; i += datainputstream.read(data, i, 3 - i)) { }
			int k = em(data);
			data = new byte[k];
			rx = 0;
			for(int j = 0; j < k; j += datainputstream.read(data, j, k - j)) { }
			datainputstream.close();
		}
		catch(IOException _ex) {
			ku = 0;
			su = 0;
			return;
		}
		int l = em(data);
		int i1 = em(data);
		xl(l, i1);
		mw = new int[i1][];
		for(int j3 = 0; j3 < l; j3++) {
			int j1 = em(data);
			int k1 = em(data);
			int l1 = em(data);
			gm(j1, k1, l1);
		}

		for(int k3 = 0; k3 < i1; k3++) {
			int i2 = em(data);
			int j2 = em(data);
			int k2 = em(data);
			int l2 = em(data);
			ox = em(data);
			px = em(data);
			int i3 = em(data);
			int ai[] = new int[i2];
			for(int l3 = 0; l3 < i2; l3++) {
				ai[l3] = em(data);
			}

			int ai1[] = new int[l2];
			for(int i4 = 0; i4 < l2; i4++) {
				ai1[i4] = em(data);
			}

			int j4 = im(i2, ai, j2, k2);
			mw[k3] = ai1;
			if(i3 == 0) {
				zu[j4] = 0;
			} else {
				zu[j4] = dw;
			}
		}

		fv = 1;
	}

	public Model(Model ap[], int i, boolean flag, boolean flag1, boolean flag2, boolean flag3) {
		fv = 1;
		gv = true;
		nv = true;
		ov = false;
		pv = false;
		qv = -1;
		tv = false;
		uv = false;
		vv = false;
		wv = false;
		xv = false;
		dw = 0xbc614e;
		jx = 0xbc614e;
		kx = 180;
		lx = 155;
		mx = 95;
		nx = 256;
		ox = 512;
		px = 32;
		tv = flag;
		uv = flag1;
		vv = flag2;
		wv = flag3;
		dm(ap, i, false);
	}

	public Model(Model ap[], int i) {
		fv = 1;
		gv = true;
		nv = true;
		ov = false;
		pv = false;
		qv = -1;
		tv = false;
		uv = false;
		vv = false;
		wv = false;
		xv = false;
		dw = 0xbc614e;
		jx = 0xbc614e;
		kx = 180;
		lx = 155;
		mx = 95;
		nx = 256;
		ox = 512;
		px = 32;
		dm(ap, i, true);
	}

	public void dm(Model ap[], int i, boolean flag) {
		int j = 0;
		int k = 0;
		for(int l = 0; l < i; l++) {
			j += ap[l].su;
			k += ap[l].ku;
		}

		xl(k, j);
		if(flag) {
			mw = new int[j][];
		}
		for(int i1 = 0; i1 < i; i1++) {
			Model p1 = ap[i1];
			p1.ol();
			px = p1.px;
			ox = p1.ox;
			kx = p1.kx;
			lx = p1.lx;
			mx = p1.mx;
			nx = p1.nx;
			for(int j1 = 0; j1 < p1.su; j1++) {
				int ai[] = new int[p1.tu[j1]];
				int ai1[] = p1.uu[j1];
				for(int k1 = 0; k1 < p1.tu[j1]; k1++) {
					ai[k1] = gm(p1.fw[ai1[k1]], p1.gw[ai1[k1]], p1.hw[ai1[k1]]);
				}

				int l1 = im(p1.tu[j1], ai, p1.vu[j1], p1.wu[j1]);
				zu[l1] = p1.zu[j1];
				yu[l1] = p1.yu[j1];
				xu[l1] = p1.xu[j1];
				if(flag) {
					if(i > 1) {
						mw[l1] = new int[p1.mw[j1].length + 1];
						mw[l1][0] = i1;
						for(int i2 = 0; i2 < p1.mw[j1].length; i2++) {
							mw[l1][i2 + 1] = p1.mw[j1][i2];
						}

					} else {
						mw[l1] = new int[p1.mw[j1].length];
						for(int j2 = 0; j2 < p1.mw[j1].length; j2++) {
							mw[l1][j2] = p1.mw[j1][j2];
						}

					}
				}
			}

		}

		fv = 1;
	}

	public Model(int i, int ai[], int ai1[], int ai2[], int j, int k) {
		this(i, 1);
		ku = i;
		for(int l = 0; l < i; l++) {
			fw[l] = ai[l];
			gw[l] = ai1[l];
			hw[l] = ai2[l];
		}

		su = 1;
		tu[0] = i;
		int ai3[] = new int[i];
		for(int i1 = 0; i1 < i; i1++) {
			ai3[i1] = i1;
		}

		uu[0] = ai3;
		vu[0] = j;
		wu[0] = k;
		fv = 1;
	}

	public int gm(int i, int j, int k) {
		for(int l = 0; l < ku; l++) {
			if(fw[l] == i && gw[l] == j && hw[l] == k) {
				return l;
			}
		}

		if(ku >= ew) {
			return -1;
		} else {
			fw[ku] = i;
			gw[ku] = j;
			hw[ku] = k;
			return ku++;
		}
	}

	public int cm(int i, int j, int k) {
		if(ku >= ew) {
			return -1;
		} else {
			fw[ku] = i;
			gw[ku] = j;
			hw[ku] = k;
			return ku++;
		}
	}

	public int im(int i, int ai[], int j, int k) {
		if(su >= lw) {
			return -1;
		} else {
			tu[su] = i;
			uu[su] = ai;
			vu[su] = j;
			wu[su] = k;
			fv = 1;
			return su++;
		}
	}

	public Model[] om(int i, int j, int k, int l, int i1, int j1, int k1, 
			boolean flag) {
		ol();
		int ai[] = new int[j1];
		int ai1[] = new int[j1];
		for(int l1 = 0; l1 < j1; l1++) {
			ai[l1] = 0;
			ai1[l1] = 0;
		}

		for(int i2 = 0; i2 < su; i2++) {
			int j2 = 0;
			int k2 = 0;
			int i3 = tu[i2];
			int ai2[] = uu[i2];
			for(int i4 = 0; i4 < i3; i4++) {
				j2 += fw[ai2[i4]];
				k2 += hw[ai2[i4]];
			}

			int k4 = j2 / (i3 * k) + (k2 / (i3 * l)) * i1;
			ai[k4] += i3;
			ai1[k4]++;
		}

		Model ap[] = new Model[j1];
		for(int l2 = 0; l2 < j1; l2++) {
			if(ai[l2] > k1) {
				ai[l2] = k1;
			}
			ap[l2] = new Model(ai[l2], ai1[l2], true, true, true, flag, true);
			ap[l2].ox = ox;
			ap[l2].px = px;
		}

		for(int j3 = 0; j3 < su; j3++) {
			int k3 = 0;
			int j4 = 0;
			int l4 = tu[j3];
			int ai3[] = uu[j3];
			for(int i5 = 0; i5 < l4; i5++) {
				k3 += fw[ai3[i5]];
				j4 += hw[ai3[i5]];
			}

			int j5 = k3 / (l4 * k) + (j4 / (l4 * l)) * i1;
			tl(ap[j5], ai3, l4, j3);
		}

		for(int l3 = 0; l3 < j1; l3++) {
			ap[l3].zl();
		}

		return ap;
	}

	public void tl(Model p1, int ai[], int i, int j) {
		int ai1[] = new int[i];
		for(int k = 0; k < i; k++) {
			int l = ai1[k] = p1.gm(fw[ai[k]], gw[ai[k]], hw[ai[k]]);
			p1.qu[l] = qu[ai[k]];
			p1.ru[l] = ru[ai[k]];
		}

		int i1 = p1.im(i, ai1, vu[j], wu[j]);
		if(!p1.wv && !wv) {
			p1.rv[i1] = rv[j];
		}
		p1.zu[i1] = zu[j];
		p1.yu[i1] = yu[j];
		p1.xu[i1] = xu[j];
	}

	public void pm(boolean flag, int i, int j, int k, int l, int i1) {
		px = 256 - i * 4;
		ox = (64 - j) * 16 + 128;
		if(vv) {
			return;
		}
		for(int j1 = 0; j1 < su; j1++) {
			if(flag) {
				zu[j1] = dw;
			} else {
				zu[j1] = 0;
			}
		}

		kx = k;
		lx = l;
		mx = i1;
		nx = (int)Math.sqrt(k * k + l * l + i1 * i1);
		tm();
	}

	public void yl(int i, int j, int k, int l, int i1) {
		px = 256 - i * 4;
		ox = (64 - j) * 16 + 128;
		if(vv) {
			return;
		} else {
			kx = k;
			lx = l;
			mx = i1;
			nx = (int)Math.sqrt(k * k + l * l + i1 * i1);
			tm();
			return;
		}
	}

	public void wm(int i, int j, int k) {
		if(vv) {
			return;
		} else {
			kx = i;
			lx = j;
			mx = k;
			nx = (int)Math.sqrt(i * i + j * j + k * k);
			tm();
			return;
		}
	}

	public void xm(int i, int j) {
		ru[i] = (byte)j;
	}

	public void sm(int i, int j, int k) {
		ww = ww + i & 0xff;
		xw = xw + j & 0xff;
		yw = yw + k & 0xff;
		um();
		fv = 1;
	}

	public void vl(int i, int j, int k) {
		ww = i & 0xff;
		xw = j & 0xff;
		yw = k & 0xff;
		um();
		fv = 1;
	}

	public void fm(int i, int j, int k) {
		tw += i;
		uw += j;
		vw += k;
		um();
		fv = 1;
	}

	public void ql(int i, int j, int k) {
		tw = i;
		uw = j;
		vw = k;
		um();
		fv = 1;
	}

	public int qm() {
		return tw;
	}

	public void vm(int i, int j, int k) {
		zw = i;
		ax = j;
		bx = k;
		um();
		fv = 1;
	}

	public void wl(int i, int j, int k, int l, int i1, int j1) {
		cx = i;
		dx = j;
		ex = k;
		fx = l;
		gx = i1;
		hx = j1;
		um();
		fv = 1;
	}

	private void um() {
		if(cx != 256 || dx != 256 || ex != 256 || fx != 256 || gx != 256 || hx != 256) {
			ix = 4;
			return;
		}
		if(zw != 256 || ax != 256 || bx != 256) {
			ix = 3;
			return;
		}
		if(ww != 0 || xw != 0 || yw != 0) {
			ix = 2;
			return;
		}
		if(tw != 0 || uw != 0 || vw != 0) {
			ix = 1;
			return;
		} else {
			ix = 0;
			return;
		}
	}

	private void rm(int i, int j, int k) {
		for(int l = 0; l < ku; l++) {
			iw[l] += i;
			jw[l] += j;
			kw[l] += k;
		}

	}

	private void pl(int i, int j, int k) {
		for(int i3 = 0; i3 < ku; i3++) {
			if(k != 0) {
				int l = yv[k];
				int k1 = yv[k + 256];
				int j2 = jw[i3] * l + iw[i3] * k1 >> 15;
				jw[i3] = jw[i3] * k1 - iw[i3] * l >> 15;
				iw[i3] = j2;
			}
			if(i != 0) {
				int i1 = yv[i];
				int l1 = yv[i + 256];
				int k2 = jw[i3] * l1 - kw[i3] * i1 >> 15;
				kw[i3] = jw[i3] * i1 + kw[i3] * l1 >> 15;
				jw[i3] = k2;
			}
			if(j != 0) {
				int j1 = yv[j];
				int i2 = yv[j + 256];
				int l2 = kw[i3] * j1 + iw[i3] * i2 >> 15;
				kw[i3] = kw[i3] * i2 - iw[i3] * j1 >> 15;
				iw[i3] = l2;
			}
		}

	}

	private void km(int i, int j, int k, int l, int i1, int j1) {
		for(int k1 = 0; k1 < ku; k1++) {
			if(i != 0) {
				iw[k1] += jw[k1] * i >> 8;
			}
			if(j != 0) {
				kw[k1] += jw[k1] * j >> 8;
			}
			if(k != 0) {
				iw[k1] += kw[k1] * k >> 8;
			}
			if(l != 0) {
				jw[k1] += kw[k1] * l >> 8;
			}
			if(i1 != 0) {
				kw[k1] += iw[k1] * i1 >> 8;
			}
			if(j1 != 0) {
				jw[k1] += iw[k1] * j1 >> 8;
			}
		}

	}

	private void ul(int i, int j, int k) {
		for(int l = 0; l < ku; l++) {
			iw[l] = iw[l] * i >> 8;
			jw[l] = jw[l] * j >> 8;
			kw[l] = kw[l] * k >> 8;
		}

	}

	private void rl() {
		hv = jv = lv = 0xf423f;
		jx = iv = kv = mv = 0xfff0bdc1;
		for(int i = 0; i < su; i++) {
			int ai[] = uu[i];
			int k = ai[0];
			int i1 = tu[i];
			int j1;
			int k1 = j1 = iw[k];
			int l1;
			int i2 = l1 = jw[k];
			int j2;
			int k2 = j2 = kw[k];
			for(int j = 0; j < i1; j++) {
				int l = ai[j];
				if(iw[l] < j1) {
					j1 = iw[l];
				} else
				if(iw[l] > k1) {
					k1 = iw[l];
				}
				if(jw[l] < l1) {
					l1 = jw[l];
				} else
				if(jw[l] > i2) {
					i2 = jw[l];
				}
				if(kw[l] < j2) {
					j2 = kw[l];
				} else
				if(kw[l] > k2) {
					k2 = kw[l];
				}
			}

			if(!uv) {
				nw[i] = j1;
				ow[i] = k1;
				pw[i] = l1;
				qw[i] = i2;
				rw[i] = j2;
				sw[i] = k2;
			}
			if(k1 - j1 > jx) {
				jx = k1 - j1;
			}
			if(i2 - l1 > jx) {
				jx = i2 - l1;
			}
			if(k2 - j2 > jx) {
				jx = k2 - j2;
			}
			if(j1 < hv) {
				hv = j1;
			}
			if(k1 > iv) {
				iv = k1;
			}
			if(l1 < jv) {
				jv = l1;
			}
			if(i2 > kv) {
				kv = i2;
			}
			if(j2 < lv) {
				lv = j2;
			}
			if(k2 > mv) {
				mv = k2;
			}
		}

	}

	public void tm() {
		if(vv) {
			return;
		}
		int i = ox * nx >> 8;
		for(int j = 0; j < su; j++) {
			if(zu[j] != dw) {
				zu[j] = (av[j] * kx + bv[j] * lx + cv[j] * mx) / i;
			}
		}

		int ai[] = new int[ku];
		int ai1[] = new int[ku];
		int ai2[] = new int[ku];
		int ai3[] = new int[ku];
		for(int k = 0; k < ku; k++) {
			ai[k] = 0;
			ai1[k] = 0;
			ai2[k] = 0;
			ai3[k] = 0;
		}

		for(int l = 0; l < su; l++) {
			if(zu[l] == dw) {
				for(int i1 = 0; i1 < tu[l]; i1++) {
					int k1 = uu[l][i1];
					ai[k1] += av[l];
					ai1[k1] += bv[l];
					ai2[k1] += cv[l];
					ai3[k1]++;
				}

			}
		}

		for(int j1 = 0; j1 < ku; j1++) {
			if(ai3[j1] > 0) {
				qu[j1] = (ai[j1] * kx + ai1[j1] * lx + ai2[j1] * mx) / (i * ai3[j1]);
			}
		}

	}

	public void jm() {
		if(vv && uv) {
			return;
		}
		for(int i = 0; i < su; i++) {
			int ai[] = uu[i];
			int j = iw[ai[0]];
			int k = jw[ai[0]];
			int l = kw[ai[0]];
			int i1 = iw[ai[1]] - j;
			int j1 = jw[ai[1]] - k;
			int k1 = kw[ai[1]] - l;
			int l1 = iw[ai[2]] - j;
			int i2 = jw[ai[2]] - k;
			int j2 = kw[ai[2]] - l;
			int k2 = j1 * j2 - i2 * k1;
			int l2 = k1 * l1 - j2 * i1;
			int i3;
			for(i3 = i1 * i2 - l1 * j1; k2 > 8192 || l2 > 8192 || i3 > 8192 || k2 < -8192 || l2 < -8192 || i3 < -8192; i3 >>= 1) {
				k2 >>= 1;
				l2 >>= 1;
			}

			int j3 = (int)(256D * Math.sqrt(k2 * k2 + l2 * l2 + i3 * i3));
			if(j3 <= 0) {
				j3 = 1;
			}
			av[i] = (k2 * 0x10000) / j3;
			bv[i] = (l2 * 0x10000) / j3;
			cv[i] = (i3 * 65535) / j3;
			yu[i] = -1;
		}

		tm();
	}

	public void nm() {
		if(fv == 2) {
			fv = 0;
			for(int i = 0; i < ku; i++) {
				iw[i] = fw[i];
				jw[i] = gw[i];
				kw[i] = hw[i];
			}

			hv = jv = lv = 0xff676981;
			jx = iv = kv = mv = 0x98967f;
			return;
		}
		if(fv == 1) {
			fv = 0;
			for(int j = 0; j < ku; j++) {
				iw[j] = fw[j];
				jw[j] = gw[j];
				kw[j] = hw[j];
			}

			if(ix >= 2) {
				pl(ww, xw, yw);
			}
			if(ix >= 3) {
				ul(zw, ax, bx);
			}
			if(ix >= 4) {
				km(cx, dx, ex, fx, gx, hx);
			}
			if(ix >= 1) {
				rm(tw, uw, vw);
			}
			rl();
			jm();
		}
	}

	public void mm(int i, int j, int k, int l, int i1, int j1, int k1, 
			int l1) {
		nm();
		if(lv > a.hd || mv < a.gd || hv > a.dd || iv < a.cd || jv > a.fd || kv < a.ed) {
			gv = false;
			return;
		}
		gv = true;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		if(j1 != 0) {
			l2 = zv[j1];
			i3 = zv[j1 + 1024];
		}
		if(i1 != 0) {
			l3 = zv[i1];
			i4 = zv[i1 + 1024];
		}
		if(l != 0) {
			j3 = zv[l];
			k3 = zv[l + 1024];
		}
		for(int j4 = 0; j4 < ku; j4++) {
			int k4 = iw[j4] - i;
			int l4 = jw[j4] - j;
			int i5 = kw[j4] - k;
			if(j1 != 0) {
				int i2 = l4 * l2 + k4 * i3 >> 15;
				l4 = l4 * i3 - k4 * l2 >> 15;
				k4 = i2;
			}
			if(i1 != 0) {
				int j2 = i5 * l3 + k4 * i4 >> 15;
				i5 = i5 * i4 - k4 * l3 >> 15;
				k4 = j2;
			}
			if(l != 0) {
				int k2 = l4 * k3 - i5 * j3 >> 15;
				i5 = l4 * j3 + i5 * k3 >> 15;
				l4 = k2;
			}
			if(i5 >= l1) {
				ou[j4] = (k4 << k1) / i5;
			} else {
				ou[j4] = k4 << k1;
			}
			if(i5 >= l1) {
				pu[j4] = (l4 << k1) / i5;
			} else {
				pu[j4] = l4 << k1;
			}
			lu[j4] = k4;
			mu[j4] = l4;
			nu[j4] = i5;
		}

	}

	public void ol() {
		nm();
		for(int i = 0; i < ku; i++) {
			fw[i] = iw[i];
			gw[i] = jw[i];
			hw[i] = kw[i];
		}

		tw = uw = vw = 0;
		ww = xw = yw = 0;
		zw = ax = bx = 256;
		cx = dx = ex = fx = gx = hx = 256;
		ix = 0;
	}

	public Model bm() {
		Model ap[] = new Model[1];
		ap[0] = this;
		Model p1 = new Model(ap, 1);
		p1.ev = ev;
		return p1;
	}

	public Model am(boolean flag, boolean flag1, boolean flag2, boolean flag3) {
		Model ap[] = new Model[1];
		ap[0] = this;
		Model p1 = new Model(ap, 1, flag, flag1, flag2, flag3);
		p1.ev = ev;
		return p1;
	}

	public void lm(Model p1) {
		ww = p1.ww;
		xw = p1.xw;
		yw = p1.yw;
		tw = p1.tw;
		uw = p1.uw;
		vw = p1.vw;
		um();
		fv = 1;
	}

	public int em(byte abyte0[]) {
		for(; abyte0[rx] == 10 || abyte0[rx] == 13; rx++) { }
		int i = bw[abyte0[rx++] & 0xff];
		int j = bw[abyte0[rx++] & 0xff];
		int k = bw[abyte0[rx++] & 0xff];
		int l = (i * 4096 + j * 64 + k) - 0x20000;
		if(l == 0x1e240) {
			l = dw;
		}
		return l;
	}

	public int ku;
	public int lu[];
	public int mu[];
	public int nu[];
	public int ou[];
	public int pu[];
	public int qu[];
	public byte ru[];
	public int su;
	public int tu[];
	public int uu[][];
	public int vu[];
	public int wu[];
	public int xu[];
	public int yu[];
	public int zu[];
	public int av[];
	public int bv[];
	public int cv[];
	public int dv;
	public int ev;
	public int fv;
	public boolean gv;
	public int hv;
	public int iv;
	public int jv;
	public int kv;
	public int lv;
	public int mv;
	public boolean nv;
	public boolean ov;
	public boolean pv;
	public int qv;
	public int rv[];
	public byte sv[];
	private boolean tv;
	public boolean uv;
	public boolean vv;
	public boolean wv;
	public boolean xv;
	private static int yv[];
	private static int zv[];
	private static byte aw[];
	private static int bw[];
	private int dw;
	public int ew;
	public int fw[];
	public int gw[];
	public int hw[];
	public int iw[];
	public int jw[];
	public int kw[];
	private int lw;
	private int mw[][];
	private int nw[];
	private int ow[];
	private int pw[];
	private int qw[];
	private int rw[];
	private int sw[];
	private int tw;
	private int uw;
	private int vw;
	private int ww;
	private int xw;
	private int yw;
	private int zw;
	private int ax;
	private int bx;
	private int cx;
	private int dx;
	private int ex;
	private int fx;
	private int gx;
	private int hx;
	private int ix;
	private int jx;
	private int kx;
	private int lx;
	private int mx;
	private int nx;
	protected int ox;
	protected int px;
	private byte qx[];
	private int rx;

	static  {
		yv = new int[512];
		zv = new int[2048];
		aw = new byte[64];
		bw = new int[256];
		for(int i = 0; i < 256; i++) {
			yv[i] = (int)(Math.sin((double)i * 0.02454369D) * 32768D);
			yv[i + 256] = (int)(Math.cos((double)i * 0.02454369D) * 32768D);
		}

		for(int j = 0; j < 1024; j++) {
			zv[j] = (int)(Math.sin((double)j * 0.00613592315D) * 32768D);
			zv[j + 1024] = (int)(Math.cos((double)j * 0.00613592315D) * 32768D);
		}

		for(int k = 0; k < 10; k++) {
			aw[k] = (byte)(48 + k);
		}

		for(int l = 0; l < 26; l++) {
			aw[l + 10] = (byte)(65 + l);
		}

		for(int i1 = 0; i1 < 26; i1++) {
			aw[i1 + 36] = (byte)(97 + i1);
		}

		aw[62] = -93;
		aw[63] = 36;
		for(int j1 = 0; j1 < 10; j1++) {
			bw[48 + j1] = j1;
		}

		for(int k1 = 0; k1 < 26; k1++) {
			bw[65 + k1] = k1 + 10;
		}

		for(int l1 = 0; l1 < 26; l1++) {
			bw[97 + l1] = l1 + 36;
		}

		bw[163] = 62;
		bw[36] = 63;
	}
}
