// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

import jagex.Utility;
import jagex.client.GameModel;
import jagex.client.Scene;
import jagex.client.Surface;

import java.io.IOException;

public class World {

    final int aeb = 0xbc614e;
    final int beb = 128;
    boolean vdb;
    boolean wdb;
    Surface xdb;
    Scene ydb;
    int baseMediaSprite;
    int[] ceb;
    int deb;
    int[] eeb;
    int[] feb;
    int[] geb;
    byte[] mapPack;
    byte[][] ieb;
    byte[][] jeb;
    byte[][] keb;
    byte[][] leb;
    byte[][] meb;
    byte[][] neb;
    byte[][] oeb;
    int[][] peb;
    int qeb;
    int reb;
    int[] mouseX;
    int[] mouseY;
    int[][] ueb;
    int[][] objectAdjacency;
    int[][] web;
    boolean playerAlive;
    GameModel[] yeb;
    GameModel[][] zeb;
    GameModel[][] afb;
    GameModel parentModel;

    public World(Scene j1, Surface k) {
        vdb = true;
        wdb = false;
        baseMediaSprite = 750;
        ceb = new int[256];
        ieb = new byte[4][2304];
        jeb = new byte[4][2304];
        keb = new byte[4][2304];
        leb = new byte[4][2304];
        meb = new byte[4][2304];
        neb = new byte[4][2304];
        oeb = new byte[4][2304];
        peb = new int[4][2304];
        qeb = 96;
        reb = 96;
        mouseX = new int[qeb * reb * 2];
        mouseY = new int[qeb * reb * 2];
        ueb = new int[qeb][reb];
        objectAdjacency = new int[qeb][reb];
        web = new int[qeb][reb];
        playerAlive = false;
        yeb = new GameModel[64];
        zeb = new GameModel[4][64];
        afb = new GameModel[4][64];
        ydb = j1;
        xdb = k;
        for (int l = 0; l < 64; l++)
            ceb[l] = Scene.zh(255 - l * 4, 255 - (int) ((double) l * 1.75D), 255 - l * 4);

        for (int i1 = 0; i1 < 64; i1++)
            ceb[i1 + 64] = Scene.zh(i1 * 3, 144, 0);

        for (int k1 = 0; k1 < 64; k1++)
            ceb[k1 + 128] = Scene.zh(192 - (int) ((double) k1 * 1.5D), 144 - (int) ((double) k1 * 1.5D), 0);

        for (int l1 = 0; l1 < 64; l1++)
            ceb[l1 + 192] = Scene.zh(96 - (int) ((double) l1 * 1.5D), 48 + (int) ((double) l1 * 1.5D), 0);

    }

    public int route(int k, int l, int i1, int j1, int k1, int l1, int[] ai,
                     int[] ai1, boolean flag) {
        for (int i2 = 0; i2 < qeb; i2++) {
            for (int j2 = 0; j2 < reb; j2++)
                ueb[i2][j2] = 0;

        }

        int k2 = 0;
        int l2 = 0;
        int i3 = k;
        int j3 = l;
        ueb[k][l] = 99;
        ai[k2] = k;
        ai1[k2++] = l;
        int k3 = ai.length;
        boolean flag1 = false;
        while (l2 != k2) {
            i3 = ai[l2];
            j3 = ai1[l2];
            l2 = (l2 + 1) % k3;
            if (i3 >= i1 && i3 <= k1 && j3 >= j1 && j3 <= l1) {
                flag1 = true;
                break;
            }
            if (flag) {
                if (i3 > 0 && i3 - 1 >= i1 && i3 - 1 <= k1 && j3 >= j1 && j3 <= l1 && (objectAdjacency[i3 - 1][j3] & 8) == 0) {
                    flag1 = true;
                    break;
                }
                if (i3 < qeb - 1 && i3 + 1 >= i1 && i3 + 1 <= k1 && j3 >= j1 && j3 <= l1 && (objectAdjacency[i3 + 1][j3] & 2) == 0) {
                    flag1 = true;
                    break;
                }
                if (j3 > 0 && i3 >= i1 && i3 <= k1 && j3 - 1 >= j1 && j3 - 1 <= l1 && (objectAdjacency[i3][j3 - 1] & 4) == 0) {
                    flag1 = true;
                    break;
                }
                if (j3 < reb - 1 && i3 >= i1 && i3 <= k1 && j3 + 1 >= j1 && j3 + 1 <= l1 && (objectAdjacency[i3][j3 + 1] & 1) == 0) {
                    flag1 = true;
                    break;
                }
            }
            if (i3 > 0 && ueb[i3 - 1][j3] == 0 && (objectAdjacency[i3 - 1][j3] & 0x78) == 0) {
                ai[k2] = i3 - 1;
                ai1[k2] = j3;
                k2 = (k2 + 1) % k3;
                ueb[i3 - 1][j3] = 2;
            }
            if (i3 < qeb - 1 && ueb[i3 + 1][j3] == 0 && (objectAdjacency[i3 + 1][j3] & 0x72) == 0) {
                ai[k2] = i3 + 1;
                ai1[k2] = j3;
                k2 = (k2 + 1) % k3;
                ueb[i3 + 1][j3] = 8;
            }
            if (j3 > 0 && ueb[i3][j3 - 1] == 0 && (objectAdjacency[i3][j3 - 1] & 0x74) == 0) {
                ai[k2] = i3;
                ai1[k2] = j3 - 1;
                k2 = (k2 + 1) % k3;
                ueb[i3][j3 - 1] = 1;
            }
            if (j3 < reb - 1 && ueb[i3][j3 + 1] == 0 && (objectAdjacency[i3][j3 + 1] & 0x71) == 0) {
                ai[k2] = i3;
                ai1[k2] = j3 + 1;
                k2 = (k2 + 1) % k3;
                ueb[i3][j3 + 1] = 4;
            }
            if (i3 > 0 && j3 > 0 && (objectAdjacency[i3][j3 - 1] & 0x74) == 0 && (objectAdjacency[i3 - 1][j3] & 0x78) == 0 && (objectAdjacency[i3 - 1][j3 - 1] & 0x7c) == 0 && ueb[i3 - 1][j3 - 1] == 0) {
                ai[k2] = i3 - 1;
                ai1[k2] = j3 - 1;
                k2 = (k2 + 1) % k3;
                ueb[i3 - 1][j3 - 1] = 3;
            }
            if (i3 < qeb - 1 && j3 > 0 && (objectAdjacency[i3][j3 - 1] & 0x74) == 0 && (objectAdjacency[i3 + 1][j3] & 0x72) == 0 && (objectAdjacency[i3 + 1][j3 - 1] & 0x76) == 0 && ueb[i3 + 1][j3 - 1] == 0) {
                ai[k2] = i3 + 1;
                ai1[k2] = j3 - 1;
                k2 = (k2 + 1) % k3;
                ueb[i3 + 1][j3 - 1] = 9;
            }
            if (i3 > 0 && j3 < reb - 1 && (objectAdjacency[i3][j3 + 1] & 0x71) == 0 && (objectAdjacency[i3 - 1][j3] & 0x78) == 0 && (objectAdjacency[i3 - 1][j3 + 1] & 0x79) == 0 && ueb[i3 - 1][j3 + 1] == 0) {
                ai[k2] = i3 - 1;
                ai1[k2] = j3 + 1;
                k2 = (k2 + 1) % k3;
                ueb[i3 - 1][j3 + 1] = 6;
            }
            if (i3 < qeb - 1 && j3 < reb - 1 && (objectAdjacency[i3][j3 + 1] & 0x71) == 0 && (objectAdjacency[i3 + 1][j3] & 0x72) == 0 && (objectAdjacency[i3 + 1][j3 + 1] & 0x73) == 0 && ueb[i3 + 1][j3 + 1] == 0) {
                ai[k2] = i3 + 1;
                ai1[k2] = j3 + 1;
                k2 = (k2 + 1) % k3;
                ueb[i3 + 1][j3 + 1] = 12;
            }
        }
        if (!flag1)
            return -1;
        l2 = 0;
        ai[l2] = i3;
        ai1[l2++] = j3;
        int i4;
        for (int l3 = i4 = ueb[i3][j3]; i3 != k || j3 != l; l3 = ueb[i3][j3]) {
            if (l3 != i4) {
                i4 = l3;
                ai[l2] = i3;
                ai1[l2++] = j3;
            }
            if ((l3 & 2) != 0)
                i3++;
            else if ((l3 & 8) != 0)
                i3--;
            if ((l3 & 1) != 0)
                j3++;
            else if ((l3 & 4) != 0)
                j3--;
        }

        return l2;
    }

    public void bo(int k, int l, int i1) {
        objectAdjacency[k][l] |= i1;
    }

    public void un(int k, int l, int i1) {
        objectAdjacency[k][l] &= 65535 - i1;
    }

    public void co(int k, int l, int i1, int j1) {
        if (k < 0 || l < 0 || k >= qeb - 1 || l >= reb - 1)
            return;
        if (GameData.boundaryVar6[j1] == 1) {
            if (i1 == 0) {
                objectAdjacency[k][l] |= 1;
                if (l > 0)
                    bo(k, l - 1, 4);
            } else if (i1 == 1) {
                objectAdjacency[k][l] |= 2;
                if (k > 0)
                    bo(k - 1, l, 8);
            } else if (i1 == 2)
                objectAdjacency[k][l] |= 0x10;
            else if (i1 == 3)
                objectAdjacency[k][l] |= 0x20;
            jn(k, l, 1, 1);
        }
    }

    public void removeWallObject(int k, int l, int i1, int j1) {
        if (k < 0 || l < 0 || k >= qeb - 1 || l >= reb - 1)
            return;
        if (GameData.boundaryVar6[j1] == 1) {
            if (i1 == 0) {
                objectAdjacency[k][l] &= 0xfffe;
                if (l > 0)
                    un(k, l - 1, 4);
            } else if (i1 == 1) {
                objectAdjacency[k][l] &= 0xfffd;
                if (k > 0)
                    un(k - 1, l, 8);
            } else if (i1 == 2)
                objectAdjacency[k][l] &= 0xffef;
            else if (i1 == 3)
                objectAdjacency[k][l] &= 0xffdf;
            jn(k, l, 1, 1);
        }
    }

    public void qn(int k, int l, int i1) {
        if (k < 0 || l < 0 || k >= qeb - 1 || l >= reb - 1)
            return;
        if (GameData.locationVar6[i1] == 1 || GameData.locationVar6[i1] == 2) {
            int j1 = en(k, l);
            int k1;
            int l1;
            if (j1 == 0 || j1 == 4) {
                k1 = GameData.locationVar4[i1];
                l1 = GameData.locationVar5[i1];
            } else {
                l1 = GameData.locationVar4[i1];
                k1 = GameData.locationVar5[i1];
            }
            for (int i2 = k; i2 < k + k1; i2++) {
                for (int j2 = l; j2 < l + l1; j2++)
                    if (GameData.locationVar6[i1] == 1)
                        objectAdjacency[i2][j2] |= 0x40;
                    else if (j1 == 0) {
                        objectAdjacency[i2][j2] |= 2;
                        if (i2 > 0)
                            bo(i2 - 1, j2, 8);
                    } else if (j1 == 2) {
                        objectAdjacency[i2][j2] |= 4;
                        if (j2 < reb - 1)
                            bo(i2, j2 + 1, 1);
                    } else if (j1 == 4) {
                        objectAdjacency[i2][j2] |= 8;
                        if (i2 < qeb - 1)
                            bo(i2 + 1, j2, 2);
                    } else if (j1 == 6) {
                        objectAdjacency[i2][j2] |= 1;
                        if (j2 > 0)
                            bo(i2, j2 - 1, 4);
                    }

            }

            jn(k, l, k1, l1);
        }
    }

    public void removeObject(int k, int l, int i1) {
        if (k < 0 || l < 0 || k >= qeb - 1 || l >= reb - 1)
            return;
        if (GameData.locationVar6[i1] == 1 || GameData.locationVar6[i1] == 2) {
            int j1 = en(k, l);
            int k1;
            int l1;
            if (j1 == 0 || j1 == 4) {
                k1 = GameData.locationVar4[i1];
                l1 = GameData.locationVar5[i1];
            } else {
                l1 = GameData.locationVar4[i1];
                k1 = GameData.locationVar5[i1];
            }
            for (int i2 = k; i2 < k + k1; i2++) {
                for (int j2 = l; j2 < l + l1; j2++)
                    if (GameData.locationVar6[i1] == 1)
                        objectAdjacency[i2][j2] &= 0xffbf;
                    else if (j1 == 0) {
                        objectAdjacency[i2][j2] &= 0xfffd;
                        if (i2 > 0)
                            un(i2 - 1, j2, 8);
                    } else if (j1 == 2) {
                        objectAdjacency[i2][j2] &= 0xfffb;
                        if (j2 < reb - 1)
                            un(i2, j2 + 1, 1);
                    } else if (j1 == 4) {
                        objectAdjacency[i2][j2] &= 0xfff7;
                        if (i2 < qeb - 1)
                            un(i2 + 1, j2, 2);
                    } else if (j1 == 6) {
                        objectAdjacency[i2][j2] &= 0xfffe;
                        if (j2 > 0)
                            un(i2, j2 - 1, 4);
                    }

            }

            jn(k, l, k1, l1);
        }
    }

    public void jn(int k, int l, int i1, int j1) {
        if (k < 1 || l < 1 || k + i1 >= qeb || l + j1 >= reb)
            return;
        for (int k1 = k; k1 <= k + i1; k1++) {
            for (int l1 = l; l1 <= l + j1; l1++)
                if ((go(k1, l1) & 0x63) != 0 || (go(k1 - 1, l1) & 0x59) != 0 || (go(k1, l1 - 1) & 0x56) != 0 || (go(k1 - 1, l1 - 1) & 0x6c) != 0)
                    on(k1, l1, 35);
                else
                    on(k1, l1, 0);

        }

    }

    public void on(int k, int l, int i1) {
        int j1 = k / 12;
        int k1 = l / 12;
        int l1 = (k - 1) / 12;
        int i2 = (l - 1) / 12;
        _mthdo(j1, k1, k, l, i1);
        if (j1 != l1)
            _mthdo(l1, k1, k, l, i1);
        if (k1 != i2)
            _mthdo(j1, i2, k, l, i1);
        if (j1 != l1 && k1 != i2)
            _mthdo(l1, i2, k, l, i1);
    }

    public void _mthdo(int k, int l, int i1, int j1, int k1) {
        GameModel h1 = yeb[k + l * 8];
        for (int l1 = 0; l1 < h1.jg; l1++)
            if (h1.ei[l1] == i1 * 128 && h1.gi[l1] == j1 * 128) {
                h1.ud(l1, k1);
                return;
            }

    }

    public int go(int k, int l) {
        if (k < 0 || l < 0 || k >= qeb || l >= reb)
            return 0;
        else
            return objectAdjacency[k][l];
    }

    public int getElevation(int k, int l) {
        int i1 = k >> 7;
        int j1 = l >> 7;
        int k1 = k & 0x7f;
        int l1 = l & 0x7f;
        if (i1 < 0 || j1 < 0 || i1 >= qeb - 1 || j1 >= reb - 1)
            return 0;
        int i2;
        int j2;
        int k2;
        if (k1 <= 128 - l1) {
            i2 = pn(i1, j1);
            j2 = pn(i1 + 1, j1) - i2;
            k2 = pn(i1, j1 + 1) - i2;
        } else {
            i2 = pn(i1 + 1, j1 + 1);
            j2 = pn(i1, j1 + 1) - i2;
            k2 = pn(i1 + 1, j1) - i2;
            k1 = 128 - k1;
            l1 = 128 - l1;
        }
        int l2 = i2 + (j2 * k1) / 128 + (k2 * l1) / 128;
        return l2;
    }

    public int pn(int k, int l) {
        if (k < 0 || k >= 96 || l < 0 || l >= 96)
            return 0;
        byte byte0 = 0;
        if (k >= 48 && l < 48) {
            byte0 = 1;
            k -= 48;
        } else if (k < 48 && l >= 48) {
            byte0 = 2;
            l -= 48;
        } else if (k >= 48) {
            byte0 = 3;
            k -= 48;
            l -= 48;
        }
        return (ieb[byte0][k * 48 + l] & 0xff) * 3;
    }

    public int cn(int k, int l) {
        if (k < 0 || k >= 96 || l < 0 || l >= 96)
            return 0;
        byte byte0 = 0;
        if (k >= 48 && l < 48) {
            byte0 = 1;
            k -= 48;
        } else if (k < 48 && l >= 48) {
            byte0 = 2;
            l -= 48;
        } else if (k >= 48) {
            byte0 = 3;
            k -= 48;
            l -= 48;
        }
        return jeb[byte0][k * 48 + l] & 0xff;
    }

    public int rn(int k, int l, int i1) {
        if (k < 0 || k >= 96 || l < 0 || l >= 96)
            return 0;
        byte byte0 = 0;
        if (k >= 48 && l < 48) {
            byte0 = 1;
            k -= 48;
        } else if (k < 48 && l >= 48) {
            byte0 = 2;
            l -= 48;
        } else if (k >= 48) {
            byte0 = 3;
            k -= 48;
            l -= 48;
        }
        return neb[byte0][k * 48 + l] & 0xff;
    }

    public void nn(int k, int l, int i1) {
        if (k < 0 || k >= 96 || l < 0 || l >= 96)
            return;
        byte byte0 = 0;
        if (k >= 48 && l < 48) {
            byte0 = 1;
            k -= 48;
        } else if (k < 48 && l >= 48) {
            byte0 = 2;
            l -= 48;
        } else if (k >= 48) {
            byte0 = 3;
            k -= 48;
            l -= 48;
        }
        neb[byte0][k * 48 + l] = (byte) i1;
    }

    public int xn(int k, int l, int i1) {
        int j1 = rn(k, l, i1);
        if (j1 == 0)
            return -1;
        int k1 = GameData.floorVar3[j1 - 1];
        return k1 != 2 ? 0 : 1;
    }

    public int in(int k, int l, int i1, int j1) {
        int k1 = rn(k, l, i1);
        if (k1 == 0)
            return j1;
        else
            return GameData.floorVar2[k1 - 1];
    }

    public int hn(int k, int l) {
        if (k < 0 || k >= 96 || l < 0 || l >= 96)
            return 0;
        byte byte0 = 0;
        if (k >= 48 && l < 48) {
            byte0 = 1;
            k -= 48;
        } else if (k < 48 && l >= 48) {
            byte0 = 2;
            l -= 48;
        } else if (k >= 48) {
            byte0 = 3;
            k -= 48;
            l -= 48;
        }
        return peb[byte0][k * 48 + l];
    }

    public int wn(int k, int l) {
        if (k < 0 || k >= 96 || l < 0 || l >= 96)
            return 0;
        byte byte0 = 0;
        if (k >= 48 && l < 48) {
            byte0 = 1;
            k -= 48;
        } else if (k < 48 && l >= 48) {
            byte0 = 2;
            l -= 48;
        } else if (k >= 48) {
            byte0 = 3;
            k -= 48;
            l -= 48;
        }
        return meb[byte0][k * 48 + l];
    }

    public int en(int k, int l) {
        if (k < 0 || k >= 96 || l < 0 || l >= 96)
            return 0;
        byte byte0 = 0;
        if (k >= 48 && l < 48) {
            byte0 = 1;
            k -= 48;
        } else if (k < 48 && l >= 48) {
            byte0 = 2;
            l -= 48;
        } else if (k >= 48) {
            byte0 = 3;
            k -= 48;
            l -= 48;
        }
        return oeb[byte0][k * 48 + l];
    }

    public boolean zn(int k, int l) {
        return wn(k, l) > 0 || wn(k - 1, l) > 0 || wn(k - 1, l - 1) > 0 || wn(k, l - 1) > 0;
    }

    public boolean yn(int k, int l) {
        return wn(k, l) > 0 && wn(k - 1, l) > 0 && wn(k - 1, l - 1) > 0 && wn(k, l - 1) > 0;
    }

    public int ln(int k, int l) {
        if (k < 0 || k >= 96 || l < 0 || l >= 96)
            return 0;
        byte byte0 = 0;
        if (k >= 48 && l < 48) {
            byte0 = 1;
            k -= 48;
        } else if (k < 48 && l >= 48) {
            byte0 = 2;
            l -= 48;
        } else if (k >= 48) {
            byte0 = 3;
            k -= 48;
            l -= 48;
        }
        return leb[byte0][k * 48 + l] & 0xff;
    }

    public int tn(int k, int l) {
        if (k < 0 || k >= 96 || l < 0 || l >= 96)
            return 0;
        byte byte0 = 0;
        if (k >= 48 && l < 48) {
            byte0 = 1;
            k -= 48;
        } else if (k < 48 && l >= 48) {
            byte0 = 2;
            l -= 48;
        } else if (k >= 48) {
            byte0 = 3;
            k -= 48;
            l -= 48;
        }
        return keb[byte0][k * 48 + l] & 0xff;
    }

    public void loadSection(int x, int y, int plane, int chunk) {
        String s = "m" + plane + x / 10 + x % 10 + y / 10 + y % 10;
        int k1;
        try {
            byte[] abyte0;
            if (vdb) {
                abyte0 = Utility.unpackData(s + ".jm", 0, mapPack);
                if (abyte0 == null || abyte0.length == 0)
                    throw new IOException("Map not defined");
            } else {
                abyte0 = new byte[20736];
                Utility.loadData("./gamedata/maps/" + s + ".jm", abyte0, 20736);
            }
            int l1 = 0;
            int i2 = 0;
            for (int j2 = 0; j2 < 2304; j2++) {
                l1 = l1 + abyte0[i2++] & 0xff;
                ieb[chunk][j2] = (byte) l1;
            }

            l1 = 0;
            for (int k2 = 0; k2 < 2304; k2++) {
                l1 = l1 + abyte0[i2++] & 0xff;
                jeb[chunk][k2] = (byte) l1;
            }

            for (int l2 = 0; l2 < 2304; l2++)
                keb[chunk][l2] = abyte0[i2++];

            for (int i3 = 0; i3 < 2304; i3++)
                leb[chunk][i3] = abyte0[i2++];

            for (int j3 = 0; j3 < 2304; j3++) {
                peb[chunk][j3] = (abyte0[i2] & 0xff) * 256 + (abyte0[i2 + 1] & 0xff);
                i2 += 2;
            }

            for (int k3 = 0; k3 < 2304; k3++)
                meb[chunk][k3] = abyte0[i2++];

            for (int l3 = 0; l3 < 2304; l3++)
                neb[chunk][l3] = abyte0[i2++];

            for (int i4 = 0; i4 < 2304; i4++)
                oeb[chunk][i4] = abyte0[i2++];

            return;
        } catch (IOException _ex) {
            k1 = 0;
        }
        for (; k1 < 2304; k1++) {
            ieb[chunk][k1] = 0;
            jeb[chunk][k1] = 0;
            keb[chunk][k1] = 0;
            leb[chunk][k1] = 0;
            peb[chunk][k1] = 0;
            meb[chunk][k1] = 0;
            neb[chunk][k1] = 0;
            if (plane == 0)
                neb[chunk][k1] = -6;
            if (plane == 3)
                neb[chunk][k1] = 8;
            oeb[chunk][k1] = 0;
        }

    }

    public void fo() {
        ydb.clear();
        for (int k = 0; k < 64; k++) {
            yeb[k] = null;
            for (int l = 0; l < 4; l++)
                zeb[l][k] = null;

            for (int i1 = 0; i1 < 4; i1++)
                afb[i1][k] = null;

        }

        System.gc();
    }

    public void loadSection(int k, int l, int i1) {
        fo();
        int j1 = (k + 24) / 48;
        int k1 = (l + 24) / 48;
        gn(k, l, i1, true);
        if (i1 == 0) {
            gn(k, l, 1, false);
            gn(k, l, 2, false);
            loadSection(j1 - 1, k1 - 1, i1, 0);
            loadSection(j1, k1 - 1, i1, 1);
            loadSection(j1 - 1, k1, i1, 2);
            loadSection(j1, k1, i1, 3);
            mn();
        }
    }

    public void mn() {
        for (int k = 0; k < 96; k++) {
            for (int l = 0; l < 96; l++)
                if (rn(k, l, 0) == 250)
                    if (k == 47 && rn(k + 1, l, 0) != 250 && rn(k + 1, l, 0) != 2)
                        nn(k, l, 9);
                    else if (l == 47 && rn(k, l + 1, 0) != 250 && rn(k, l + 1, 0) != 2)
                        nn(k, l, 9);
                    else
                        nn(k, l, 2);

        }

    }

    public void fn(int k, int l, int i1, int j1, int k1) {
        int l1 = k * 3;
        int i2 = l * 3;
        int j2 = ydb.oi(j1);
        int k2 = ydb.oi(k1);
        j2 = j2 >> 1 & 0x7f7f7f;
        k2 = k2 >> 1 & 0x7f7f7f;
        if (i1 == 0) {
            xdb.drawLineHoriz(l1, i2, 3, j2);
            xdb.drawLineHoriz(l1, i2 + 1, 2, j2);
            xdb.drawLineHoriz(l1, i2 + 2, 1, j2);
            xdb.drawLineHoriz(l1 + 2, i2 + 1, 1, k2);
            xdb.drawLineHoriz(l1 + 1, i2 + 2, 2, k2);
            return;
        }
        if (i1 == 1) {
            xdb.drawLineHoriz(l1, i2, 3, k2);
            xdb.drawLineHoriz(l1 + 1, i2 + 1, 2, k2);
            xdb.drawLineHoriz(l1 + 2, i2 + 2, 1, k2);
            xdb.drawLineHoriz(l1, i2 + 1, 1, j2);
            xdb.drawLineHoriz(l1, i2 + 2, 2, j2);
        }
    }

    public void gn(int k, int l, int i1, boolean flag) {
        int j1 = (k + 24) / 48;
        int k1 = (l + 24) / 48;
        loadSection(j1 - 1, k1 - 1, i1, 0);
        loadSection(j1, k1 - 1, i1, 1);
        loadSection(j1 - 1, k1, i1, 2);
        loadSection(j1, k1, i1, 3);
        mn();
        if (parentModel == null)
            parentModel = new GameModel(qeb * reb * 2 + 256, qeb * reb * 2 + 256, true, true, false, false, true);
        if (flag) {
            xdb.blackScreen();
            for (int l1 = 0; l1 < 96; l1++) {
                for (int j2 = 0; j2 < 96; j2++)
                    objectAdjacency[l1][j2] = 0;

            }

            GameModel h1 = parentModel;
            h1.re();
            for (int l2 = 0; l2 < 96; l2++) {
                for (int k3 = 0; k3 < 96; k3++) {
                    int k4 = -pn(l2, k3);
                    if (rn(l2, k3, i1) > 0 && GameData.floorVar3[rn(l2, k3, i1) - 1] == 4)
                        k4 = 0;
                    if (rn(l2 - 1, k3, i1) > 0 && GameData.floorVar3[rn(l2 - 1, k3, i1) - 1] == 4)
                        k4 = 0;
                    if (rn(l2, k3 - 1, i1) > 0 && GameData.floorVar3[rn(l2, k3 - 1, i1) - 1] == 4)
                        k4 = 0;
                    if (rn(l2 - 1, k3 - 1, i1) > 0 && GameData.floorVar3[rn(l2 - 1, k3 - 1, i1) - 1] == 4)
                        k4 = 0;
                    int l5 = h1.je(l2 * 128, k4, k3 * 128);
                    int l7 = (int) (Math.random() * 10D) - 5;
                    h1.ud(l5, l7);
                }

            }

            for (int l3 = 0; l3 < 95; l3++) {
                for (int l4 = 0; l4 < 95; l4++) {
                    int i6 = cn(l3, l4);
                    int i8 = ceb[i6];
                    int k10 = i8;
                    int i13 = i8;
                    int j15 = 0;
                    if (i1 == 1 || i1 == 2) {
                        i8 = 0xbc614e;
                        k10 = 0xbc614e;
                        i13 = 0xbc614e;
                    }
                    if (rn(l3, l4, i1) > 0) {
                        int j17 = rn(l3, l4, i1);
                        int j6 = GameData.floorVar3[j17 - 1];
                        int k19 = xn(l3, l4, i1);
                        i8 = k10 = GameData.floorVar2[j17 - 1];
                        if (j6 == 4) {
                            i8 = 1;
                            k10 = 1;
                        }
                        if (j6 == 5) {
                            if (hn(l3, l4) > 0 && hn(l3, l4) < 24000)
                                if (in(l3 - 1, l4, i1, i13) != 0xbc614e && in(l3, l4 - 1, i1, i13) != 0xbc614e) {
                                    i8 = in(l3 - 1, l4, i1, i13);
                                    j15 = 0;
                                } else if (in(l3 + 1, l4, i1, i13) != 0xbc614e && in(l3, l4 + 1, i1, i13) != 0xbc614e) {
                                    k10 = in(l3 + 1, l4, i1, i13);
                                    j15 = 0;
                                } else if (in(l3 + 1, l4, i1, i13) != 0xbc614e && in(l3, l4 - 1, i1, i13) != 0xbc614e) {
                                    k10 = in(l3 + 1, l4, i1, i13);
                                    j15 = 1;
                                } else if (in(l3 - 1, l4, i1, i13) != 0xbc614e && in(l3, l4 + 1, i1, i13) != 0xbc614e) {
                                    i8 = in(l3 - 1, l4, i1, i13);
                                    j15 = 1;
                                }
                        } else if (j6 != 2 || hn(l3, l4) > 0 && hn(l3, l4) < 24000)
                            if (xn(l3 - 1, l4, i1) != k19 && xn(l3, l4 - 1, i1) != k19) {
                                i8 = i13;
                                j15 = 0;
                            } else if (xn(l3 + 1, l4, i1) != k19 && xn(l3, l4 + 1, i1) != k19) {
                                k10 = i13;
                                j15 = 0;
                            } else if (xn(l3 + 1, l4, i1) != k19 && xn(l3, l4 - 1, i1) != k19) {
                                k10 = i13;
                                j15 = 1;
                            } else if (xn(l3 - 1, l4, i1) != k19 && xn(l3, l4 + 1, i1) != k19) {
                                i8 = i13;
                                j15 = 1;
                            }
                        if (GameData.floorVar4[j17 - 1] != 0)
                            objectAdjacency[l3][l4] |= 0x40;
                        if (GameData.floorVar3[j17 - 1] == 2)
                            objectAdjacency[l3][l4] |= 0x80;
                    }
                    fn(l3, l4, j15, i8, k10);
                    int k17 = ((pn(l3 + 1, l4 + 1) - pn(l3 + 1, l4)) + pn(l3, l4 + 1)) - pn(l3, l4);
                    if (i8 != k10 || k17 != 0) {
                        int[] ai = new int[3];
                        int[] ai7 = new int[3];
                        if (j15 == 0) {
                            if (i8 != 0xbc614e) {
                                ai[0] = l4 + l3 * qeb + qeb;
                                ai[1] = l4 + l3 * qeb;
                                ai[2] = l4 + l3 * qeb + 1;
                                int j22 = h1.createFace(3, ai, 0xbc614e, i8);
                                mouseX[j22] = l3;
                                mouseY[j22] = l4;
                                h1.faceTag[j22] = 0x30d40 + j22;
                            }
                            if (k10 != 0xbc614e) {
                                ai7[0] = l4 + l3 * qeb + 1;
                                ai7[1] = l4 + l3 * qeb + qeb + 1;
                                ai7[2] = l4 + l3 * qeb + qeb;
                                int k22 = h1.createFace(3, ai7, 0xbc614e, k10);
                                mouseX[k22] = l3;
                                mouseY[k22] = l4;
                                h1.faceTag[k22] = 0x30d40 + k22;
                            }
                        } else {
                            if (i8 != 0xbc614e) {
                                ai[0] = l4 + l3 * qeb + 1;
                                ai[1] = l4 + l3 * qeb + qeb + 1;
                                ai[2] = l4 + l3 * qeb;
                                int l22 = h1.createFace(3, ai, 0xbc614e, i8);
                                mouseX[l22] = l3;
                                mouseY[l22] = l4;
                                h1.faceTag[l22] = 0x30d40 + l22;
                            }
                            if (k10 != 0xbc614e) {
                                ai7[0] = l4 + l3 * qeb + qeb;
                                ai7[1] = l4 + l3 * qeb;
                                ai7[2] = l4 + l3 * qeb + qeb + 1;
                                int i23 = h1.createFace(3, ai7, 0xbc614e, k10);
                                mouseX[i23] = l3;
                                mouseY[i23] = l4;
                                h1.faceTag[i23] = 0x30d40 + i23;
                            }
                        }
                    } else if (i8 != 0xbc614e) {
                        int[] ai1 = new int[4];
                        ai1[0] = l4 + l3 * qeb + qeb;
                        ai1[1] = l4 + l3 * qeb;
                        ai1[2] = l4 + l3 * qeb + 1;
                        ai1[3] = l4 + l3 * qeb + qeb + 1;
                        int j20 = h1.createFace(4, ai1, 0xbc614e, i8);
                        mouseX[j20] = l3;
                        mouseY[j20] = l4;
                        h1.faceTag[j20] = 0x30d40 + j20;
                    }
                }

            }

            for (int i5 = 1; i5 < 95; i5++) {
                for (int k6 = 1; k6 < 95; k6++)
                    if (rn(i5, k6, i1) > 0 && GameData.floorVar3[rn(i5, k6, i1) - 1] == 4) {
                        int j8 = GameData.floorVar2[rn(i5, k6, i1) - 1];
                        int l10 = h1.je(i5 * 128, -pn(i5, k6), k6 * 128);
                        int j13 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6), k6 * 128);
                        int k15 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6 + 1), (k6 + 1) * 128);
                        int l17 = h1.je(i5 * 128, -pn(i5, k6 + 1), (k6 + 1) * 128);
                        int[] ai2 = {
                                l10, j13, k15, l17
                        };
                        int k20 = h1.createFace(4, ai2, j8, 0xbc614e);
                        mouseX[k20] = i5;
                        mouseY[k20] = k6;
                        h1.faceTag[k20] = 0x30d40 + k20;
                        fn(i5, k6, 0, j8, j8);
                    } else if (rn(i5, k6, i1) == 0 || GameData.floorVar3[rn(i5, k6, i1) - 1] != 3) {
                        if (rn(i5, k6 + 1, i1) > 0 && GameData.floorVar3[rn(i5, k6 + 1, i1) - 1] == 4) {
                            int k8 = GameData.floorVar2[rn(i5, k6 + 1, i1) - 1];
                            int i11 = h1.je(i5 * 128, -pn(i5, k6), k6 * 128);
                            int k13 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6), k6 * 128);
                            int l15 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6 + 1), (k6 + 1) * 128);
                            int i18 = h1.je(i5 * 128, -pn(i5, k6 + 1), (k6 + 1) * 128);
                            int[] ai3 = {
                                    i11, k13, l15, i18
                            };
                            int l20 = h1.createFace(4, ai3, k8, 0xbc614e);
                            mouseX[l20] = i5;
                            mouseY[l20] = k6;
                            h1.faceTag[l20] = 0x30d40 + l20;
                            fn(i5, k6, 0, k8, k8);
                        }
                        if (rn(i5, k6 - 1, i1) > 0 && GameData.floorVar3[rn(i5, k6 - 1, i1) - 1] == 4) {
                            int l8 = GameData.floorVar2[rn(i5, k6 - 1, i1) - 1];
                            int j11 = h1.je(i5 * 128, -pn(i5, k6), k6 * 128);
                            int l13 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6), k6 * 128);
                            int i16 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6 + 1), (k6 + 1) * 128);
                            int j18 = h1.je(i5 * 128, -pn(i5, k6 + 1), (k6 + 1) * 128);
                            int[] ai4 = {
                                    j11, l13, i16, j18
                            };
                            int i21 = h1.createFace(4, ai4, l8, 0xbc614e);
                            mouseX[i21] = i5;
                            mouseY[i21] = k6;
                            h1.faceTag[i21] = 0x30d40 + i21;
                            fn(i5, k6, 0, l8, l8);
                        }
                        if (rn(i5 + 1, k6, i1) > 0 && GameData.floorVar3[rn(i5 + 1, k6, i1) - 1] == 4) {
                            int i9 = GameData.floorVar2[rn(i5 + 1, k6, i1) - 1];
                            int k11 = h1.je(i5 * 128, -pn(i5, k6), k6 * 128);
                            int i14 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6), k6 * 128);
                            int j16 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6 + 1), (k6 + 1) * 128);
                            int k18 = h1.je(i5 * 128, -pn(i5, k6 + 1), (k6 + 1) * 128);
                            int[] ai5 = {
                                    k11, i14, j16, k18
                            };
                            int j21 = h1.createFace(4, ai5, i9, 0xbc614e);
                            mouseX[j21] = i5;
                            mouseY[j21] = k6;
                            h1.faceTag[j21] = 0x30d40 + j21;
                            fn(i5, k6, 0, i9, i9);
                        }
                        if (rn(i5 - 1, k6, i1) > 0 && GameData.floorVar3[rn(i5 - 1, k6, i1) - 1] == 4) {
                            int j9 = GameData.floorVar2[rn(i5 - 1, k6, i1) - 1];
                            int l11 = h1.je(i5 * 128, -pn(i5, k6), k6 * 128);
                            int j14 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6), k6 * 128);
                            int k16 = h1.je((i5 + 1) * 128, -pn(i5 + 1, k6 + 1), (k6 + 1) * 128);
                            int l18 = h1.je(i5 * 128, -pn(i5, k6 + 1), (k6 + 1) * 128);
                            int[] ai6 = {
                                    l11, j14, k16, l18
                            };
                            int k21 = h1.createFace(4, ai6, j9, 0xbc614e);
                            mouseX[k21] = i5;
                            mouseY[k21] = k6;
                            h1.faceTag[k21] = 0x30d40 + k21;
                            fn(i5, k6, 0, j9, j9);
                        }
                    }

            }

            h1.setLight(true, 40, 48, -50, -10, -50);
            yeb = parentModel.rd(0, 0, 1536, 1536, 8, 64, 233, false);
            for (int l6 = 0; l6 < 64; l6++)
                ydb.addModel(yeb[l6]);

            for (int k9 = 0; k9 < 96; k9++) {
                for (int i12 = 0; i12 < 96; i12++)
                    web[k9][i12] = pn(k9, i12);

            }

        }
        parentModel.re();
        int i2 = 0x606060;
        for (int k2 = 0; k2 < 95; k2++) {
            for (int i3 = 0; i3 < 95; i3++) {
                int i4 = ln(k2, i3);
                if (i4 > 0 && (GameData.boundaryVar8[i4 - 1] == 0 || wdb)) {
                    eo(parentModel, i4 - 1, k2, i3, k2 + 1, i3);
                    if (flag && GameData.boundaryVar6[i4 - 1] != 0) {
                        objectAdjacency[k2][i3] |= 1;
                        if (i3 > 0)
                            bo(k2, i3 - 1, 4);
                    }
                    if (flag)
                        xdb.drawLineHoriz(k2 * 3, i3 * 3, 3, i2);
                }
                i4 = tn(k2, i3);
                if (i4 > 0 && (GameData.boundaryVar8[i4 - 1] == 0 || wdb)) {
                    eo(parentModel, i4 - 1, k2, i3, k2, i3 + 1);
                    if (flag && GameData.boundaryVar6[i4 - 1] != 0) {
                        objectAdjacency[k2][i3] |= 2;
                        if (k2 > 0)
                            bo(k2 - 1, i3, 8);
                    }
                    if (flag)
                        xdb.drawLineVert(k2 * 3, i3 * 3, 3, i2);
                }
                i4 = hn(k2, i3);
                if (i4 > 0 && i4 < 12000 && (GameData.boundaryVar8[i4 - 1] == 0 || wdb)) {
                    eo(parentModel, i4 - 1, k2, i3, k2 + 1, i3 + 1);
                    if (flag && GameData.boundaryVar6[i4 - 1] != 0)
                        objectAdjacency[k2][i3] |= 0x20;
                    if (flag) {
                        xdb.gg(k2 * 3, i3 * 3, i2);
                        xdb.gg(k2 * 3 + 1, i3 * 3 + 1, i2);
                        xdb.gg(k2 * 3 + 2, i3 * 3 + 2, i2);
                    }
                }
                if (i4 > 12000 && i4 < 24000 && (GameData.boundaryVar8[i4 - 12001] == 0 || wdb)) {
                    eo(parentModel, i4 - 12001, k2 + 1, i3, k2, i3 + 1);
                    if (flag && GameData.boundaryVar6[i4 - 12001] != 0)
                        objectAdjacency[k2][i3] |= 0x10;
                    if (flag) {
                        xdb.gg(k2 * 3 + 2, i3 * 3, i2);
                        xdb.gg(k2 * 3 + 1, i3 * 3 + 1, i2);
                        xdb.gg(k2 * 3, i3 * 3 + 2, i2);
                    }
                }
            }

        }

        if (flag)
            xdb.jf(baseMediaSprite - 1, 0, 0, 285, 285);
        parentModel.setLight(false, 60, 24, -50, -10, -50);
        zeb[i1] = parentModel.rd(0, 0, 1536, 1536, 8, 64, 338, true);
        for (int j3 = 0; j3 < 64; j3++)
            ydb.addModel(zeb[i1][j3]);

        for (int j4 = 0; j4 < 95; j4++) {
            for (int j5 = 0; j5 < 95; j5++) {
                int i7 = ln(j4, j5);
                if (i7 > 0)
                    vn(i7 - 1, j4, j5, j4 + 1, j5);
                i7 = tn(j4, j5);
                if (i7 > 0)
                    vn(i7 - 1, j4, j5, j4, j5 + 1);
                i7 = hn(j4, j5);
                if (i7 > 0 && i7 < 12000)
                    vn(i7 - 1, j4, j5, j4 + 1, j5 + 1);
                if (i7 > 12000 && i7 < 24000)
                    vn(i7 - 12001, j4 + 1, j5, j4, j5 + 1);
            }

        }

        for (int k5 = 1; k5 < 95; k5++) {
            for (int j7 = 1; j7 < 95; j7++) {
                int l9 = wn(k5, j7);
                if (l9 > 0) {
                    int j12 = k5;
                    int k14 = j7;
                    int l16 = k5 + 1;
                    int i19 = j7;
                    int l19 = k5 + 1;
                    int l21 = j7 + 1;
                    int j23 = k5;
                    int l23 = j7 + 1;
                    int j24 = 0;
                    int l24 = web[j12][k14];
                    int j25 = web[l16][i19];
                    int l25 = web[l19][l21];
                    int j26 = web[j23][l23];
                    if (l24 > 0x13880)
                        l24 -= 0x13880;
                    if (j25 > 0x13880)
                        j25 -= 0x13880;
                    if (l25 > 0x13880)
                        l25 -= 0x13880;
                    if (j26 > 0x13880)
                        j26 -= 0x13880;
                    if (l24 > j24)
                        j24 = l24;
                    if (j25 > j24)
                        j24 = j25;
                    if (l25 > j24)
                        j24 = l25;
                    if (j26 > j24)
                        j24 = j26;
                    if (j24 >= 0x13880)
                        j24 -= 0x13880;
                    if (l24 < 0x13880)
                        web[j12][k14] = j24;
                    else
                        web[j12][k14] -= 0x13880;
                    if (j25 < 0x13880)
                        web[l16][i19] = j24;
                    else
                        web[l16][i19] -= 0x13880;
                    if (l25 < 0x13880)
                        web[l19][l21] = j24;
                    else
                        web[l19][l21] -= 0x13880;
                    if (j26 < 0x13880)
                        web[j23][l23] = j24;
                    else
                        web[j23][l23] -= 0x13880;
                }
            }

        }

        parentModel.re();
        for (int k7 = 1; k7 < 95; k7++) {
            for (int i10 = 1; i10 < 95; i10++) {
                int k12 = wn(k7, i10);
                if (k12 > 0) {
                    int l14 = k7;
                    int i17 = i10;
                    int j19 = k7 + 1;
                    int i20 = i10;
                    int i22 = k7 + 1;
                    int k23 = i10 + 1;
                    int i24 = k7;
                    int k24 = i10 + 1;
                    int i25 = k7 * 128;
                    int k25 = i10 * 128;
                    int i26 = i25 + 128;
                    int k26 = k25 + 128;
                    int l26 = i25;
                    int i27 = k25;
                    int j27 = i26;
                    int k27 = k26;
                    int l27 = web[l14][i17];
                    int i28 = web[j19][i20];
                    int j28 = web[i22][k23];
                    int k28 = web[i24][k24];
                    int l28 = GameData.roofVar2[k12 - 1];
                    if (yn(l14, i17) && l27 < 0x13880) {
                        l27 += l28 + 0x13880;
                        web[l14][i17] = l27;
                    }
                    if (yn(j19, i20) && i28 < 0x13880) {
                        i28 += l28 + 0x13880;
                        web[j19][i20] = i28;
                    }
                    if (yn(i22, k23) && j28 < 0x13880) {
                        j28 += l28 + 0x13880;
                        web[i22][k23] = j28;
                    }
                    if (yn(i24, k24) && k28 < 0x13880) {
                        k28 += l28 + 0x13880;
                        web[i24][k24] = k28;
                    }
                    if (l27 >= 0x13880)
                        l27 -= 0x13880;
                    if (i28 >= 0x13880)
                        i28 -= 0x13880;
                    if (j28 >= 0x13880)
                        j28 -= 0x13880;
                    if (k28 >= 0x13880)
                        k28 -= 0x13880;
                    byte byte0 = 16;
                    if (!zn(l14 - 1, i17))
                        i25 -= byte0;
                    if (!zn(l14 + 1, i17))
                        i25 += byte0;
                    if (!zn(l14, i17 - 1))
                        k25 -= byte0;
                    if (!zn(l14, i17 + 1))
                        k25 += byte0;
                    if (!zn(j19 - 1, i20))
                        i26 -= byte0;
                    if (!zn(j19 + 1, i20))
                        i26 += byte0;
                    if (!zn(j19, i20 - 1))
                        i27 -= byte0;
                    if (!zn(j19, i20 + 1))
                        i27 += byte0;
                    if (!zn(i22 - 1, k23))
                        j27 -= byte0;
                    if (!zn(i22 + 1, k23))
                        j27 += byte0;
                    if (!zn(i22, k23 - 1))
                        k26 -= byte0;
                    if (!zn(i22, k23 + 1))
                        k26 += byte0;
                    if (!zn(i24 - 1, k24))
                        l26 -= byte0;
                    if (!zn(i24 + 1, k24))
                        l26 += byte0;
                    if (!zn(i24, k24 - 1))
                        k27 -= byte0;
                    if (!zn(i24, k24 + 1))
                        k27 += byte0;
                    k12 = GameData.roofVar4[k12 - 1];
                    l27 = -l27;
                    i28 = -i28;
                    j28 = -j28;
                    k28 = -k28;
                    if (hn(k7, i10) > 12000 && hn(k7, i10) < 24000 && wn(k7 - 1, i10 - 1) == 0) {
                        int[] ai8 = new int[3];
                        ai8[0] = parentModel.je(j27, j28, k26);
                        ai8[1] = parentModel.je(l26, k28, k27);
                        ai8[2] = parentModel.je(i26, i28, i27);
                        parentModel.createFace(3, ai8, k12, 0xbc614e);
                    } else if (hn(k7, i10) > 12000 && hn(k7, i10) < 24000 && wn(k7 + 1, i10 + 1) == 0) {
                        int[] ai9 = new int[3];
                        ai9[0] = parentModel.je(i25, l27, k25);
                        ai9[1] = parentModel.je(i26, i28, i27);
                        ai9[2] = parentModel.je(l26, k28, k27);
                        parentModel.createFace(3, ai9, k12, 0xbc614e);
                    } else if (hn(k7, i10) > 0 && hn(k7, i10) < 12000 && wn(k7 + 1, i10 - 1) == 0) {
                        int[] ai10 = new int[3];
                        ai10[0] = parentModel.je(l26, k28, k27);
                        ai10[1] = parentModel.je(i25, l27, k25);
                        ai10[2] = parentModel.je(j27, j28, k26);
                        parentModel.createFace(3, ai10, k12, 0xbc614e);
                    } else if (hn(k7, i10) > 0 && hn(k7, i10) < 12000 && wn(k7 - 1, i10 + 1) == 0) {
                        int[] ai11 = new int[3];
                        ai11[0] = parentModel.je(i26, i28, i27);
                        ai11[1] = parentModel.je(j27, j28, k26);
                        ai11[2] = parentModel.je(i25, l27, k25);
                        parentModel.createFace(3, ai11, k12, 0xbc614e);
                    } else if (l27 == i28 && j28 == k28) {
                        int[] ai12 = new int[4];
                        ai12[0] = parentModel.je(i25, l27, k25);
                        ai12[1] = parentModel.je(i26, i28, i27);
                        ai12[2] = parentModel.je(j27, j28, k26);
                        ai12[3] = parentModel.je(l26, k28, k27);
                        parentModel.createFace(4, ai12, k12, 0xbc614e);
                    } else if (l27 == k28 && i28 == j28) {
                        int[] ai13 = new int[4];
                        ai13[0] = parentModel.je(l26, k28, k27);
                        ai13[1] = parentModel.je(i25, l27, k25);
                        ai13[2] = parentModel.je(i26, i28, i27);
                        ai13[3] = parentModel.je(j27, j28, k26);
                        parentModel.createFace(4, ai13, k12, 0xbc614e);
                    } else {
                        boolean flag1 = true;
                        if (wn(k7 - 1, i10 - 1) > 0)
                            flag1 = false;
                        if (wn(k7 + 1, i10 + 1) > 0)
                            flag1 = false;
                        if (!flag1) {
                            int[] ai14 = new int[3];
                            ai14[0] = parentModel.je(i26, i28, i27);
                            ai14[1] = parentModel.je(j27, j28, k26);
                            ai14[2] = parentModel.je(i25, l27, k25);
                            parentModel.createFace(3, ai14, k12, 0xbc614e);
                            int[] ai16 = new int[3];
                            ai16[0] = parentModel.je(l26, k28, k27);
                            ai16[1] = parentModel.je(i25, l27, k25);
                            ai16[2] = parentModel.je(j27, j28, k26);
                            parentModel.createFace(3, ai16, k12, 0xbc614e);
                        } else {
                            int[] ai15 = new int[3];
                            ai15[0] = parentModel.je(i25, l27, k25);
                            ai15[1] = parentModel.je(i26, i28, i27);
                            ai15[2] = parentModel.je(l26, k28, k27);
                            parentModel.createFace(3, ai15, k12, 0xbc614e);
                            int[] ai17 = new int[3];
                            ai17[0] = parentModel.je(j27, j28, k26);
                            ai17[1] = parentModel.je(l26, k28, k27);
                            ai17[2] = parentModel.je(i26, i28, i27);
                            parentModel.createFace(3, ai17, k12, 0xbc614e);
                        }
                    }
                }
            }

        }

        parentModel.setLight(true, 50, 50, -50, -10, -50);
        afb[i1] = parentModel.rd(0, 0, 1536, 1536, 8, 64, 169, true);
        for (int j10 = 0; j10 < 64; j10++)
            ydb.addModel(afb[i1][j10]);

        for (int l12 = 0; l12 < 96; l12++) {
            for (int i15 = 0; i15 < 96; i15++)
                if (web[l12][i15] >= 0x13880)
                    web[l12][i15] -= 0x13880;

        }

    }

    public void eo(GameModel h1, int k, int l, int i1, int j1, int k1) {
        on(l, i1, 40);
        on(j1, k1, 40);
        int l1 = GameData.boundaryVar3[k];
        int i2 = GameData.boundaryVar4[k];
        int j2 = GameData.boundaryVar5[k];
        int k2 = l * 128;
        int l2 = i1 * 128;
        int i3 = j1 * 128;
        int j3 = k1 * 128;
        int k3 = h1.je(k2, -web[l][i1], l2);
        int l3 = h1.je(k2, -web[l][i1] - l1, l2);
        int i4 = h1.je(i3, -web[j1][k1] - l1, j3);
        int j4 = h1.je(i3, -web[j1][k1], j3);
        int[] ai = {
                k3, l3, i4, j4
        };
        int k4 = h1.createFace(4, ai, i2, j2);
        if (GameData.boundaryVar8[k] == 5) {
            h1.faceTag[k4] = 30000 + k;
            return;
        } else {
            h1.faceTag[k4] = 0;
            return;
        }
    }

    public void vn(int k, int l, int i1, int j1, int k1) {
        int l1 = GameData.boundaryVar3[k];
        if (web[l][i1] < 0x13880)
            web[l][i1] += 0x13880 + l1;
        if (web[j1][k1] < 0x13880)
            web[j1][k1] += 0x13880 + l1;
    }
}
