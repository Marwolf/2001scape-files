// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

package jagex;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

// Referenced classes of package jagex:
//            o

public class Buffer {

    protected InputStream input;
    protected OutputStream output;
    protected Socket socket;
    protected boolean closing;
    protected byte inputBuffer[];
    int inputBufferOffset;

    public Buffer(InputStream inputstream) {
        closing = false;
        input = inputstream;
    }

    public Buffer(Socket socket)
            throws IOException {
        closing = false;
        this.socket = socket;
        input = socket.getInputStream();
        output = socket.getOutputStream();
    }

    public Buffer(String s)
            throws IOException {
        if (s.startsWith("../gamedata")) {
            s = "./data40" + s.substring(2);
        }
        closing = false;
        input = Utility.openStream(s);
    }

    public Buffer(byte abyte0[]) {
        closing = false;
        inputBuffer = abyte0;
        inputBufferOffset = 0;
        closing = true;
    }

    public Buffer(byte abyte0[], int i) {
        closing = false;
        inputBuffer = abyte0;
        inputBufferOffset = i;
        closing = true;
    }

    public void close() {
        if (closing)
            return;
        if (output != null)
            try {
                output.flush();
            } catch (IOException _ex) {
            }
        try {
            if (socket != null)
                socket.close();
            if (input != null)
                input.close();
            if (output != null) {
                output.close();
                return;
            }
        } catch (IOException _ex) {
            System.out.println("Error closing stream");
        }
    }

    public int read()
            throws IOException {
        if (inputBuffer != null)
            return inputBuffer[inputBufferOffset++];
        if (closing)
            return 0;
        else
            return input.read();
    }

    public int readByte()
            throws IOException {
        return read();
    }

    public int readShort()
            throws IOException {
        int i = readByte();
        int j = readByte();
        return i * 256 + j;
    }

    public int available()
            throws IOException {
        if (closing)
            return 0;
        else
            return input.available();
    }

    public void readBytes(int i, byte abyte0[])
            throws IOException {
        if (closing)
            return;
        int j = 0;
        boolean flag = false;
        int k;
        for (; j < i; j += k)
            if ((k = input.read(abyte0, j, i - j)) <= 0)
                throw new IOException("EOF");

    }

    public void skipToNextDataValue()
            throws IOException {
        for (int i = read(); i != '=' && i != -1; i = read()) ;
    }

    public int readDataInt()
            throws IOException {
        int i = 0;
        boolean flag = false;
        int j;
        for (j = read(); j < '0' || j > '9'; ) {
            if (j == '-')
                flag = true;
            j = read();
            if (j == -1)
                throw new IOException("Eof!");
        }

        for (; j >= '0' && j <= '9'; j = read())
            i = (i * 10 + j) - '0';

        if (flag)
            i = -i;
        return i;
    }

    public String readDataString()
            throws IOException {
        String s = "";
        boolean quotations = false;
        int i;
        for (i = read(); i < ' ' || i == ',' || i == ';' || i == '='; ) {
            i = read();
            if (i == -1)
                throw new IOException("Eof!");
        }

        if (i == '"') {
            quotations = true;
            i = read();
        }
        for (; i != -1; i = read()) {
            if (!quotations && (i == ',' || i == '=' || i == ';') || quotations && i == '"')
                break;
            s = s + (char) i;//le[i];
        }

        return s;
    }

    public int readDataHex()
            throws IOException {
        int i = 0;
        int j;
        for (j = read(); (j < '0' || j > '9') && (j < 'a' || j > 'f') && (j < 'A' || j > 'F'); ) {
            j = read();
            if (j == -1)
                throw new IOException("Eof!");
        }

        do {
            if (j >= '0' && j <= '9')
                i = (i * 16 + j) - '0';
            else if (j >= 'a' && j <= 'f') {
                i = (i * 16 + j + 10) - 'a';
            } else {
                if (j < 'A' || j > 'F')
                    break;
                i = (i * 16 + j + 10) - 'A';
            }
            j = read();
        } while (true);
        return i;
    }
    /*final int zd = 61;
    final int ae = 59;
    final int be = 42;
    final int ce = 43;
    final int de = 44;
    final int ee = 45;
    final int fe = 46;
    final int ge = 47;
    final int he = 92;
    final int ie = 32;
    final int je = 124;
    final int ke = 34;
    static char le[];

    static {
        le = new char[256];
        for (int i = 0; i < 256; i++)
            le[i] = (char) i;

        le[61] = '=';
        le[59] = ';';
        le[42] = '*';
        le[43] = '+';
        le[44] = ',';
        le[45] = '-';
        le[46] = '.';
        le[47] = '/';
        le[92] = '\\';
        le[124] = '|';
        le[33] = '!';
        le[34] = '"';
    }*/
}
