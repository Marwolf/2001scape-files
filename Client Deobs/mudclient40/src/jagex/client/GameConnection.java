// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

package jagex.client;

import jagex.Utility;

import java.awt.*;
import java.io.IOException;

// Referenced classes of package jagex.client:
//            k, a

public class GameConnection extends GameApplet {

    public static String[] loginScreenStatuses;
    public static boolean uc = true;
    public static int vc = 0x5f5e0ff;
    public static int wc;
    public static int xc = 1;
    public String serverAddress;
    public String serverAddress2;
    public int port;
    public ClientStream clientStream;
    public int friendListCount;
    public long[] friendListHashes;
    public int[] friendListOnline;
    public int ignoreListCount;
    public long[] ignoreList;
    public int settingsHideStatus;
    public int settingsBlockChat;
    public int settingsBlockPrivate;
    public int settingsBlockTrade;
    public int sd;
    static {
        loginScreenStatuses = new String[50];
        loginScreenStatuses[0] = "You must enter both a username";
        loginScreenStatuses[1] = "and a password - Please try again";
        loginScreenStatuses[2] = "Connection lost! Please wait...";
        loginScreenStatuses[3] = "Attempting to re-establish";
        loginScreenStatuses[4] = "That username is already in use";
        loginScreenStatuses[5] = "Wait 60 seconds then retry";
        loginScreenStatuses[6] = "Please wait...";
        loginScreenStatuses[7] = "Connecting to server";
        loginScreenStatuses[8] = "Sorry! The server is currently full";
        loginScreenStatuses[9] = "Please try again later";
        loginScreenStatuses[10] = "Invalid username or password";
        loginScreenStatuses[11] = "Try again, or create a new account";
        loginScreenStatuses[12] = "Sorry! Unable to connect to server";
        loginScreenStatuses[13] = "Please check your internet settings";
        loginScreenStatuses[14] = "Username already taken";
        loginScreenStatuses[15] = "Please choose another username";
        loginScreenStatuses[16] = "The client has been updated";
        loginScreenStatuses[17] = "Please clear your cache and reload";
    }
    String username;
    String password;
    int psize;
    int opcode;
    byte[] pdata;
    long hd;
    long lastPing;
    public GameConnection() {
        serverAddress = "127.0.0.1";
        serverAddress2 = "127.0.0.1";
        port = 43594;
        username = "";
        password = "";
        pdata = new byte[5000];
        friendListHashes = new long[50];
        friendListOnline = new int[50];
        ignoreList = new long[50];
    }

    public void login(String u, String p) {
        try {
            username = u;
            u = Utility.formatAuthString(u, 20);
            password = p;
            p = Utility.formatAuthString(p, 20);
            if (u.trim().length() == 0 || p.trim().length() == 0) {
                showLoginScreenStatus(loginScreenStatuses[0], loginScreenStatuses[1]);
                return;
            }
            showLoginScreenStatus(loginScreenStatuses[6], loginScreenStatuses[7]);
            if (inAppletMode())
                clientStream = ClientStream.create(serverAddress, this, port);
            else
                clientStream = ClientStream.create(serverAddress, null, port);
            clientStream.newPacket(0);
            clientStream.writeLong(Utility.username2hash(u));
            clientStream.writeString(p);
            clientStream.writeShort(xc);
            clientStream.flushPacket2();
            clientStream.readShort();
            int i = clientStream.read();
            System.out.println("Login response: " + i);
            if (i == 0) {
                kb();
                resetGame();
                return;
            }
            if (i == 2) {
                showLoginScreenStatus(loginScreenStatuses[8], loginScreenStatuses[9]);
                return;
            }
            if (i == 3) {
                showLoginScreenStatus(loginScreenStatuses[10], loginScreenStatuses[11]);
                return;
            }
            if (i == 4) {
                showLoginScreenStatus(loginScreenStatuses[4], loginScreenStatuses[5]);
                return;
            }
            if (i == 5) {
                showLoginScreenStatus(loginScreenStatuses[16], loginScreenStatuses[17]);
            } else {
                showLoginScreenStatus(loginScreenStatuses[12], loginScreenStatuses[13]);
            }
        } catch (Exception exception) {
            serverAddress = serverAddress2;
            System.out.println(String.valueOf(exception));
            showLoginScreenStatus(loginScreenStatuses[12], loginScreenStatuses[13]);
        }
    }

    public void newPlayer(String s1, String s2, String s3, int i, int j, int l) {
        try {
            if (inAppletMode())
                clientStream = ClientStream.create(serverAddress, this, port);
            else
                clientStream = ClientStream.create(serverAddress, null, port);
            clientStream.newPacket(2);
            s1 = Utility.formatAuthString(s1, 20);
            s2 = Utility.formatAuthString(s2, 20);
            clientStream.writeLong(Utility.username2hash(s1));
            clientStream.writeString(s2);
            for (; s3.length() < 40; s3 = s3 + " ") ;
            clientStream.writeString(s3);
            clientStream.writeInt(i);
            clientStream.writeInt(j);
            clientStream.writeInt(l);
            clientStream.flushPacket();
            clientStream.readShort();
            int i1 = clientStream.read();
            clientStream.close();
            System.out.println("Newplayer response: " + i1);
            if (i1 == 0) {
                hb();
                return;
            }
            if (i1 == 2) {
                showLoginScreenStatus(loginScreenStatuses[8], loginScreenStatuses[9]);
                return;
            }
            if (i1 == 3) {
                showLoginScreenStatus(loginScreenStatuses[14], loginScreenStatuses[15]);
                return;
            }
            if (i1 == 4) {
                showLoginScreenStatus(loginScreenStatuses[4], loginScreenStatuses[5]);
                return;
            }
            if (i1 == 5) {
                showLoginScreenStatus(loginScreenStatuses[16], loginScreenStatuses[17]);
            } else {
                showLoginScreenStatus(loginScreenStatuses[12], loginScreenStatuses[13]);
            }
        } catch (Exception exception) {
            serverAddress = serverAddress2;
            System.out.println(String.valueOf(exception));
            showLoginScreenStatus(loginScreenStatuses[12], loginScreenStatuses[13]);
        }
    }

    public void closeConnection() {
        if (clientStream != null) {
            clientStream.newPacket(1);
            clientStream.flushPacket();
            username = "";
            password = "";
            resetLoginVars();
        }
    }

    public void relogin(String s1, String s2) {
        username = s1;
        s1 = Utility.formatAuthString(s1, 20);
        password = s2;
        s2 = Utility.formatAuthString(s2, 20);
        if (s1.length() == 0 || s2.length() == 0) {
            resetLoginVars();
            return;
        }
        for (long l = System.currentTimeMillis(); System.currentTimeMillis() - l < 60000L; ) {
            s(loginScreenStatuses[2], loginScreenStatuses[3]);
            try {
                if (inAppletMode())
                    clientStream = ClientStream.create(serverAddress, this, port);
                else
                    clientStream = ClientStream.create(serverAddress, null, port);
                clientStream.newPacket(19);
                clientStream.writeLong(Utility.username2hash(s1));
                clientStream.writeString(s2);
                clientStream.writeShort(xc);
                clientStream.flushPacket2();
                clientStream.readShort();
                int i = clientStream.read();
                if (i == 0) {
                    kb();
                    q();
                    return;
                }
                if (i >= 1 && i <= 4) {
                    s1 = "";
                    s2 = "";
                    resetLoginVars();
                    return;
                }
            } catch (Exception _ex) {
                s(loginScreenStatuses[2], loginScreenStatuses[3]);
            }
            s(loginScreenStatuses[2], loginScreenStatuses[3]);
            try {
                Thread.sleep(5000L);
            } catch (Exception ignored) {
            }
        }

        s1 = "";
        s2 = "";
        resetLoginVars();
    }

    public void lostConnection() {
        System.out.println("Lost connection");
        relogin(username, password);
    }

    public void s(String s1, String s2) {
        Graphics g = getGraphics();
        Font font = new Font("Helvetica", 1, 15);
        int i = getAppletWidth();
        int j = getAppletHeight();
        g.setColor(Color.black);
        g.fillRect(i / 2 - 140, j / 2 - 25, 280, 50);
        g.setColor(Color.white);
        g.drawRect(i / 2 - 140, j / 2 - 25, 280, 50);
        drawString(g, s1, font, i / 2, j / 2 - 10);
        drawString(g, s2, font, i / 2, j / 2 + 10);
    }

    public void kb() {
        psize = 0;
        opcode = 0;
        wc = -500;
        friendListCount = 0;
    }

    public void pong() {
        lastPing = System.currentTimeMillis();
    }

    public void checkConnection() {
        try {
            long l = System.currentTimeMillis();
            if (l - lastPing > 5000L) {
                lastPing = l;
                clientStream.newPacket(5);
                clientStream.flushPacket2();
            }
            if (!jb())
                return;
            wc++;
            if (wc > vc) {
                kb();
                lostConnection();
                return;
            }
            if (psize == 0 && clientStream.available() >= 2)
                psize = clientStream.readShort();
            if (psize > 0 && clientStream.available() >= psize) {
                clientStream.readBytes(psize, pdata);
                opcode = Utility.getUnsignedByte(pdata[0]);
                wc = 0;
                if (opcode == 8) {
                    String s1 = new String(pdata, 1, psize - 1);
                    showServerMessage(Utility.filterString(s1, true));
                } else if (opcode == 23) {
                    friendListCount = Utility.getUnsignedByte(pdata[1]);
                    for (int i = 0; i < friendListCount; i++) {
                        friendListHashes[i] = Utility.getUnsignedLong(pdata, 2 + i * 9);
                        friendListOnline[i] = Utility.getUnsignedByte(pdata[10 + i * 9]);
                    }

                } else if (opcode == 24) {
                    long l1 = Utility.getUnsignedLong(pdata, 1);
                    int online = pdata[9] & 0xff;
                    for (int j1 = 0; j1 < friendListCount; j1++)
                        if (friendListHashes[j1] == l1) {
                            if (friendListOnline[j1] == 0 && online != 0)
                                showServerMessage("@pri@" + Utility.hash2username(l1) + " has logged in");
                            if (friendListOnline[j1] != 0 && online == 0)
                                showServerMessage("@pri@" + Utility.hash2username(l1) + " has logged out");
                            friendListOnline[j1] = online;
                            psize = 0;
                            return;
                        }

                    friendListHashes[friendListCount] = l1;
                    friendListOnline[friendListCount] = online;
                    friendListCount++;
                    showServerMessage("@pri@" + Utility.hash2username(l1) + " has been added to your friends list");
                } else if (opcode == 26) {
                    ignoreListCount = Utility.getUnsignedByte(pdata[1]);
                    for (int j = 0; j < ignoreListCount; j++)
                        ignoreList[j] = Utility.getUnsignedLong(pdata, 2 + j * 8);

                } else if (opcode == 27) {
                    settingsHideStatus = pdata[1];
                    settingsBlockChat = pdata[2];
                    settingsBlockPrivate = pdata[3];
                    settingsBlockTrade = pdata[4];
                    sd = pdata[5];
                } else if (opcode == 28) {
                    long l2 = Utility.getUnsignedLong(pdata, 1);
                    String s2 = new String(pdata, 9, psize - 9);
                    if (l2 != Utility.username2hash(username))
                        s2 = Utility.filterString(s2, true);
                    showServerMessage("@pri@" + Utility.hash2username(l2) + ": tells you " + s2);
                } else {
                    handleIncomingPacket(opcode, psize, pdata);
                }
                psize = 0;
            }
        } catch (IOException _ex) {
            lostConnection();
        }
    }

    public void ab(String s1) {
        s1 = Utility.formatAuthString(s1, 20);
        clientStream.newPacket(25);
        clientStream.writeString(s1);
        clientStream.flushPacket();
    }

    public void sendPrivacySettings(int i, int j, int l, int i1, int j1) {
        clientStream.newPacket(31);
        clientStream.writeByte(i);
        clientStream.writeByte(j);
        clientStream.writeByte(l);
        clientStream.writeByte(i1);
        clientStream.writeByte(j1);
        clientStream.flushPacket();
    }

    public void ignoreAdd(String s1) {
        long l = Utility.username2hash(s1);
        clientStream.newPacket(29);
        clientStream.writeLong(l);
        clientStream.flushPacket();
        for (int i = 0; i < ignoreListCount; i++)
            if (ignoreList[i] == l)
                return;

        if (ignoreListCount >= 50) {
        } else {
            ignoreList[ignoreListCount++] = l;
        }
    }

    public void cb(long l) {
        clientStream.newPacket(30);
        clientStream.writeLong(l);
        clientStream.flushPacket();
        for (int i = 0; i < ignoreListCount; i++)
            if (ignoreList[i] == l) {
                ignoreListCount--;
                if (ignoreListCount - i >= 0) System.arraycopy(ignoreList, i + 1, ignoreList, i, ignoreListCount - i);

                return;
            }

    }

    public void friendAdd(String s1) {
        clientStream.newPacket(26);
        clientStream.writeLong(Utility.username2hash(s1));
        clientStream.flushPacket();
    }

    public void y(long l) {
        clientStream.newPacket(27);
        clientStream.writeLong(l);
        clientStream.flushPacket();
        for (int i = 0; i < friendListCount; i++) {
            if (friendListHashes[i] != l)
                continue;
            friendListCount--;
            for (int j = i; j < friendListCount; j++) {
                friendListHashes[j] = friendListHashes[j + 1];
                friendListOnline[j] = friendListOnline[j + 1];
            }

            break;
        }

        showServerMessage("@pri@" + Utility.hash2username(l) + " has been removed from your friends list");
    }

    public void sendPrivateMessage(long l, String s1) {
        if (s1.length() > 80)
            s1 = s1.substring(0, 80);
        clientStream.newPacket(28);
        clientStream.writeLong(l);
        clientStream.writeByte(s1.length());
        clientStream.writeString(s1);
        clientStream.flushPacket();
        showServerMessage("@pri@You tell " + Utility.hash2username(l) + ": " + s1);
    }

    public boolean sendCommandString(String s1) {
        if (s1.toLowerCase().startsWith("tell ")) {
            s1 = s1.substring(5);
            int i = s1.indexOf(' ');
            if (i == -1 || i >= s1.length() - 1) {
                showServerMessage("You must type a message too!");
                return true;
            } else {
                String s2 = s1.substring(0, i);
                s1 = s1.substring(i + 1);
                sendPrivateMessage(Utility.username2hash(s2), s1);
                return true;
            }
        }
        clientStream.newPacket(3);
        clientStream.writeString(s1);
        clientStream.flushPacket();
        lastPing = hd = System.currentTimeMillis();
        if (uc)
            showServerMessage("@yel@" + username.trim() + ": @whi@" + s1);
        return false;
    }

    public void showLoginScreenStatus(String s1, String s2) {
    }

    public void q() {
    }

    public void resetGame() {
    }

    public void resetLoginVars() {
    }

    public void hb() {
    }

    public void handleIncomingPacket(int i, int j, byte[] abyte0) {
    }

    public void showServerMessage(String s1) {
    }

    public boolean jb() {
        return true;
    }
}
